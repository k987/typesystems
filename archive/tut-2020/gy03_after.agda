{-# OPTIONS --prop #-}
module gy03_after where

open import Lib

module Nat where

  -- module I where
  --   Tm : Set
  --   zero : Tm
  --   suc : Tm → Tm

  --   Tm = ℕ
  --   zero = O
  --   suc = S

  record Algebra {i} : Set (lsuc i) where
    field
      Tm : Set i
      zero : Tm
      suc : Tm → Tm

    ⟦_⟧ : ℕ → Tm
    ⟦ O   ⟧ = zero
    ⟦ S t ⟧ = suc ⟦ t ⟧

  -- How many algebras exists, where terms get interpreted as elements of the
  -- empty set?
  -- A0 : Algebra
  -- A0 = record
  --   { Tm   = ↑p 𝟘
  --   ; zero = ?
  --   ; suc  = λ x → x
  --   }
  -- module A0 = Algebra A0

  -- How many algebras exists, where terms get interpreted as elements of the
  -- one element set?
  A1 : Algebra
  A1 = record
    { Tm   = ↑p 𝟙
    ; zero = ↑[ * ]↑
    ; suc  = λ x → x
    -- ; suc  = λ _ → ↑[ * ]↑
    }
  module A1 = Algebra A1

  test : 𝟚 × ℕ
  test = I , 342

  -- Product algebra:
  prod : ∀ {i} → (M N : Algebra {i}) → Algebra
  prod M N = record
    { Tm   = Algebra.Tm M × Algebra.Tm N
    ; zero = Algebra.zero M , Algebra.zero N
    ; suc  = λ { (m , n) → Algebra.suc M m , Algebra.suc N n }
    }

  -- Dependent algebra structure of natural numbers:
  record DepAlgebra {i} : Set (lsuc i) where
    field
      Tm      : ℕ → Set i
      zero    : Tm O
      suc     : ∀{t} → Tm t → Tm (S t)

    ⟦_⟧ : (t : ℕ) → Tm t
    ⟦ O   ⟧ = zero
    ⟦ S t ⟧ = suc ⟦ t ⟧

  -- Define addition over the natural numbers as terms using an algebra!
  Plus : ℕ → Algebra
  Plus n = record
    { Tm = ℕ
    ; zero = n
    ; suc = S
    }
  module Plus (n : ℕ) = Algebra (Plus n)

  -- Helper function for using the algebraic addition:
  plus : ℕ → ℕ → ℕ
  plus a b = Plus.⟦ a ⟧ b

  -- Prove the left and right identity of the plus function defined using this
  -- algebra!

  idr : ℕ → DepAlgebra
  idr n = record
    { Tm   = λ n → ↑p (plus n 0 ≡ n)
    ; zero = ↑[ refl ]↑
    ; suc  = λ _ → ↑[ refl ]↑
    }

  idl : DepAlgebra
  idl = record
    { Tm   = λ n → ↑p (plus 0 n ≡ n)
    ; zero = ↑[ refl ]↑
    ; suc  = λ eq → ↑[ cong S ↓[ eq ]↓ ]↑
    }

module NatBoolAST where

  open import NatBoolAST

  -- Define an algebra, in which the interpretation of true and false are equal,
  -- but the interpretation of false and zero are different!
  T≡F≢Z : Algebra
  T≡F≢Z = record
    { Tm      = ℕ
    ; true    = 1
    ; false   = 1
    ; ite     = λ _ _ _ → 0
    ; zero    = 0
    ; suc     = λ _ → 0
    ; isZero  = λ _ → 0
    ; _+_     = λ _ _ → 0
    }
  module T≡F≢Z = Algebra T≡F≢Z

  T≡F : T≡F≢Z.⟦ I.true ⟧ ≡ T≡F≢Z.⟦ I.false ⟧
  T≡F = refl

  F≢Z : ¬ T≡F≢Z.⟦ I.false ⟧ ≡ T≡F≢Z.⟦ I.zero ⟧
  F≢Z ()

  -- Degenerate Standard Algebra:
  DSA : Algebra
  DSA = record
    { Tm      = ℕ
    ; true    = 1
    ; false   = 0
    ; ite     = λ { (S _) t f → t ; O t f → f }
    ; zero    = 0
    ; suc     = S
    ; isZero  = λ { O → 1 ; (S _) → 0 }
    ; _+_     = _+ℕ_
    }
  module DSA = Algebra DSA

  -- Define an algebra that only returns I if a certain term is I.zero,
  -- otherwise O!
  z? : Algebra
  z? = record
    { Tm      = 𝟚
    ; true    = O
    ; false   = O
    ; ite     = λ _ _ _ → O
    ; zero    = I
    ; suc     = λ _ → O
    ; isZero  = λ _ → O
    ; _+_     = λ _ _ → O
    }
  module z? = Algebra z?

  -- Define an algebra that optimizes terms by eliminating zero additions from
  -- the left side, such that e.g. `zero + n` becomes `n`!
  -- (Tip: Use the previously defined z? algebra!)
  Opt : Algebra
  Opt = record
    { Tm      = I.Tm
    ; true    = I.true
    ; false   = I.false
    ; ite     = I.ite
    ; zero    = I.zero
    ; suc     = I.suc
    ; isZero  = I.isZero
    ; _+_     = λ l r → if z?.⟦ l ⟧ then r else (l I.+ r)
    }
  module Opt = Algebra Opt

  -- Example programs for testing optimization
  p : I.Tm
  p =
    isZero (
      ite
        (isZero (suc zero))
        (zero + zero)
        (ite true zero (suc (suc zero)))
      )
    where open I

  q : I.Tm
  q = ite (isZero (zero + suc zero)) false (isZero zero)
    where open I

  r : I.Tm
  r = (zero + zero) + (zero + (suc zero + zero))
    where open I

  test : I.Tm
  test = {!Opt.⟦ r ⟧!}
    where open I

  -- Prove the correcthess of this optimization over the degenerate standard
  -- algebra using a dependent algebra!

  plusHelper : ∀ {t} {t'} →
    ↑p (DSA.⟦ t ⟧ ≡ DSA.⟦ Opt.⟦ t ⟧ ⟧) →
    ↑p (DSA.⟦ t' ⟧ ≡ DSA.⟦ Opt.⟦ t' ⟧ ⟧) →
    ↑p (DSA.⟦ t I.+ t' ⟧ ≡ DSA.⟦ Opt.⟦ t I.+ t' ⟧ ⟧)
  plusHelper {t} {t'} ↑[ eq1 ]↑ ↑[ eq2 ]↑ with Opt.⟦ t ⟧
  ... | I.true           = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | I.false          = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | I.ite ot ot₁ ot₂ = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | I.zero           = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | I.suc ot         = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | I.isZero ot      = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑
  ... | ot I.+ ot₁       = ↑[ cong-2 _+ℕ_ eq1 eq2 ]↑

  OptCorrect : DepAlgebra
  OptCorrect = record
    { Tm     = λ t → ↑p (DSA.⟦ t ⟧ ≡ DSA.⟦ Opt.⟦ t ⟧ ⟧)
    ; true   = ↑[ refl ]↑
    ; false  = ↑[ refl ]↑
    ; ite    = λ { ↑[ eq1 ]↑ ↑[ eq2 ]↑ ↑[ eq3 ]↑ →
                     ↑[ cong-3 DSA.ite eq1 eq2 eq3 ]↑
                 }
    ; zero   = ↑[ refl ]↑
    ; suc    = λ { ↑[ eq ]↑ → ↑[ cong S eq ]↑ }
    ; isZero = λ { ↑[ eq ]↑ → ↑[ cong DSA.isZero eq ]↑ }
    ; _+_    = λ { {t} {t'} → plusHelper {t} {t'} }
    }

  -- Define an algebra that counts the number of trues and falses!
  T+F : Algebra
  T+F = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module T+F = Algebra T+F

  import Lemmas

  -- Prove that the number of trues is less than or equal to the
  -- sum of trues and falses!
  T≤T+F : DepAlgebra
  T≤T+F = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }

-- Extra task:
-- Improve the optimizer, so it also simplifies from the right.
-- (n + zero = n, not only zero + n = n)
