-- {-# OPTIONS --prop --rewriting #-}
{-# OPTIONS --prop #-}
module gy05 where

open import Lib

import NatBoolAST as AST
import NatBoolWT as WT
import NatBool as NB

module NB' where

  open NB.I

  eq : ite false true (isZero (zero + zero)) ≡ isZero (ite true zero (zero + zero))
  eq =
    ?
      ≡⟨ ? ⟩
    ?
      ∎

  -- There are terms that can be distinguished

  Diff : NB.Algebra
  Diff = record
    { Ty       = ?
    ; Tm       = ?
    ; Nat      = ?
    ; Bool     = ?
    ; zero     = ?
    ; suc      = ?
    ; isZero   = ?
    ; _+_      = ?
    ; true     = ?
    ; false    = ?
    ; ite      = ?
    ; isZeroβ₁ = ?
    ; isZeroβ₂ = ?
    ; +β₁      = ?
    ; +β₂      = ?
    ; iteβ₁    = ?
    ; iteβ₂    = ?
    }
  module Diff = NB.Algebra Diff

  neq' : ¬ I ≡ O
  neq' eq = ?

  neq'' : ¬ Diff.⟦ true ⟧t ≡ Diff.⟦ false ⟧t
  neq'' eq = ?

  neq : ¬ true ≡ false
  neq eq = ?

  -- Product of two NatBool algebras

  prod : ∀ {i} → (M N : NB.Algebra {i}) → NB.Algebra
  prod {i} M N = record
    { Ty       = ?
    ; Tm       = ?
    ; Nat      = ?
    ; Bool     = ?
    ; zero     = ?
    ; suc      = ?
    ; isZero   = ?
    ; _+_      = ?
    ; true     = ?
    ; false    = ?
    ; ite      = ?
    ; isZeroβ₁ = ?
    ; isZeroβ₂ = ?
    ; +β₁      = ?
    ; +β₂      = ?
    ; iteβ₁    = ?
    ; iteβ₂    = ?
    }
    where
      module M = NB.Algebra M
      module N = NB.Algebra N

-- Conversions between levels

-- WT-I→AST-I

WT-I→AST-I : AST.Algebra
WT-I→AST-I = record
  { Tm     = ?
  ; zero   = ?
  ; suc    = ?
  ; isZero = ?
  ; _+_    = ?
  ; true   = ?
  ; false  = ?
  ; ite    = ?
  }
  where open WT.I
module WT-I→AST-I = AST.Algebra WT-I→AST-I

-- WT→AST

WT→AST : WT.Algebra → AST.Algebra
WT→AST wt = record
  { Tm     = ?
  ; zero   = ?
  ; suc    = ?
  ; isZero = ?
  ; _+_    = ?
  ; true   = ?
  ; false  = ?
  ; ite    = ?
  }
  where open WT.Algebra wt

-- AST-I→WT-I

AST-I→WT-I : WT.Algebra
AST-I→WT-I = record
  { Ty       = ?
  ; Tm       = ?
  ; Nat      = ?
  ; Bool     = ?
  ; zero     = ?
  ; suc      = ?
  ; isZero   = ?
  ; _+_      = ?
  ; true     = ?
  ; false    = ?
  ; ite      = ?
  }
  where open AST.I
module AST-I→WT-I = WT.Algebra AST-I→WT-I

-- AST→WT

AST→WT : AST.Algebra → WT.Algebra
AST→WT ast = record
  { Tm     = ?
  ; zero   = ?
  ; suc    = ?
  ; isZero = ?
  ; _+_    = ?
  ; true   = ?
  ; false  = ?
  ; ite    = ?
  }
  where open AST.Algebra ast

-- NB-I→WT-I

NB-I→WT-I : WT.Algebra
NB-I→WT-I = record
  { Ty       = ?
  ; Tm       = ?
  ; Nat      = ?
  ; Bool     = ?
  ; zero     = ?
  ; suc      = ?
  ; isZero   = ?
  ; _+_      = ?
  ; true     = ?
  ; false    = ?
  ; ite      = ?
  }
  where open NB.I
module NB-I→WT-I = WT.Algebra NB-I→WT-I

-- WT-I→NB-I

WT-I→NB-I : NB.Algebra
WT-I→NB-I = record
  { Ty       = ?
  ; Tm       = ?
  ; Nat      = ?
  ; Bool     = ?
  ; zero     = ?
  ; suc      = ?
  ; isZero   = ?
  ; _+_      = ?
  ; true     = ?
  ; false    = ?
  ; ite      = ?
  ; isZeroβ₁ = ?
  ; isZeroβ₂ = ?
  ; +β₁      = ?
  ; +β₂      = ?
  ; iteβ₁    = ?
  ; iteβ₂    = ?
  }
  where open WT.I
module WT-I→NB-I = NB.Algebra WT-I→NB-I
