{-# OPTIONS --prop --rewriting #-}
module gy06_after where

open import Lib

-- Record and module explanation:

record Ember : Set where
  field
    age : ℕ
    isMale : 𝟚

isti : Ember
isti = record { age = 23 ; isMale = I }

module isti = Ember isti
open isti

-- Press C-c C-n in the hole to normalize the expression in the context!
n = {!age!}

-- Initial algebra of integers

module I where
  postulate
    Z     : Set
    zero  : Z
    suc   : Z → Z
    pred  : Z → Z
    sucpred : (i : Z) → suc (pred i) ≡ i -- suc ∘ pred = id
    predsuc : (i : Z) → pred (suc i) ≡ i -- pred ∘ suc = id

record Algebra {i} : Set (lsuc i) where
  field
    Z        : Set i
    zero     : Z
    suc      : Z → Z
    pred     : Z → Z
    sucpred  : (i : Z) → suc (pred i) ≡ i
    predsuc  : (i : Z) → pred (suc i) ≡ i

  postulate
    ⟦_⟧      : I.Z → Z
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦pred⟧ #-}

record DepAlgebra {i} : Set (lsuc i) where
  field
    Z        : I.Z → Set i
    zero     : Z I.zero
    suc      : {i' : I.Z} → Z i' → Z (I.suc  i')
    pred     : {i' : I.Z} → Z i' → Z (I.pred i')
    sucpred  : {i' : I.Z} → (i : Z i') → transport Z (I.sucpred i') (suc (pred i)) ≡ i
    predsuc  : {i' : I.Z} → (i : Z i') → transport Z (I.predsuc i') (pred (suc i)) ≡ i
  postulate
    ⟦_⟧      : (n : I.Z) → Z n
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ #-}


-- Example equalities:

module Examples {i} (Alg : Algebra {i}) where
  open Algebra Alg

  eq1 : pred (suc (pred zero)) ≡ pred zero
  eq1 = predsuc (pred zero)

  eq2 : pred (suc (suc (pred zero))) ≡ zero
  eq2 =
    pred (suc (suc (pred zero)))
      ≡⟨ predsuc (suc (pred zero)) ⟩
    suc (pred zero)
      ≡⟨ sucpred zero ⟩
    zero
      ∎

  eq3 : suc (suc (suc (pred (pred zero)))) ≡ suc zero
  eq3 =
    suc (suc (suc (pred (pred zero))))
      ≡⟨ cong suc ( cong suc (sucpred (pred zero))) ⟩
    suc (suc (pred zero))
      ≡⟨ cong suc ( sucpred (zero)) ⟩
    suc zero
      ∎


-- Addition:

M : I.Z → Algebra
M i = record
  { Z        = I.Z
  ; zero     = i
  ; suc      = I.suc
  ; pred     = I.pred
  ; sucpred  = I.sucpred
  ; predsuc  = I.predsuc
  }

module changeZeroToSucZero = Algebra (M (I.suc I.zero))
a = changeZeroToSucZero.⟦ I.pred (I.pred I.zero) ⟧

_+ℤ_ : I.Z → I.Z → I.Z
j +ℤ i = Algebra.⟦_⟧ (M i) j

module _ where
  open I
  -- Press C-c C-n in the hole to normalize the expression in the context!
  b = {!suc (zero) +ℤ pred (pred zero)!}

+-test-1 : {i : I.Z} →
           I.suc (I.suc (I.pred I.zero)) +ℤ i
           ≡
           I.suc (I.suc (I.pred i))
+-test-1 = refl

+-test-2 : {i : I.Z} →
           I.pred (I.suc (I.suc (I.pred I.zero))) +ℤ i
           ≡
           I.pred (I.suc (I.suc (I.pred i)))
+-test-2 = refl


-- Normal forms:

data Nf : Set where
  -suc  : ℕ → Nf
  zero  : Nf
  +suc  : ℕ → Nf

minusThree minusFive plusSix : Nf
minusThree  = -suc 2
minusFive   = +suc 4
plusSix     = +suc 5

sucNf : Nf → Nf
sucNf (-suc O) = zero
sucNf (-suc (S x)) = -suc x
sucNf zero = +suc 0
sucNf (+suc x) = +suc (S x)

sucNf-test-1 : sucNf (+suc 5) ≡ (+suc 6)
sucNf-test-1 = refl

sucNf-test-2 : sucNf (-suc 5) ≡ (-suc 4)
sucNf-test-2 = refl

predNf : Nf → Nf
predNf (-suc x) = -suc (S x)
predNf zero = -suc 0
predNf (+suc O) = zero
predNf (+suc (S x)) = +suc x


-- Normalisation:

N : Algebra
N = record
  { Z       = Nf
  ; zero    = zero
  ; suc     = sucNf
  ; pred    = predNf
  ; sucpred = λ { (-suc O) → refl ; (-suc (S n)) → refl ; zero → refl ; (+suc O) → refl ; (+suc (S n)) → refl }
  ; predsuc = λ { (-suc O) → refl ; (-suc (S n)) → refl ; zero → refl ; (+suc O) → refl ; (+suc (S n)) → refl }
  }
module N = Algebra N

⌜_⌝ : Nf → I.Z
⌜ -suc O ⌝ = I.pred I.zero
⌜ -suc (S x) ⌝ = I.pred ⌜ -suc x ⌝ -- \cul \cur
⌜ zero ⌝ = I.zero
⌜ +suc O ⌝ = I.suc I.zero
⌜ +suc (S x) ⌝ = I.suc ⌜ +suc x ⌝

norm-test : ⌜ N.⟦ (I.pred (I.pred (I.suc (I.pred (I.pred (I.suc I.zero)))))) ⟧ ⌝
            ≡
            I.pred (I.pred I.zero)
norm-test = refl


-- Stability:

stab : (nf : Nf) → N.⟦ ⌜ nf ⌝ ⟧ ≡ nf
stab (-suc O) = refl
stab (-suc (S x)) = cong predNf (stab (-suc x))
stab zero = refl
stab (+suc O) = refl
stab (+suc (S x)) = cong sucNf (stab (+suc x))


-- Lemmas for completeness:

⌜suc⌝ : (nf : Nf) → ⌜ sucNf nf ⌝ ≡ I.suc ⌜ nf ⌝
⌜suc⌝ (-suc O) = I.sucpred _ ⁻¹
⌜suc⌝ (-suc (S x)) = I.sucpred _ ⁻¹
⌜suc⌝ zero = refl
⌜suc⌝ (+suc x) = refl

⌜pred⌝ : (nf : Nf) → ⌜ predNf nf ⌝ ≡ I.pred ⌜ nf ⌝
⌜pred⌝ (-suc x) = refl
⌜pred⌝ zero = refl
⌜pred⌝ (+suc O) = I.predsuc _ ⁻¹
⌜pred⌝ (+suc (S x)) = I.predsuc _ ⁻¹


-- Completeness:

Comp : DepAlgebra
Comp = record
  { Z       = λ t → ↑p (⌜ N.⟦ t ⟧ ⌝ ≡ t)
  ; zero    = ↑[ refl ]↑
  ; suc     = λ { ↑[ ih ]↑ → ↑[ ⌜suc⌝ _ ◾ cong I.suc ih ]↑ }
  ; pred    = λ { ↑[ ih ]↑ → ↑[ ⌜pred⌝ _ ◾ cong I.pred ih ]↑ }
  ; sucpred = λ { ↑[ ih ]↑ → refl }
  ; predsuc = λ { ↑[ ih ]↑ → refl }
  }
module Comp = DepAlgebra Comp

comp : (t : I.Z) → ⌜ N.⟦ t ⟧ ⌝ ≡ t
comp i = ↓[ Comp.⟦ i ⟧ ]↓
