{-# OPTIONS --prop #-}
module gy08_after where

open import Lib hiding (_,_; _∘_)

open import Def

open I

v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
v0 = q
v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]
v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ∘ p ] -- q [ p ] [ p ]
v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ∘ p ∘ p ] -- v2 [ p ]

var-test-1 : def true v0 ≡ true
var-test-1 =
  def true v0
    ≡⟨ refl ⟩
  v0 [ id , true ]
    ≡⟨ refl ⟩
  q [ id , true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

var-test-2 : def true (def false v0) ≡ false
var-test-2 =
  def true (def false v0)
    ≡⟨ refl ⟩
  def true (q [ id , false ])
    ≡⟨ cong-2 def refl ▹β₂ ⟩
  def true false
    ≡⟨ refl ⟩
  false [ id , true ]
    ≡⟨ false[] ⟩
  false
    ∎

var-test-3 : def true (def false v1) ≡ true
var-test-3 =
  def true (def false v1)
    ≡⟨ refl ⟩
  def true (v1 [ id , false ])
    ≡⟨ refl ⟩
  def true (q [ p ] [ id , false ])
    ≡⟨ cong-2 def refl [∘] ⟩
  def true (q [ p ∘ (id , false) ])
    ≡⟨ cong-2 def refl (cong (q [_]) ▹β₁) ⟩
  def true (q [ id ])
    ≡⟨ cong-2 def refl [id] ⟩
  def true q
    ≡⟨ refl ⟩
  q [ id , true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

-- Different solution with nested equality proof
var-test-3' : def true (def false v1) ≡ true
var-test-3' =
  def true (def false v1)
    ≡⟨ refl ⟩
  def true (q [ p ] [ id , false ])
    ≡⟨ cong-2 def refl (
      (q [ p ] [ id , false ])
        ≡⟨ [∘] ⟩
      q [ p ∘ (id , false) ]
        ≡⟨ cong (q [_]) ▹β₁ ⟩
      q [ id ]
        ≡⟨ [id] ⟩
      q
        ∎
    ) ⟩
  def true q
    ≡⟨ refl ⟩
  q [ id , true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

-- Different solution using def[]
var-test-3'' : def true (def false v1) ≡ true
var-test-3'' =
  def true (def false v1)
    ≡⟨ refl ⟩
  (def false v1) [ id , true ]
    ≡⟨ def[] ⟩
  def (false [ id , true ]) (q [ p ] [ ((id , true) ∘ p) , q ])
    ≡⟨ cong-2 def false[] [∘] ⟩
  def false (q [ p ∘ ((id , true) ∘ p , q) ])
    ≡⟨ cong-2 def refl (cong (q [_]) ▹β₁) ⟩
  def false (q [ (id , true) ∘ p ])
    ≡⟨ refl ⟩
  q [ (id , true) ∘ p ] [ id , false ]
    ≡⟨ [∘] ⟩
  q [ ((id , true) ∘ p) ∘ (id , false) ]
    ≡⟨ cong (q [_]) ass ⟩
  q [ (id , true) ∘ (p ∘ (id , false)) ]
    ≡⟨ cong (λ σ → (q [ (id , true) ∘ σ ])) ▹β₁ ⟩
  q [ (id , true) ∘ id ]
    ≡⟨ cong (q [_]) idr  ⟩
  q [ id , true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

-- Shorter solution using def[]
var-test-3''' : def true (def false v1) ≡ true
var-test-3''' =
  def true (def false v1)
    ≡⟨ def[] ⟩
  def (false [ id , true ]) (q [ p ] [ ((id , true) ∘ p) , q ])
    ≡⟨ cong-2 def false[] [∘] ⟩
  def false (q [ p ∘ ((id , true) ∘ p , q) ])
    ≡⟨ cong-2 def refl (cong (q [_]) ▹β₁) ⟩
  def false (q [ (id , true) ∘ p ])
    ≡⟨ refl ⟩
  q [ (id , true) ∘ p ] [ id , false ]
    ≡⟨ [∘] ⟩
  q [ (id , true) ∘ p ∘ (id , false) ]
    ≡⟨ cong (q [_]) (ass ◾ cong _ ▹β₁ ◾ idr) ⟩
  q [ id , true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

sub-1 : Sub (∙ ▹ Nat) (∙ ▹ Bool ▹ Nat)
sub-1 = ε , isZero v0 , suc v0

sub-2 : {Γ : Con} → {A : Ty} → Sub (Γ ▹ Bool ▹ A ▹ A) (Γ ▹ A)
sub-2 = p ∘ p ∘ p , ite v2 v1 v0

sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
sub-swap = p ∘ p , v0 , v1

sub-3 : Sub (∙ ▹ Nat) (∙ ▹ Bool)
sub-3 = ε , isZero v0

tm-3 : Tm (∙ ▹ Bool) Nat
tm-3 = ite v0 (suc zero) zero

sub-3-test : tm-3 [ sub-3 ] ≡ ite (isZero v0) (suc zero) zero
sub-3-test =
  tm-3 [ sub-3 ]
    ≡⟨ refl ⟩
  (ite v0 (suc zero) zero) [ ε , isZero v0 ]
    ≡⟨ ite[] ⟩
  ite
    (q [ ε , isZero v0 ])
    (suc zero [ ε , isZero v0 ])
    (zero [ ε , isZero v0 ])
    ≡⟨ cong-3 ite ▹β₂ suc[] zero[] ⟩
  ite
    (isZero v0)
    (suc (zero [ ε , isZero q ]))
    zero
    ≡⟨ cong (λ t → ite (isZero v0) (suc t) zero) zero[] ⟩
  ite (isZero v0) (suc zero) zero
    ∎

-- let x:=1 in x + x = 2
eq-1 : def (suc zero) (v0 + v0) ≡ suc (suc zero)
eq-1 =
  def (suc zero) (v0 + v0)
    ≡⟨ refl ⟩
  (v0 + v0) [ id , suc zero ]
    ≡⟨ +[] ⟩
  ((q [ id , suc zero ]) + (q [ id , suc zero ]))
    ≡⟨ cong-2 _+_ ▹β₂ ▹β₂ ⟩
  (suc zero + suc zero)
    ≡⟨ +β₂ ⟩
  suc (zero + suc zero)
    ≡⟨ cong suc +β₁ ⟩
  suc (suc zero)
    ∎

-- Shorter soultion using transitivity
eq-1' : def (suc zero) (v0 + v0) ≡ suc (suc zero)
eq-1' =
  def (suc zero) (v0 + v0)
    ≡⟨ +[] ◾ cong-2 _+_ ▹β₂ ▹β₂ ⟩
  (suc zero + suc zero)
    ≡⟨ +β₂ ◾ cong suc +β₁ ⟩
  suc (suc zero)
    ∎

sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
sub-eq-1 {σ = σ} {δ = δ} = σ ≡⟨ ∙η ⟩ ε ≡⟨ ∙η ⁻¹ ⟩ δ ∎

sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ∘ σ ≡ ε
sub-eq-2 = {!!}

sub-eq-3 :
  {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
  (σ , t) ∘ δ ≡ (σ ∘ δ , t [ δ ])
sub-eq-3 {σ = σ} {δ = δ} {t = t} =
  (σ , t) ∘ δ
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  (σ ∘ δ , t [ δ ])
    ∎

sub-eq-4 :
  {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
  (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
sub-eq-4 = {!!}
