{-# OPTIONS --prop --rewriting #-}
module gy03 where

open import Lib

module NatBoolAST where

  module I where
    data Tm   : Set where
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

  record Model {i} : Set (lsuc i) where
    field
      Tm      : Set i
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.true         ⟧ = true
    ⟦ I.false        ⟧ = false
    ⟦ I.ite t t' t'' ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n        ⟧ = num n
    ⟦ I.isZero t     ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'      ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  tm1 : I.Tm
  tm1 = ite true (num 1 +o num 3) (isZero (isZero false))
    where open I

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 1  num 3  isZero
                    |
                    |
                  false
  -}

  -- Calculate the height of a syntax tree

  Height : Model {i = lzero}
  Height = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module H = Model Height

  height-test-1 : H.⟦ I.false I.+o (I.num 1) ⟧ ≡ 1
  height-test-1 = refl {x = 1}

  height-test-2 : H.⟦ tm1 ⟧ ≡ 3
  height-test-2 = refl {x = 3}

  -- Count the number of trues in a term

  Trues : Model {i = lzero}
  Trues = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module T = Model Trues

  trues-test-1 : T.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  trues-test-1 = refl {x = 0}

  trues-test-2 : T.⟦ tm1 ⟧ ≡ 1
  trues-test-2 = refl {x = 1}

  trues-test-3 : T.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  trues-test-3 = refl {x = 3}

  -- Interpret the syntax as a C style evaluation of the term
  -- (Booleans as numbers: false = 0, true = 1)
  -- (Numbers as booleans: 0 = false, _ = true)

  CStyle : Model {i = lzero}
  CStyle = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module C = Model CStyle

  cStyle-test-1 : C.⟦ I.false I.+o (I.num 1) ⟧ ≡ 1
  cStyle-test-1 = refl {x = 1}

  cStyle-test-2 : C.⟦ tm1 ⟧ ≡ 4
  cStyle-test-2 = refl {x = 4}

  -- Variation with boolean output

  CStyle' : Model {i = lzero}
  CStyle' = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module C' = Model CStyle'

  cStyle'-test-1 : C'.⟦ I.false I.+o (I.num 0) ⟧ ≡ ff
  cStyle'-test-1 = refl {x = ff}

  cStyle'-test-2 : C'.⟦ tm1 ⟧ ≡ tt
  cStyle'-test-2 = refl {x = tt}

  -- Distinguishing

  TF : Model {i = lsuc lzero}
  TF = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module TF = Model TF

  -- true≠false
  -- true≠isZero

  -- Type inference

  data Ty : Set where
    Nat : Ty
    Bool : Ty

  Infer : Model {i = lzero}
  Infer = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module Inf = Model Infer

  infer-test-1 : Inf.⟦ (I.num 3) I.+o (I.num 1) ⟧ ≡ Just Nat
  infer-test-1 = refl {x = Just Nat}

  infer-test-2 : Inf.⟦ tm1 ⟧ ≡ Nothing
  infer-test-2 = refl {x = Nothing {A = Ty}}

  infer-test-3 : Inf.⟦ I.ite I.true I.true I.true ⟧ ≡ Just Bool
  infer-test-3 = refl {x = Just Bool}

  -- Dependent model and evaluation

  record DepModel {i} : Set (lsuc i) where
    field
      Tm      : I.Tm → Set i
      true    : Tm I.true
      false   : Tm I.false
      ite     : ∀{t t' t''} → Tm t → Tm t' → Tm t'' → Tm (I.ite t t' t'')
      num     : (n : ℕ) → Tm (I.num n)
      isZero  : ∀{t} → Tm t → Tm (I.isZero t)
      _+o_    : ∀{t t'} → Tm t → Tm t' → Tm (t I.+o t')

    ⟦_⟧ : (t : I.Tm) → Tm t
    ⟦ I.true          ⟧ = true
    ⟦ I.false         ⟧ = false
    ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n         ⟧ = num n
    ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'       ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  evalType : Maybe Ty → Set
  evalType T = {!   !}

  ite-help : ∀ {t} {t'} {t''} →
       evalType Inf.⟦ t ⟧ →
       evalType Inf.⟦ t' ⟧ →
       evalType Inf.⟦ t'' ⟧ → evalType Inf.⟦ I.ite t t' t'' ⟧
  ite-help {t} {t'} {t''} co tr fa with Inf.⟦ t ⟧ | Inf.⟦ t' ⟧ | Inf.⟦ t'' ⟧
  ... | A | B | C = {!   !}

  isZero-help : ∀ {t} → evalType Inf.⟦ t ⟧ → evalType Inf.⟦ I.isZero t ⟧
  isZero-help {t} n with Inf.⟦ t ⟧
  ... | A = {!   !}

  +o-help : ∀ {t} {t'} →
         evalType Inf.⟦ t ⟧ →
         evalType Inf.⟦ t' ⟧ → evalType Inf.⟦ t I.+o t' ⟧
  +o-help {t} {t'} l r with Inf.⟦ t ⟧ | Inf.⟦ t' ⟧
  ... | A | B = {!   !}

  eval : DepModel
  eval = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = λ where {t} {t'} {t''} co tr fa → ite-help {t} {t'} {t''} co tr fa
    ; num    = {!   !}
    ; isZero = λ where {t} n → isZero-help {t} n
    ; _+o_   = λ where {t} {t'} l r → +o-help {t} {t'} l r
    }

-- test = {! E.⟦ (I.num 3) I.+o (I.num 1) ⟧ !}
-- test = {! E.⟦ I.ite I.true I.true I.true ⟧ !}
-- test = {! E.⟦ tm1 ⟧ !}