{-# OPTIONS --prop --rewriting #-}
module gy07_after where

open import Lib

module IntOperations where

  open import Int using (Model ; ℤ ; norm)
  open Int.I

  +One +Two +Three -One -Two -Three : ℤ

  +One   = Suc Zero
  +Two   = Suc +One
  +Three = Suc +Two

  -One   = Pred Zero
  -Two   = Pred -One
  -Three = Pred -Two

  -- a     =       (Suc (Pred (Suc Zero)))
  -- b     = Pred (Zero                   )
  -- a + b = Pred ((Suc (Pred (Suc Zero))))

  Plus : ℤ → Model {ℓ = lzero}
  Plus a = record
    { Z       = Z
    ; Zero    = a
    ; Suc     = Suc
    ; Pred    = Pred
    ; SucPred = SucPred
    ; PredSuc = PredSuc
    }

  _+ℤ_ : ℤ → ℤ → ℤ
  a +ℤ b = ⟦ b ⟧ where open Model (Plus a)

  +ℤ-test = {! +Two +ℤ +One !}

  Minus : ℤ → Model {ℓ = lzero}
  Minus a = record
    { Z       = Z
    ; Zero    = a
    ; Suc     = Pred
    ; Pred    = Suc
    ; SucPred = PredSuc
    ; PredSuc = SucPred
    }

  _-ℤ_ : ℤ → ℤ → ℤ
  a -ℤ b = ⟦ b ⟧ where open Model (Minus a)

  -ℤ-test = {! norm (-Three -ℤ -One)  !}


module DefWT where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty

    data Con : Set where
      ∙ : Con
      _▹_ : Con → Ty → Con

    infixl 5 _▹_

    data Var : Con → Ty → Set where
      vz : ∀{Γ A} → Var (Γ ▹ A) A
      vs : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    data Tm (Γ : Con) : Ty → Set where
      var     : ∀{A} → Var Γ A → Tm Γ A
      def     : ∀{A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

      true    : Tm Γ Bool
      false   : Tm Γ Bool
      ite     : ∀{A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
      num     : ℕ → Tm Γ Nat
      isZero  : Tm Γ Nat → Tm Γ Bool
      _+o_    : Tm Γ Nat → Tm Γ Nat → Tm Γ Nat

    v0 : {Γ : Con}{A : Ty}        → Tm (Γ ▹ A) A
    v0 = var vz
    v1 : {Γ : Con}{A B : Ty}      → Tm (Γ ▹ A ▹ B) A
    v1 = var (vs vz)
    v2 : {Γ : Con}{A B C : Ty}    → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = var (vs (vs vz))
    v3 : {Γ : Con}{A B C D : Ty}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = var (vs (vs (vs vz)))

  open I

  -- tm-impossible : Tm ∙ Nat
  -- tm-impossible = def false (v0 +o v0)

  -- ∙ = \.
  -- ▹ = \tw + right arrow key

  con-example : Con
  con-example = ∙ ▹ Nat

  tm-example : Tm con-example Bool
  tm-example = isZero v0

  tm-0 : Tm (∙ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : Tm (∙ ▹ Nat ▹ Nat) Nat
  --     ?                |  ? ▹ Nat
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : Tm (∙ ▹ Nat ▹ Bool ▹ Nat) Bool
  tm-2 = ite (isZero (v0 +o v2)) v1 v1

  tm-3 : {A : Ty} → Tm (∙ ▹ Bool ▹ A ▹ Nat) A
  tm-3 = ite (ite v2 v2 (isZero v0)) v1 v1
