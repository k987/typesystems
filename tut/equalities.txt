Monoid rules:
- ass : (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
- idl : id ⊚ γ ≡ γ
- idr : γ ⊚ id ≡ γ

Eta rules:
- ∙η : {σ : Sub Γ ∙} → σ ≡ ε
- ▹η : (p ⊚ γa) ,o (q [ γa ]) ≡ γa

Beta rules:
- ▹β₁      : p ⊚ (γ ,o t) ≡ γ
- ▹β₂      : q [ γ ,o t ] ≡ t
- iteβ₁    : ite true u v ≡ u
- iteβ₂    : ite false u v ≡ v
- isZeroβ₁ : isZero (num 0) ≡ true
- isZeroβ₂ : isZero (num (1 + n)) ≡ false
- +β       : num m +o num n ≡ num (m + n)

Substitution rules:
- [∘]      : t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
- [id]     : t [ id ] ≡ t
- true[]   : true [ γ ] ≡ true
- false[]  : false [ γ ] ≡ false
- ite[]    : (ite t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])
- num[]    : num n [ γ ] ≡ num n
- isZero[] : isZero t [ γ ] ≡ isZero (t [ γ ])
- +[]      : (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])