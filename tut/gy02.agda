{-# OPTIONS --prop --rewriting #-}
module gy02 where

open import Lib

module Nat where

  -- data ℕ : Set where
  --   zero : ℕ
  --   suc : ℕ → ℕ

  -- Model

  record Model {i} : Set (lsuc i) where
    field
      Nat  : Set i
      Zero : Nat
      Suc  : Nat → Nat

    ⟦_⟧ : ℕ → Nat
    ⟦ zero  ⟧ = Zero
    ⟦ suc n ⟧ = Suc ⟦ n ⟧

  -- Initial model

  I : Model
  I = record
    { Nat  = ℕ
    ; Zero = zero
    ; Suc  = suc
    }

  module I = Model I

  -- Example: C-style bool interpretation

  CBool : Model
  CBool = record
    { Nat  = ?
    ; Zero = ?
    ; Suc  = ?
    }

  module CB = Model CBool

  CB-test-1 : CB.⟦ 0 ⟧ ≡ ff
  CB-test-1 = refl

  CB-test-2 : CB.⟦ 1 ⟧ ≡ tt
  CB-test-2 = refl

  CB-test-3 : CB.⟦ 3 ⟧ ≡ tt
  CB-test-3 = refl

  -- _*2+1

  TimesTwoPlusOne : Model
  TimesTwoPlusOne = record
    { Nat  = ?
    ; Zero = ?
    ; Suc  = ?
    }

  module TTPO = Model TimesTwoPlusOne

  _*2+1 : ℕ → ℕ
  n *2+1 = TTPO.⟦ n ⟧

  *2+1-test-1 : 3 *2+1 ≡ 7
  *2+1-test-1 = refl

  -- Plus

  Plus : Model
  Plus = ? -- C-c C-r

  module P = Model Plus

  _+'_ : ℕ → ℕ → ℕ
  _+'_ = P.⟦_⟧

  -- Dependent example

  -- natToSet
  -- dependent

  -- Dependent model

  record DepModel {i} : Set (lsuc i) where
    field
      Nat   : I.Nat → Set i
      Zero  : Nat I.Zero
      Suc   : {n : I.Nat} → Nat n → Nat (I.Suc n)

    ⟦_⟧ : (n : I.Nat) → Nat n
    ⟦ zero ⟧ = Zero
    ⟦ suc n ⟧ = Suc ⟦ n ⟧


  -- Associativity of addition

  Assoc : (n o : I.Nat) → DepModel
  Assoc n o = record
    { Nat  = ?
    ; Zero = ?
    ; Suc  = ?
    }

  assoc : (m n o : I.Nat) → (m + n) + o ≡ m + (n + o)
  assoc m n o = ?


module NatBoolAST where

  module I where
    data Tm   : Set where
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

  record Model {i} : Set (lsuc i) where
    field
      Tm      : Set i
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.true         ⟧ = true
    ⟦ I.false        ⟧ = false
    ⟦ I.ite t t' t'' ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n        ⟧ = num n
    ⟦ I.isZero t     ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'      ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  tm1 : I.Tm
  tm1 = ite true (num 0 +o num 3) (isZero (isZero false))
    where open I

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 0  num 3  isZero
                    |
                    |
                  false
  -}

  -- Calculate the height of a syntax tree

  Height : Model
  Height = record
    { Tm     = ?
    ; true   = ?
    ; false  = ?
    ; ite    = ?
    ; num    = ?
    ; isZero = ?
    ; _+o_   = ?
    }

  module H = Model Height

  height-test-1 : H.⟦ I.false I.+o (I.num 1) ⟧ ≡ 1
  height-test-1 = refl

  height-test-2 : H.⟦ tm1 ⟧ ≡ 3
  height-test-2 = refl

  -- Count the number of trues in a term

  Trues : Model
  Trues = record
    { Tm     = ?
    ; true   = ?
    ; false  = ?
    ; ite    = ?
    ; num    = ?
    ; isZero = ?
    ; _+o_   = ?
    }

  module T = Model Trues

  trues-test-1 : T.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  trues-test-1 = refl

  trues-test-2 : T.⟦ tm1 ⟧ ≡ 1
  trues-test-2 = refl
