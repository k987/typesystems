{-# OPTIONS --prop --rewriting #-}
module gy03_after where

open import Lib

module NatBoolAST where

  module I where
    data Tm   : Set where
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

  record Model {i} : Set (lsuc i) where
    field
      Tm      : Set i
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.true         ⟧ = true
    ⟦ I.false        ⟧ = false
    ⟦ I.ite t t' t'' ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n        ⟧ = num n
    ⟦ I.isZero t     ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'      ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  tm1 : I.Tm
  tm1 = ite true (num 1 +o num 3) (isZero (isZero false))
    where open I

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 1  num 3  isZero
                    |
                    |
                  false
  -}

  -- Calculate the height of a syntax tree

  Height : Model {i = lzero}
  Height = record
    { Tm     = ℕ
    ; true   = 0
    ; false  = 0
    ; ite    = λ co tr fa → 1 + max co (max tr fa)
    ; num    = λ _ → 0
    ; isZero = λ x → x + 1
    ; _+o_   = λ l r → 1 + max l r
    }

  module H = Model Height

  height-test-1 : H.⟦ I.false I.+o (I.num 1) ⟧ ≡ 1
  height-test-1 = refl {x = 1}

  height-test-2 : H.⟦ tm1 ⟧ ≡ 3
  height-test-2 = refl {x = 3}

  -- Count the number of trues in a term

  Trues : Model {i = lzero}
  Trues = record
    { Tm     = ℕ
    ; true   = 1
    ; false  = 0
    ; ite    = λ co tr fa → co + tr + fa
    ; num    = λ _ → 0
    ; isZero = λ x → x
    ; _+o_   = λ l r → l + r
    }

  module T = Model Trues

  trues-test-1 : T.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  trues-test-1 = refl {x = 0}

  trues-test-2 : T.⟦ tm1 ⟧ ≡ 1
  trues-test-2 = refl {x = 1}

  trues-test-3 : T.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  trues-test-3 = refl {x = 3}

  -- Interpret the syntax as a C style evaluation of the term
  -- (Booleans as numbers: false = 0, true = 1)
  -- (Numbers as booleans: 0 = false, _ = true)

  CStyle : Model {i = lzero}
  CStyle = record
    { Tm     = ℕ
    ; true   = 1
    ; false  = 0
    ; ite    = λ where zero tr fa → fa
                       (suc co) tr fa → tr
    ; num    = λ x → x
    ; isZero = λ where zero → 1
                       (suc x) → 0
    ; _+o_   = _+_
    -- ; _+o_   = λ l r → l + r
    }

  module C = Model CStyle

  cStyle-test-1 : C.⟦ I.false I.+o (I.num 1) ⟧ ≡ 1
  cStyle-test-1 = refl {x = 1}

  cStyle-test-2 : C.⟦ tm1 ⟧ ≡ 4
  cStyle-test-2 = refl {x = 4}

  -- Variation with boolean output

  ha_akkor_amugy_ : {A : Set} → 𝟚 → A → A → A
  ha tt akkor i amugy h = i
  ha ff akkor i amugy h = h

  CStyle' : Model {i = lzero}
  CStyle' = record
    { Tm     = 𝟚
    ; true   = tt
    ; false  = ff
    ; ite    = if_then_else_
    ; num    = λ where zero → ff
                       (suc x) → tt
    ; isZero = if_then ff else tt -- λ n → if n then ff else tt
    ; _+o_   = _∨_
    }

  module C' = Model CStyle'

  cStyle'-test-1 : C'.⟦ I.false I.+o (I.num 0) ⟧ ≡ ff
  cStyle'-test-1 = refl {x = ff}

  cStyle'-test-2 : C'.⟦ tm1 ⟧ ≡ tt
  cStyle'-test-2 = refl {x = tt}

  -- Distinguishing

  TF : Model {i = lsuc lzero}
  TF = record
    { Tm     = Prop
    ; true   = ⊤
    ; false  = ⊥
    ; ite    = λ x x₁ x₂ → ⊤
    ; num    = λ x → ⊤
    ; isZero = λ x → ⊥
    ; _+o_   = λ x x₁ → ⊤
    }

  module TF = Model TF

  true≠false : ¬ (I.true ≡ I.false)
  true≠false eq = coep TF.⟦_⟧ eq trivi

  true≠isZero : {t : I.Tm} → ¬ (I.true ≡ I.isZero t)
  true≠isZero eq = coep TF.⟦_⟧ eq trivi
