{-# OPTIONS --prop --rewriting #-}
module gy02_after where

open import Lib

module Nat where

  -- data ℕ : Set where
  --   zero : ℕ
  --   suc : ℕ → ℕ

  -- Model

  record Model {i} : Set (lsuc i) where
    field
      Nat  : Set i
      Zero : Nat
      Suc  : Nat → Nat

    ⟦_⟧ : ℕ → Nat
    ⟦ zero  ⟧ = Zero
    ⟦ suc n ⟧ = Suc ⟦ n ⟧

  -- Initial model

  I : Model
  I = record
    { Nat  = ℕ
    ; Zero = zero
    ; Suc  = suc
    }

  module I = Model I

  -- Example: C-style bool interpretation

  CBool : Model
  CBool = record
    { Nat  = 𝟚
    ; Zero = ff
    ; Suc  = λ _ → tt
    }

  module CB = Model CBool

  -- Use the following function call if the module
  -- declaration above isn't working for you.
  CB-test-1 : Model.⟦_⟧ CBool 0 ≡ ff
  CB-test-1 = refl {x = ff}

  CB-test-2 : CB.⟦ 1 ⟧ ≡ tt
  CB-test-2 = refl {x = tt}

  CB-test-3 : CB.⟦ 3 ⟧ ≡ tt
  CB-test-3 = refl {x = tt}

  -- _*2+1

  TimesTwoPlusOne : Model
  TimesTwoPlusOne = record
    { Nat  = ℕ
    ; Zero = suc zero
    ; Suc  = λ n → suc (suc n)
    }

    --   suc    |   (suc    |    zero)
    -- suc (suc | (suc (suc | (suc zero))))

  module TTPO = Model TimesTwoPlusOne

  _*2+1 : ℕ → ℕ
  n *2+1 = TTPO.⟦ n ⟧

  *2+1-test-1 : 3 *2+1 ≡ 7
  *2+1-test-1 = refl {x = 7}

  -- Plus

  plus : ℕ → (ℕ → ℕ)
  plus zero n = n
  plus (suc m) n = suc (plus m n)

  addTen : ℕ → ℕ
  addTen = plus 10

  -- test = {! addTen 32  !}

  Plus : Model
  Plus = record
    { Nat  = ℕ → ℕ
    ; Zero = λ n → n
    ; Suc  = λ f → λ n → suc (f n)
    -- ; Suc = λ f → λ n → f (suc n)
    }

    -- Amink van: addFive, n
    -- Ami kell: addSix n

  module P = Model Plus

  _+'_ : ℕ → ℕ → ℕ
  _+'_ = P.⟦_⟧

  -- test-2 = {! 13 +' 29  !}

  -- Dependent example

  natToSet : ℕ → Set
  natToSet zero = 𝟚
  natToSet (suc zero) = 𝟚
  natToSet (suc (suc n)) = ℕ

  dependent : (n : ℕ) → natToSet n
  dependent zero = ff
  dependent (suc zero) = tt
  dependent (suc (suc n)) = suc (suc n)

  -- test-3 = {! dependent 123  !}

  -- Dependent model

  record DepModel {i} : Set (lsuc i) where
    field
      Nat   : I.Nat → Set i
      Zero  : Nat I.Zero
      Suc   : {n : I.Nat} → Nat n → Nat (I.Suc n)

    ⟦_⟧ : (n : I.Nat) → Nat n
    ⟦ zero ⟧ = Zero
    ⟦ suc n ⟧ = Suc ⟦ n ⟧


  -- Associativity of addition

  Assoc : (n o : I.Nat) → DepModel
  Assoc n o = record
    { Nat  = λ m → Lift ((m + n) + o ≡ m + (n + o))
    ; Zero = mkLift (refl {x = n + o})
    ; Suc  = λ ih → ih
    }

    -- Ami van (ih): (m + n) + o ≡ m + (n + o)
    -- Ami kell: (suc m + n) + o ≡ suc m + (n + o)
    --           suc (m + n) + o ≡ suc (m + (n + o))
    --           suc ((m + n) + o) ≡ suc (m + (n + o))

  assoc : (m n o : I.Nat) → (m + n) + o ≡ m + (n + o)
  assoc m n o = unLift ⟦ m ⟧
    where open DepModel (Assoc n o)
