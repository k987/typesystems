{-# OPTIONS --prop --rewriting #-}
module gy01 where

infix 4 _≡_
postulate
  _≡_  : ∀{i}{A : Set i}(x : A) → A → Prop i
  refl : ∀{i}{A : Set i}{x : A} → x ≡ x
  cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A}(a= : a ≡ a') → f a ≡ f a'
{-# BUILTIN REWRITE _≡_ #-}

-- Booleans

data 𝟚 : Set where
  tt ff : 𝟚

example-bool : 𝟚
example-bool = ?

another-bool : 𝟚
another-bool = ?

-- idb

idb : 𝟚 → 𝟚
idb x = ?

idb-test-1 : idb tt ≡ tt
idb-test-1 = refl

idb-test-2 : idb ff ≡ ff
idb-test-2 = refl

-- neg

neg : 𝟚 → 𝟚
neg x = ?

neg-test-1 : neg tt ≡ ff
neg-test-1 = refl

neg-test-2 : neg ff ≡ tt
neg-test-2 = refl

-- How many 𝟚 → 𝟚 functions are there?

-- or

or : 𝟚 → 𝟚 → 𝟚
or a b = ?

or-test-1 : or tt tt ≡ tt
or-test-1 = refl

or-test-2 : or tt ff ≡ tt
or-test-2 = refl

or-test-3 : or ff tt ≡ tt
or-test-3 = refl

or-test-4 : or ff ff ≡ ff
or-test-4 = refl

-- and

and : 𝟚 → 𝟚 → 𝟚
and = ?

and-test-1 : and tt tt ≡ tt
and-test-1 = refl

and-test-2 : and tt ff ≡ ff
and-test-2 = refl

and-test-3 : and ff tt ≡ ff
and-test-3 = refl

and-test-4 : and ff ff ≡ ff
and-test-4 = refl

-- xor

xor : 𝟚 → 𝟚 → 𝟚
xor = ?

xor-test-1 : xor tt tt ≡ ff
xor-test-1 = refl

xor-test-2 : xor tt ff ≡ tt
xor-test-2 = refl

xor-test-3 : xor ff tt ≡ tt
xor-test-3 = refl

xor-test-4 : xor ff ff ≡ ff
xor-test-4 = refl

-- Natural numbers

-- {-# BUILTIN NATURAL ℕ #-}

-- addTwo

-- addTwo-test-1 : addTwo 0 ≡ 2
-- addTwo-test-1 = refl

-- addTwo-test-2 : addTwo 3 ≡ 5
-- addTwo-test-2 = refl

-- _*2+1 : ℕ → ℕ
-- n *2+1 = ?

-- *2+1-test-1 : 3 *2+1 ≡ 7
-- *2+1-test-1 = refl

-- plus

-- plus-idl : (n : ℕ) → plus 0 n ≡ n
-- plus-idl n = ?

-- plus-idr : (n : ℕ) → plus n 0 ≡ n
-- plus-idr n = ?
