{-# OPTIONS --prop #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I
open import Full.DepAlgebra
-- open import Full.Standard

module Full.Canonicity where

open I

-- Bool

data PBool : Tm ∙ Bool → Set where
  Ptrue  : PBool true
  Pfalse : PBool false

Pite :
  {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) →
  PBool t' → A u' → A v' → A (ite t' u' v')
Pite {u' = u'} {v'} A Ptrue  u v = transport A (iteβ₁ ⁻¹) u
Pite {u' = u'} {v'} A Pfalse u v = transport A (iteβ₂ ⁻¹) v

PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
PcanBool Ptrue  = ι₁ ↑[ refl ]↑
PcanBool Pfalse = ι₂ ↑[ refl ]↑

-- Tree1

data PTree1 {A' : Ty} (A : Tm ∙ A' → Set) : Tm ∙ (Tree1 A') → Set where
  Pleaf1 : PTree1 A leaf1
  Pnode1 : {ll' : Tm ∙ (Tree1 A')} → {a' : Tm ∙ A'} → {rr' : Tm ∙ (Tree1 A')} →
           PTree1 A ll' → A a' → PTree1 A rr' → PTree1 A (node1 ll' a' rr')

PrecTree1 :
  {A' B' : Ty} → {b' : Tm ∙ B'} → {n' : Tm (∙ ▹ B' ▹ A' ▹ B') B'} → {t' : Tm ∙ (Tree1 A')} →
  {A : Tm ∙ A' → Set} → (B : Tm ∙ B' → Set) → (b : B b') →
  (n : {ll rr : Tm ∙ B'} → {a : Tm ∙ A'} → B ll → A a → B rr → B (n' [ id , ll , a , rr ])) →
  (t : PTree1 A t') → B (recTree1 b' n' t')
PrecTree1 B b n Pleaf1 = transport B (Tree1β₁ ⁻¹) b
-- PrecTree1 B b n (Pnode1 ll a rr) = n (PrecTree1 B b n ll) a (PrecTree1 B b n rr)
PrecTree1 B b n (Pnode1 ll a rr) = transport B (Tree1β₂ ⁻¹) (n (PrecTree1 B b n ll) a (PrecTree1 B b n rr))

PcanTree1 :
  {A' : Ty} → {t' : Tm ∙ (Tree1 A')} → {A : Tm ∙ A' → Set} → PTree1 A t' →
  ↑p (t' ≡ leaf1) ⊎ Σ (Tm ∙ (Tree1 A')) λ ll' → Σ (Tm ∙ A') λ a' → Σ (Tm ∙ (Tree1 A')) λ rr' → ↑p (t' ≡ node1 ll' a' rr')
PcanTree1 Pleaf1 = ι₁ refl↑
PcanTree1 (Pnode1 {ll'} {a'} {rr'} ll a rr) = ι₂ (ll' ,Σ (a' ,Σ (rr' ,Σ refl↑)))

Can : DepAlgebra
Can = record
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ where {Θ = Θ} σ δ x → transport Θ (ass ⁻¹) (σ (δ x))
       -- ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = λ where {Γ = Γ} x → transport Γ (idl ⁻¹) x
       -- ; id         = idf
       ; ass        = {!!}
       ; idl        = {!!}
       ; idr        = {!!}
       ; _[_]       = λ where {A = A} t σ x → transport A ([∘] ⁻¹) (t (σ x))
       -- ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = {!!}
       ; [∘]        = {!!}
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = {!!}
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
       ; _,_        = λ σ t x → {!!}
       -- ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = {!!}
       ; ▹β₂        = {!!}
       ; ▹η         = {!!}

       ; Bool       = PBool
       ; true       = λ _ → transport PBool (true[] ⁻¹) Ptrue
       -- ; true       = λ _ → Ptrue
       ; false      = λ _ → transport PBool (false[] ⁻¹) Pfalse
       -- ; false      = λ _ → Pfalse
       ; ite        = λ where {A = A} t u v {ν'} ν̂ → transport A (ite[] ⁻¹) (Pite A (t ν̂) (u ν̂) (v ν̂))
       -- ; ite        = λ where {A = A} t u v {ν'} ν̂ → Pite A (t ν̂) (u ν̂) (v ν̂)
       ; iteβ₁      = {!!}
       ; iteβ₂      = {!!}
       ; true[]     = {!!}
       ; false[]    = {!!}
       ; ite[]      = {!!}

       ; Tree1      = PTree1
       ; leaf1      = λ where {A = A} _ → transport (PTree1 A) (leaf1[] ⁻¹) Pleaf1
       -- ; leaf1      = λ _ → Pleaf1
       ; node1      = λ where {A = A} ll a rr ν̂ → transport (PTree1 A) (node1[] ⁻¹) (Pnode1 (ll ν̂) (a ν̂) (rr ν̂))
       -- ; node1      = λ ll a rr ν̂ → Pnode1 (ll ν̂) (a ν̂) (rr ν̂)
       ; recTree1   = λ where {B = B} b n t ν̂ → transport B (recTree1[] ⁻¹)
                                                (PrecTree1 B (b ν̂) (λ ll a rr → {!n ?!}) (t ν̂ ) )
       -- ; recTree1   = λ where {B = B} b n t ν̂ → PrecTree1 B (b ν̂) (λ ll a rr → n (ν̂ ,Σ ll ,Σ a ,Σ rr)) (t ν̂ )
       ; Tree1β₁    = {!!}
       ; Tree1β₂    = {!!}
       ; leaf1[]    = {!!}
       ; node1[]    = {!!}
       ; recTree1[] = {!!}
       }
module Can = DepAlgebra Can

