\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module Fun.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Fun.Algebra

open I

isO : ℕ → 𝟚
isO O = I
isO (S n) = O

St : Algebra
St = record
       { Con = Set
       ; STy = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; Bool = 𝟚
       ; sty = idf
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; zero = const O
       ; suc = S ∘f_
       ; isZero = isO ∘f_
       ; _+_ = λ m n γ → m γ +ℕ n γ
       ; true = const I
       ; false = const O
       ; ite = λ b u v γ → if b γ then u γ else v γ
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; isZero[] = refl
       ; +[] = refl
       ; true[] = refl
       ; false[] = refl
       ; ite[] = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; iteβ₁ = refl
       ; iteβ₂ = refl
       }
open Algebra St using (⟦_⟧ST ; ⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

---------------------------------------------------------
-- embedding for Bool and Nat
---------------------------------------------------------

⌜_⌝N : ℕ → Tm ∙ (sty Nat)
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → Tm ∙ (sty Bool)
⌜ O ⌝B = false
⌜ I ⌝B = true

⌜_⌝ : ∀ {A} → ⟦ A ⟧ST → Tm ∙ (sty A)
⌜_⌝ {Nat} = ⌜_⌝N
⌜_⌝ {Bool} = ⌜_⌝B

-- lemmas

isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝B ≡ isZero ⌜ n ⌝N
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

+-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝N ≡ ⌜ m ⌝N + ⌜ n ⌝N
+-⌜⌝ {O} = refl
+-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})

ite-⌜⌝ : ∀ {A b t f} → ⌜ if b then t else f ⌝ ≡ ite {A = sty A} ⌜ b ⌝ ⌜ t ⌝ ⌜ f ⌝
ite-⌜⌝ {b = O} = refl
ite-⌜⌝ {b = I} = refl

P-ite : {A : Ty} → {P : Tm ∙ A → Set} →
           {b : Tm ∙ (sty Bool)} → {t f : Tm ∙ A} →
           ⌜ eval b ⌝ ≡ b → P t → P f →
           P (ite b t f)
P-ite {A} {P} {b} {t} {f} eq Pt Pf with (eval b)
... | O = transport (λ a → P (ite a t f)) eq Pf
... | I = transport (λ a → P (ite a t f)) eq Pt
\end{code}
