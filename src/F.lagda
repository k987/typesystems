\chapter{Polymorphisms}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting --type-in-type #-}

module F where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

module I where
  infixl 5 _▹_
  infixl 5 _▹U
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 5 _,̇_
  infixl 6 _[_]T
  infixl 6 _[_]t
  
  postulate
    Con       : Set
    Sub       : Con → Con → Set
    Ty        : Con → Set
    Tm        : (Γ : Con) → Ty Γ → Set

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]T     : ∀{Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
    [id]T     : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    [∘]T      : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T

    _[_]t     : ∀{Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
    [id]t     : ∀{Γ A} {t : Tm Γ A} → transport (Tm Γ) [id]T (t [ id ]t) ≡ t
    [∘]t      : ∀{Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                transport (Tm Γ) [∘]T (t [ σ ∘ δ ]t) ≡ t [ σ ]t [ δ ]t

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _▹_       : (Γ : Con) → Ty Γ → Con
    _,_       : ∀{Γ Δ A} → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (A [_]T) ▹β₁) (q [ σ , t ]t) ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , transport (Tm Γ) ([∘]T ⁻¹) (q [ σ ]t) ≡ σ

    _⇒_       : ∀{Γ} → Ty Γ → Ty Γ → Ty Γ
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) (B [ p ]T) → Tm Γ (A ⇒ B)
    app       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) (B [ p ]T)
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) (B [ p ]T)} → app (lam t) ≡ t
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    ⇒[]       : ∀{Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ (A [ σ ]T ⇒ B [ σ ]T)
    lam[]     : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) (B [ p ]T)}{σ : Sub Γ Δ} →
                transport (Tm Γ) ⇒[] ((lam t) [ σ ]t) ≡
                lam (transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹ ◾ ap (B [_]T) ▹β₁ ◾ [∘]T) (t [ σ ∘ p , transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹) q ]t))

    _▹U       : Con → Con
    _,̇_       : ∀{Γ Δ}(σ : Sub Γ Δ) → Ty Γ → Sub Γ (Δ ▹U)
    ṗ         : ∀{Γ} → Sub (Γ ▹U) Γ
    q̇         : ∀{Γ} → Ty (Γ ▹U)
    ▹̇β₁       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → ṗ ∘ (σ ,̇ A) ≡ σ
    ▹̇β₂       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → q̇ [ σ ,̇ A ]T ≡ A
    ▹̇η        : ∀{Γ} → (ṗ ,̇ q̇ {Γ}) ≡ id

    ∀̇         : ∀{Γ} → Ty (Γ ▹U) → Ty Γ
    laṁ       : ∀{Γ A} → Tm (Γ ▹U) A → Tm Γ (∀̇ A)
    apṗ       : ∀{Γ A} → Tm Γ (∀̇ A) → Tm (Γ ▹U) A
    ∀β        : ∀{Γ A}{t : Tm (Γ ▹U) A} → apṗ (laṁ t) ≡ t
    ∀η        : ∀{Γ A}{t : Tm Γ (∀̇ A)} → laṁ (apṗ t) ≡ t
    ∀[]       : ∀{Γ Δ}{A : Ty (Δ ▹U)}{σ : Sub Γ Δ} → ((∀̇ A) [ σ ]T) ≡ ∀̇ (A [ σ ∘ ṗ ,̇ q̇ ]T)

    {-# REWRITE ass idl idr [id]T [∘]T [id]t [∘]t ▹β₁ ▹β₂ ▹η ⇒β ⇒η ⇒[] lam[] ▹̇β₁ ▹̇β₂ ▹̇η ∀β ∀η ∀[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixl 5 _▹U
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 5 _,̇_
  infixl 6 _[_]T
  infixl 6 _[_]t
  infixl 5 _$_
  
  field
    Con       : Set i
    Sub       : Con → Con → Set k
    Ty        : Con → Set j
    Tm        : (Γ : Con) → Ty Γ → Set l

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]T     : ∀{Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
    [id]T     : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    [∘]T      : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T

    _[_]t     : ∀{Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
    [id]t     : ∀{Γ A} {t : Tm Γ A} → transport (Tm Γ) [id]T (t [ id ]t) ≡ t
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                transport (Tm Γ) [∘]T (t [ σ ∘ δ ]t) ≡ t [ σ ]t [ δ ]t

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _▹_       : (Γ : Con) → Ty Γ → Con
    _,_       : ∀{Γ Δ A} → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (A [_]T) ▹β₁) (q [ σ , t ]t) ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , transport (Tm Γ) ([∘]T ⁻¹) (q [ σ ]t) ≡ σ

    _⇒_       : ∀{Γ} → Ty Γ → Ty Γ → Ty Γ
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) (B [ p ]T) → Tm Γ (A ⇒ B)
    app       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) (B [ p ]T)
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) (B [ p ]T)} → app (lam t) ≡ t
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    ⇒[]       : ∀{Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ (A [ σ ]T ⇒ B [ σ ]T)
    lam[]     : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) (B [ p ]T)}{σ : Sub Γ Δ} →
                transport (Tm Γ) ⇒[] ((lam t) [ σ ]t) ≡
                lam (transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹ ◾ ap (B [_]T) ▹β₁ ◾ [∘]T) (t [ σ ∘ p , transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹) q ]t))

    _▹U       : Con → Con
    _,̇_       : ∀{Γ Δ}(σ : Sub Γ Δ) → Ty Γ → Sub Γ (Δ ▹U)
    ṗ         : ∀{Γ} → Sub (Γ ▹U) Γ
    q̇         : ∀{Γ} → Ty (Γ ▹U)
    ▹̇β₁       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → ṗ ∘ (σ ,̇ A) ≡ σ
    ▹̇β₂       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → q̇ [ σ ,̇ A ]T ≡ A
    ▹̇η        : ∀{Γ} → (ṗ ,̇ q̇ {Γ}) ≡ id

    ∀̇         : ∀{Γ} → Ty (Γ ▹U) → Ty Γ
    laṁ       : ∀{Γ A} → Tm (Γ ▹U) A → Tm Γ (∀̇ A)
    apṗ       : ∀{Γ A} → Tm Γ (∀̇ A) → Tm (Γ ▹U) A
    ∀β        : ∀{Γ A}{t : Tm (Γ ▹U) A} → apṗ (laṁ t) ≡ t
    ∀η        : ∀{Γ A}{t : Tm Γ (∀̇ A)} → laṁ (apṗ t) ≡ t
    ∀[]       : ∀{Γ Δ}{A : Ty (Δ ▹U)}{σ : Sub Γ Δ} → ((∀̇ A) [ σ ]T) ≡ ∀̇ (A [ σ ∘ ṗ ,̇ q̇ ]T)

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  _$_ {Γ}{A}{B} t u = transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (B [_]T) ▹β₁ ◾ [id]T) (app t [ id , transport (Tm Γ) ([id]T ⁻¹) u ]t)

  qp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm (Γ ▹ A ▹ B) (A [ p ]T [ p ]T)
  qp = q [ p ]t
  qṗ : {Γ : Con}{A : Ty Γ} → Tm (Γ ▹ A ▹U) (A [ p ]T [ ṗ ]T)
  qṗ = q [ ṗ ]t
  qpp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)} → Tm (Γ ▹ A ▹ B ▹ C) (A [ p ]T [ p ]T [ p ]T)
  qpp = q [ p ]t [ p ]t
  qṗp : {Γ : Con}{A : Ty Γ}{C : Ty (Γ ▹ A ▹U)} → Tm (Γ ▹ A ▹U ▹ C) (A [ p ]T [ ṗ ]T [ p ]T)
  qṗp = q [ ṗ ]t [ p ]t
  qpṗ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm (Γ ▹ A ▹ B ▹U) (A [ p ]T [ p ]T [ ṗ ]T)
  qpṗ = q [ p ]t [ ṗ ]t
  qṗṗ : {Γ : Con}{A : Ty Γ} → Tm (Γ ▹ A ▹U ▹U) (A [ p ]T [ ṗ ]T [ ṗ ]T)
  qṗṗ = q [ ṗ ]t [ ṗ ]t
  qppp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)}{D : Ty (Γ ▹ A ▹ B ▹ C)} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) (A [ p ]T [ p ]T [ p ]T [ p ]T)
  qppp = q [ p ]t [ p ]t [ p ]t

  q̇p : {Γ : Con}{A : Ty (Γ ▹U)} → Ty (Γ ▹U ▹ A)
  q̇p = q̇ [ p ]T
  q̇ṗ : {Γ : Con} → Ty (Γ ▹U ▹U)
  q̇ṗ = q̇ [ ṗ ]T

  postulate
    ⟦_⟧C : I.Con → Con
    ⟦_⟧T : ∀{Γ} → I.Ty Γ → Ty ⟦ Γ ⟧C
    ⟦_⟧s : ∀{Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀{Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦∘⟧ : ∀{Γ Δ Θ}{σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧s ≡ ⟦ σ ⟧s ∘ ⟦ δ ⟧s
    ⟦id⟧ : ∀{Γ} → ⟦ I.id {Γ} ⟧s ≡ id
    {-# REWRITE ⟦∘⟧ ⟦id⟧ #-}

    ⟦[]T⟧ : ∀{Γ Δ}{A : I.Ty Δ}{σ : I.Sub Γ Δ} → ⟦ A I.[ σ ]T ⟧T ≡ ⟦ A ⟧T [ ⟦ σ ⟧s ]T
    {-# REWRITE ⟦[]T⟧ #-}
    ⟦[]t⟧ : ∀{Γ Δ A}{t : I.Tm Δ A}{σ : I.Sub Γ Δ} → ⟦ t I.[ σ ]t ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧s ]t
    {-# REWRITE ⟦[]t⟧ #-}

    ⟦∙⟧ : ⟦ I.∙ ⟧C ≡ ∙
    {-# REWRITE ⟦∙⟧ #-}
    ⟦ε⟧ : ∀{Γ} → ⟦ I.ε {Γ} ⟧s ≡ ε
    {-# REWRITE ⟦ε⟧ #-}

    ⟦▹⟧ : {Γ : I.Con}{A : I.Ty Γ} → ⟦ Γ I.▹ A ⟧C ≡ ⟦ Γ ⟧C ▹ ⟦ A ⟧T
    {-# REWRITE ⟦▹⟧ #-}
    ⟦,⟧ : ∀{Γ Δ A}{σ : I.Sub Γ Δ}{t : I.Tm Γ (A I.[ σ ]T)} → ⟦ I._,_ {A = A} σ t ⟧s ≡ ⟦ σ ⟧s , ⟦ t ⟧t
    ⟦p⟧ : ∀{Γ A} → ⟦ I.p {Γ}{A} ⟧s ≡ p
    {-# REWRITE ⟦,⟧ ⟦p⟧ #-}
    ⟦q⟧ : ∀{Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    {-# REWRITE ⟦q⟧ #-}

    ⟦⇒⟧   : ∀{Γ}{A B : I.Ty Γ} → ⟦ A I.⇒ B ⟧T ≡ ⟦ A ⟧T ⇒ ⟦ B ⟧T
    {-# REWRITE ⟦⇒⟧ #-}
    ⟦lam⟧ : ∀{Γ A B}{t : I.Tm (Γ I.▹ A) (B I.[ I.p ]T)} → ⟦ I.lam {A = A}{B} t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀{Γ A B}{t : I.Tm Γ (A I.⇒ B)} → ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦▹U⟧ : {Γ : I.Con} → ⟦ Γ I.▹U ⟧C ≡ ⟦ Γ ⟧C ▹U
    {-# REWRITE ⟦▹U⟧ #-}
    ⟦,̇⟧ : ∀{Γ Δ}{σ : I.Sub Γ Δ}{A : I.Ty Γ} → ⟦ σ I.,̇ A ⟧s ≡ ⟦ σ ⟧s ,̇ ⟦ A ⟧T
    ⟦ṗ⟧ : ∀{Γ} → ⟦ I.ṗ {Γ} ⟧s ≡ ṗ
    {-# REWRITE ⟦,̇⟧ ⟦ṗ⟧ #-}
    ⟦q̇⟧ : ∀{Γ} → ⟦ I.q̇ {Γ} ⟧T ≡ q̇
    {-# REWRITE ⟦q̇⟧ #-}

    ⟦∀̇⟧ : ∀{Γ}{A : I.Ty (Γ I.▹U)} → ⟦ I.∀̇ A ⟧T ≡ ∀̇ ⟦ A ⟧T
    {-# REWRITE ⟦∀̇⟧ #-}
    ⟦laṁ⟧ : ∀{Γ A}{t : I.Tm (Γ I.▹U) A} → ⟦ I.laṁ t ⟧t ≡ laṁ ⟦ t ⟧t
    ⟦apṗ⟧ : ∀{Γ A}{t : I.Tm Γ (I.∀̇ A)} → ⟦ I.apṗ t ⟧t ≡ apṗ ⟦ t ⟧t
    {-# REWRITE ⟦laṁ⟧ ⟦apṗ⟧ #-}

St : Algebra
St = record
  { Con = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; Ty = λ Γ → Γ → Set
  ; Tm = λ Γ A → (γ : Γ) → A γ
  ; _∘_ = λ σ δ γ → σ (δ γ)
  ; id = λ γ → γ
  ; ass = refl
  ; idl = refl
  ; idr = refl
  ; _[_]T = λ A σ γ → A (σ γ)
  ; [id]T = refl
  ; [∘]T = refl
  ; _[_]t = λ t σ γ → t (σ γ)
  ; [id]t = refl
  ; [∘] = refl
  ; ∙ = ↑p 𝟙
  ; ε = λ _ → ↑[ * ]↑
  ; ∙η = refl
  ; _▹_ = Σ
  ; _,_ = λ σ t γ → σ γ ,Σ t γ
  ; p = π₁
  ; q = π₂
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; _⇒_ = λ A B γ → A γ → B γ
  ; lam = λ t γ α → t (γ ,Σ α)
  ; app = λ t γ → t (π₁ γ) (π₂ γ)
  ; ⇒β = refl
  ; ⇒η = refl
  ; ⇒[] = refl
  ; lam[] = refl
  ; _▹U = λ Γ → Γ × Set
  ; _,̇_ = λ σ A γ → σ γ ,Σ A γ
  ; ṗ = π₁
  ; q̇ = π₂
  ; ▹̇β₁ = refl
  ; ▹̇β₂ = refl
  ; ▹̇η = refl
  ; ∀̇ = λ A γ → (X : Set) → A (γ ,Σ X)
  ; laṁ = λ t γ X → t (γ ,Σ X)
  ; apṗ = λ t γ → t (π₁ γ) (π₂ γ)
  ; ∀β = refl
  ; ∀η = refl
  ; ∀[] = refl
  }
module St = Algebra St

norm : ∀{A} → I.Tm I.∙ A → St.⟦ A ⟧T ↑[ * ]↑
norm t = St.⟦ t ⟧t ↑[ * ]↑

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixl 5 _▹U
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 5 _,̇_
  infixl 6 _[_]T
  infixl 6 _[_]t
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Sub : ∀{Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Ty  : ∀{Γ'} → Con Γ' → I.Ty Γ' → Set j
    Tm  : ∀{Γ' A'}(Γ : Con Γ') → Ty Γ A' → I.Tm Γ' A' → Set l

    _∘_ : ∀{Γ' Δ' Θ' σ' δ'}{Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
          Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id  : ∀{Γ'}{Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀{Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀{Γ' Δ' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀{Γ' Δ' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
      
    _[_]T     : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}(A : Ty Δ A')(σ : Sub Γ Δ σ') → Ty Γ (A' I.[ σ' ]T)
    [id]T     : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty Γ A'} → A [ id ]T ≡ A
    [∘]T      : ∀{Γ' Δ' Θ' A' σ' δ'}{Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty Θ A'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
                A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T

    _[_]t     : ∀{Γ' Δ' A' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty Δ A'}(t : Tm Δ A t')(σ : Sub Γ Δ σ') → Tm Γ (A [ σ ]T) (t' I.[ σ' ]t)
    [id]t     : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty Γ A'}{t : Tm Γ A t'} → coe (ap2 (Tm Γ) [id]T I.[id]t) (t [ id ]t) ≡ t
    [∘]t      : ∀{Γ' Δ' Θ' A' t' σ' δ'}{Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty Θ A'}{t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
                coe (ap2 (Tm Γ) [∘]T I.[∘]t) (t [ σ ∘ δ ]t) ≡ t [ σ ]t [ δ ]t

    ∙   : Con I.∙
    ε   : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η  : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
          σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀{Γ' A'}(Γ : Con Γ') → Ty Γ A' → Con (Γ' I.▹ A')
    _,_ : ∀{Γ' Δ' A' σ' t'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty Δ A'}(σ : Sub Γ Δ σ')(t : Tm Γ (A [ σ ]T) t') → Sub Γ (Δ ▹ A) (σ' I., t')
    p   : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty Γ A'} → Sub (Γ ▹ A) Γ I.p
    q   : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty Γ A'} → Tm (Γ ▹ A) (A [ p ]T) I.q
    ▹β₁ : ∀{Γ' Δ' A' σ' t'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty Δ A'}{σ : Sub Γ Δ σ'}{t : Tm Γ (A [ σ ]T) t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀{Γ' Δ' A' σ' t'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty Δ A'}{σ : Sub Γ Δ σ'}{t : Tm Γ (A [ σ ]T) t'} →
          coe (apd3 (λ A' → Tm {_}{A'} Γ) (I.[∘]T {A = A'}{σ = I.p {Δ'}{A'}}{δ = σ' I., t'} ⁻¹) {![∘]T {A = A}{p}{σ , t} ⁻¹ ◾ ap (A [_]T) ▹β₁!} {!I.▹β₂!}) (q [ σ , t ]t) ≡ t

{-    
transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (A [_]T) ▹β₁)
    ▹η  : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , transport (Tm Γ) ([∘]T ⁻¹) (q [ σ ]t) ≡ σ
-}
\end{code}
