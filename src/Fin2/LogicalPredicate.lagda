\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fin2.LogicalPredicate where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _,Σ_)
open import Fin2.Algebra
open import Fin2.Standard

open I
open Algebra St using (⟦_⟧T ; ⟦_⟧t)
\end{code}
\begin{code}
data _P+_ {i j A' B'} (A : Tm ∙ A' → Set i)(B : Tm ∙ B' → Set j)
        : Tm ∙ (A' + B') → Set (i ⊔ j) where
 Pinl : ∀ {a'} → A a' → (A P+ B) (inl a')
 Pinr : ∀ {b'} → B b' → (A P+ B) (inr b')

indP+ : ∀ {i j k A' B'} (A : Tm ∙ A' → Set i)(B : Tm ∙ B' → Set j)
  (P : ∀ {t'} → (A P+ B) t' → Set k) →
  (∀ {a'} (a : A a') → P (Pinl a)) → (∀ {b'} (b : B b') → P (Pinr b)) →
  ∀ {t'} (t : (A P+ B) t') → P t
indP+ A B P u v (Pinl a) = u a
indP+ A B P u v (Pinr b) = v b

recP+ : ∀ {i j k A' B'} (A : Tm ∙ A' → Set i)(B : Tm ∙ B' → Set j)
  (C : Tm ∙ (A' + B') → Set k) →
  (∀ {a'} → A a' → C (inl a')) → (∀ {b'} → B b' → C (inr b')) →
  ∀ {t'} → (A P+ B) t' → C t'
recP+ A B C u v t = indP+ A B (λ {t'} _ → C t') u v t

+-Prec : ∀ {i j k A' B' C'}
  (A : Tm ∙ A' → Set i)(B : Tm ∙ B' → Set j)(C : Tm ∙ C' → Set k)
  {u' : Tm (∙ ▹ A') C'}{v' : Tm (∙ ▹ B') C'} →
  (∀ {a'} → A a' → C (u' [ id , a' ])) →
  (∀ {b'} → B b' → C (v' [ id , b' ])) →
  ∀ {t'} → (A P+ B) t' → C (case t' u' v')
+-Prec A B C {u'}{v'} u v t = recP+ A B (λ t' → C (case t' u' v')) u v t

LP : DepAlgebra
LP = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') ×m A (q [ σ' ])
         ; Unit = λ _ → ↑p 𝟙
         ; Empty = λ _ → ↑p 𝟘
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _×_ = λ A B t' → A (proj₁ t') ×m B (proj₂ t')
         ; _+_ = _P+_
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; tt = λ _ → *↑
         ; ⟨_,_⟩ = λ u v x → u x ,Σ v x
         ; proj₁ = λ t x → π₁ (t x)
         ; proj₂ = λ t x → π₂ (t x)
         ; absurd = λ t x → ⟦ ↓[ t x ]↓ ⟧𝟘
         ; inl = λ t x → Pinl (t x)
         ; inr = λ t x → Pinr (t x)
         ; case = λ { {A = A}{B}{C} t u v {ν'} x →
                    +-Prec A B C (λ a → u (x ,Σ a))
                                 (λ b → v (x ,Σ b))
                                 (t x) }
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; uη = refl
         ; ×β₁ = refl
         ; ×β₂ = refl
         ; ×η = refl
         ; eη = {!!}
         ; +β₁ = refl
         ; +β₂ = refl
         ; +η = {!!}
         ; lam[] = refl
         ; tt[] = refl
         ; ⟨,⟩[] = refl
         ; absurd[] = refl
         ; inl[] = refl
         ; inr[] = refl
         ; case[] = refl
         }
module LP = DepAlgebra LP

p+ : ∀ {A B} (t : Tm ∙ (A + B)) → (LP.⟦ A ⟧T P+ LP.⟦ B ⟧T) t
p+ t = LP.⟦ t ⟧t {id} *↑

canonicity+ : ∀ {A B} {t : Tm ∙ (A + B)} →
  Σ (Tm ∙ A) (λ a → ↑p (t ≡ inl a)) ⊎ Σ (Tm ∙ B) (λ b → ↑p (t ≡ inr b))
canonicity+ {A}{B}{t} =
  let C t = Σ (Tm ∙ A) (λ a → ↑p (t ≡ inl a)) ⊎
            Σ (Tm ∙ B) (λ b → ↑p (t ≡ inr b))
  in  recP+ LP.⟦ A ⟧T LP.⟦ B ⟧T C
            (λ {a'} _ → ι₁ (a' ,Σ refl↑))
            (λ {b'} _ → ι₂ (b' ,Σ refl↑))
            (p+ t)
\end{code}
