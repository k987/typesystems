{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I
open import Full.DepAlgebra
-- open import Full.Standard

module Full.Canonicity where

open I

-- Bool

data PBool : Tm ∙ Bool → Set where
  Ptrue  : PBool true
  Pfalse : PBool false

Pite :
  {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) → 
  PBool t' → A u' → A v' → A (ite t' u' v')
Pite {u' = u'} {v'} A Ptrue  u v = u
Pite {u' = u'} {v'} A Pfalse u v = v

PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
PcanBool Ptrue  = ι₁ ↑[ refl ]↑
PcanBool Pfalse = ι₂ ↑[ refl ]↑

-- Nat

data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀{n'} → PNat n' → PNat (suc n')

PrecNat :
  {A' : Ty}{u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'}{t' : Tm ∙ Nat}
  (A : Tm ∙ A' → Set)(u : A u')(v : ∀{w'} → A w' → A (v' [ id , w' ]))
  (t : PNat t') →
  A (recNat u' v' t')
PrecNat {A'}{u'}{v'}{.zero}    A u v Pzero    = u
PrecNat {A'}{u'}{v'}{.(suc _)} A u v (Psuc t) = v (PrecNat A u v t)

PcanNat : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat Pzero    = ι₁ ↑[ refl ]↑
PcanNat (Psuc t) with PcanNat t
... | ι₁ e = ι₂ (zero ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
... | ι₂ (t' ,Σ e) = ι₂ (suc t' ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)

-- Prod

data PProd {A' B' : Ty}(A : Tm ∙ A' → Set)(B : Tm ∙ B' → Set) : Tm ∙ (Prod A' B') → Set where
   Ppair : {u' : Tm ∙ A'}{v' : Tm ∙ B'} → A u' → B v' → PProd A B (pair u' v')

PrecProd :
  {A' B' C' : Ty}{u' : Tm (∙ ▹ A' ▹ B') C'}{t' : Tm ∙ (Prod A' B')}
  {A : Tm ∙ A' → Set}{B : Tm ∙ B' → Set}(C : Tm ∙ C' → Set)
  (u : ∀{w₁'}{w₂'} → A w₁' → B w₂' → C (u' [ id , w₁' , w₂' ]))
  (t : PProd A B t') → C (recProd u' t')
PrecProd C u (Ppair w₁ w₂) = u w₁ w₂

-- +

data P+ {A' B' : Ty}(A : Tm ∙ A' → Set)(B : Tm ∙ B' → Set) : Tm ∙ (A' + B') → Set where
   Pinl : {t' : Tm ∙ A'} → A t' → P+ A B (inl t')
   Pinr : {t' : Tm ∙ B'} → B t' → P+ A B (inr t')

Pcase :
  {A' B' C' : Ty}{t' : Tm ∙ (A' + B')}{u' : Tm (∙ ▹ A') C'}{v' : Tm (∙ ▹ B') C'}
  {A : Tm ∙ A' → Set}{B : Tm ∙ B' → Set}(C : Tm ∙ C' → Set)
  (t : P+ A B t')(u : ∀{w'} → A w' → C (u' [ id , w' ]))(v : ∀{w'} → B w' → C (v' [ id , w' ])) →
  C (case t' u' v')
Pcase C (Pinl t) u v = u t
Pcase C (Pinr t) u v = v t

-- List

data PList {A'}(A : Tm ∙ A' → Set) : Tm ∙ (List A') → Set where
  Pnil : PList A nil
  Pcons : ∀{u' t'} → A u' → PList A t' → PList A (cons u' t')

PrecList :
  {A' B' : Ty}{u' : Tm ∙ B'}{v' : Tm (∙ ▹ A' ▹ B') B'}{t' : Tm ∙ (List A')}
  {A : Tm ∙ A' → Set}(B : Tm ∙ B' → Set)(u : B u')(v : ∀{w₁' w₂'} → A w₁' → B w₂' → B (v' [ id , w₁' , w₂' ]))(t : PList A t') →
  B (recList u' v' t')
PrecList B u v Pnil = u
PrecList B u v (Pcons w t) = v w (PrecList B u v t)

-- Tree

data PTree : Tm ∙ Tree → Set where
  Pleaf : PTree leaf
  Pnode : ∀{u' v'} → PTree u' → PTree v' → PTree (node u' v')

PrecTree : 
  {A' : Ty}{u' : Tm ∙ A'}{v' : Tm (∙ ▹ A' ▹ A') A'}{t' : Tm ∙ Tree}
  (A : Tm ∙ A' → Set)(u : A u')(v : ∀{w₁' w₂'} → A w₁' → A w₂' → A (v' [ id , w₁' , w₂' ]))
  (t : PTree t') →
  A (recTree u' v' t')
PrecTree A u v Pleaf = u
PrecTree A u v (Pnode t₁ t₂) = v (PrecTree A u v t₁) (PrecTree A u v t₂)

Can : DepAlgebra
Can = record
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = refl
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
       ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
       
       ; Bool       = PBool
       ; true       = λ _ → Ptrue
       ; false      = λ _ → Pfalse
       ; ite        = λ {Γ'}{A'}{t'}{u'}{v'}{Γ}{A} t u v {ν'} ν̂ → Pite A (t ν̂) (u ν̂) (v ν̂)
       ; iteβ₁      = refl
       ; iteβ₂      = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
       
       ; Nat        = PNat
       ; zero       = λ _ → Pzero
       ; suc        = λ t ν̂ → Psuc (t ν̂)
       ; recNat     = λ {_}{_}{_}{_}{_}{_}{A} u v t {ν'} ν̂ → PrecNat A (u ν̂) (λ ŵ → v (ν̂ ,Σ ŵ)) (t ν̂)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
       
       ; _⇒_        = λ {A'}{B'} A B t' → (u' : Tm ∙ A') → A u' → B (t' $ u')
       ; lam        = λ t {ν'} ν̂ u' û → t {ν' , u'} (ν̂ ,Σ û)
       ; app        = λ t {ν'} ν̂ → t {p ∘ ν'} (π₁ ν̂) (q [ ν' ]) (π₂ ν̂)
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       
       ; Unit       = λ _ → ↑p 𝟙
       ; tt         = _
       ; Unitη      = refl
       
       ; _×_        = λ A B t' → A (proj₁ t') ⊗ B (proj₂ t')
       ; ⟨_,_⟩      = λ u v ν̂ → u ν̂ ,Σ v ν̂
       ; proj₁      = λ t ν̂ → π₁ (t ν̂)
       ; proj₂      = λ t ν̂ → π₂ (t ν̂)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
       
       ; Prod       = PProd
       ; pair       = λ u v {ν'} ν̂ → Ppair (u ν̂) (v ν̂)
       ; recProd    = λ {_}{_}{_}{_}{_}{_}{_}{_}{_}{C} u t ν̂ → PrecProd C (λ w₁ w₂ → u (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Prodβ      = refl
       ; pair[]     = refl
       ; recProd[]  = refl
       
       ; Empty      = λ _ → ↑p 𝟘
       ; absurd     = λ t ν̂ → ⟦ ↓[ t ν̂ ]↓ ⟧𝟘
       ; absurd[]   = refl
       
       ; _+_        = P+
       ; inl        = λ t ν̂ → Pinl (t ν̂)
       ; inr        = λ t ν̂ → Pinr (t ν̂)
       ; case       = λ {_}{_}{_}{_}{_}{_}{_}{_}{_}{_}{C} t u v {ν'} ν̂ → Pcase C (t ν̂) (λ w → u (ν̂ ,Σ w)) (λ w → v (ν̂ ,Σ w))
       ; +β₁        = refl
       ; +β₂        = refl
       ; inl[]      = refl
       ; inr[]      = refl
       ; case[]     = refl
       
       ; List       = PList
       ; nil        = λ _ → Pnil
       ; cons       = λ u t ν̂ → Pcons (u ν̂) (t ν̂)
       ; recList    = λ {_}{_}{_}{_}{_}{_}{_}{_}{B} u v t ν̂ → PrecList B (u ν̂) (λ w₁ w₂ → v (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; recList[]  = refl
       
       ; Tree       = PTree
       ; leaf       = λ _ → Pleaf
       ; node       = λ u v ν̂ → Pnode (u ν̂) (v ν̂)
       ; recTree    = λ {_}{_}{_}{_}{_}{_}{A} u v t ν̂ → PrecTree A (u ν̂) (λ w₁ w₂ → v (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Treeβ₁     = refl
       ; Treeβ₂     = refl
       ; leaf[]     = refl
       ; node[]     = refl
       ; recTree[]  = refl
       
       ; Tree1      = {!!}
       ; leaf1      = {!!}
       ; node1      = {!!}
       ; recTree1   = {!!}
       ; Tree1β₁    = {!!}
       ; Tree1β₂    = {!!}
       ; leaf1[]    = {!!}
       ; node1[]    = {!!}
       ; recTree1[] = {!!}
       
       ; Tree2      = {!!}
       ; leaf2      = {!!}
       ; node2      = {!!}
       ; recTree2   = {!!}
       ; Tree2β₁    = {!!}
       ; Tree2β₂    = {!!}
       ; leaf2[]    = {!!}
       ; node2[]    = {!!}
       ; recTree2[] = {!!}
       }
module Can = DepAlgebra Can

open I

canBool : (t : Tm ∙ Bool) → ↑p (t ≡ true) ⊎ ↑p (t ≡ false)
canBool t = PcanBool (Can.⟦ t ⟧t {id} ↑[ * ]↑)

canNat : (t : Tm ∙ Nat) → ↑p (t ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t ≡ suc u)
canNat t = PcanNat (Can.⟦ t ⟧t {id} ↑[ * ]↑)

can⇒ : ∀{A B}(t : Tm ∙ (A ⇒ B)) → Σ (Tm (∙ ▹ A) B) λ u → ↑p (t ≡ lam u)
can⇒ t = app t ,Σ ↑[ refl ]↑

canUnit : (t : Tm ∙ Unit) → ↑p (t ≡ tt)
canUnit t = ↑[ Unitη ]↑

can× : ∀{A B}(t : Tm ∙ (A × B)) → Σ (Tm ∙ A) λ u → Σ (Tm ∙ B) λ v → ↑p (t ≡ ⟨ u , v ⟩)
can× t = proj₁ t ,Σ (proj₂ t ,Σ ↑[ refl ]↑)

canProd : ∀{A B}(t : Tm ∙ (Prod A B)) → Σ (Tm ∙ A) λ u → Σ (Tm ∙ B) λ v → ↑p (t ≡ pair u v)
canProd t = {!!}

canEmpty : (t : Tm ∙ Empty) → 𝟘
canEmpty t = ↓[ Can.⟦ t ⟧t {id} ↑[ * ]↑ ]↓

can+ : ∀{A B}(t : Tm ∙ (A + B)) →
  (Σ (Tm ∙ A) λ u → ↑p (t ≡ inl u)) ⊎ (Σ (Tm ∙ B) λ v → ↑p (t ≡ inr v))
can+ t = {!!}

canList : ∀{A}(t : Tm ∙ (List A)) →
  ↑p (t ≡ nil) ⊎ Σ (Tm ∙ A) λ u → Σ (Tm ∙ (List A)) λ v → ↑p (t ≡ cons u v)
canList t = {!!}

canTree : (t : Tm ∙ Tree) →
  ↑p (t ≡ leaf) ⊎ Σ (Tm ∙ Tree) λ u → Σ (Tm ∙ Tree) λ v → ↑p (t ≡ node u v)
canTree t = {!!}

canTree1 : ∀{A}(t : Tm ∙ (Tree1 A)) →
  ↑p (t ≡ leaf1) ⊎ Σ (Tm ∙ (Tree1 A)) λ t₁ → Σ (Tm ∙ A) λ w → Σ (Tm ∙ (Tree1 A)) λ t₂ → ↑p (t ≡ node1 t₁ w t₂)
canTree1 t = {!!}

canTree2 : ∀{A B}(t : Tm ∙ (Tree2 A B)) →
  (Σ (Tm ∙ B) λ u → ↑p (t ≡ leaf2 u)) ⊎ Σ (Tm ∙ (Tree2 A B)) λ t₁ → Σ (Tm ∙ A) λ w → Σ (Tm ∙ (Tree2 A B )) λ t₂ → ↑p (t ≡ node2 t₁ w t₂)
canTree2 t = {!!}
