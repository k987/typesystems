{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I
open import Full.Algebra
open import Full.Standard

module Full.Stability where

open I
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

stabilityB : ∀ {b} → norm ⌜ b ⌝B ≡ b
stabilityB {O} = refl
stabilityB {I} = refl

stabilityN : ∀ {n} → norm ⌜ n ⌝N ≡ n
stabilityN {O} = refl
stabilityN {S n} = cong S stabilityN

stabilityT : ∀ {t} → norm ⌜ t ⌝T ≡ t
stabilityT {Leaf} = refl
stabilityT {Node l r} = cong-2 Node stabilityT stabilityT
