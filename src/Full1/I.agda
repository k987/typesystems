{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module Full.I where

module I where
  data Ty      : Set where
    Bool       : Ty
    Nat        : Ty
    _⇒_        : Ty → Ty → Ty
    Unit       : Ty
    _×_        : Ty → Ty → Ty
    Prod       : Ty → Ty → Ty
    Empty      : Ty
    _+_        : Ty → Ty → Ty
    List       : Ty → Ty
    Tree       : Ty
    Tree1      : Ty → Ty
    Tree2      : Ty → Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_
  infixl 6 _+_

  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁      : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂      : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt         : ∀{Γ} → Tm Γ Unit
    Unitη      : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt

    ⟨_,_⟩      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η         : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]      : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    pair       : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    recProd    : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B) C → Tm Γ (Prod A B) → Tm Γ C
    Prodβ      : ∀{Γ A B C}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} →
                 recProd t (pair u v) ≡ (t [ id , u , v ])
    pair[]     : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
    recProd[]  : ∀{Γ Δ A B C}{t : Tm (Δ ▹ A ▹ B) C}{u : Tm Δ (Prod A B)}{σ : Sub Γ Δ} →
                 recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

    absurd     : ∀{Γ A} → Tm Γ Empty → Tm Γ A
    absurd[]   : ∀{Γ Δ A}{t : Tm Δ Empty}{σ : Sub Γ Δ} →
                 (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])

    inl        : ∀{Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr        : ∀{Γ A B} → Tm Γ B → Tm Γ (A + B)
    case       : ∀{Γ A B C} → Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
    +β₁        : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inl t) u v ≡ u [ id , t ]
    +β₂        : ∀{Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inr t) u v ≡ v [ id , t ]
    inl[]      : ∀{Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} → (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[]      : ∀{Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} → (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[]     : ∀{Γ Δ A B C}{t : Tm Δ (A + B)}{u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
                 (case t u v) [ σ ] ≡ case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    recList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ Δ A}{σ : Sub Γ Δ} → nil {Δ}{A} [ σ ] ≡ nil {Γ}{A}
    cons[]     : ∀{Γ Δ A}{t₁ : Tm Δ A}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ A ▹ B) B}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf       : ∀{Γ} → Tm Γ Tree
    node       : ∀{Γ} → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
    recTree    : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A ▹ A) A → Tm Γ Tree → Tm Γ A
    Treeβ₁     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A} → recTree u v leaf ≡ u
    Treeβ₂     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A}{t t₁ : Tm Γ Tree} →
                 recTree u v (node t t₁) ≡ v [ id , recTree u v t , recTree u v t₁ ]
    leaf[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → leaf [ σ ] ≡ leaf
    node[]     : ∀{Γ Δ}{t t₁ : Tm Δ Tree}{σ : Sub Γ Δ} → (node t t₁) [ σ ] ≡ node (t [ σ ]) (t₁ [ σ ])
    recTree[]  : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A ▹ A) A}{t : Tm Δ Tree}{σ : Sub Γ Δ} →
                 recTree u v t [ σ ] ≡ recTree (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf1      : ∀{Γ A} → Tm Γ (Tree1 A)
    node1      : ∀{Γ A} → Tm Γ (Tree1 A) → Tm Γ A → Tm Γ (Tree1 A) → Tm Γ (Tree1 A)
    recTree1   : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A ▹ B) B → Tm Γ (Tree1 A) → Tm Γ B
    Tree1β₁    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B} → recTree1 u v leaf1 ≡ u
    Tree1β₂    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B}{t₁ t₂ : Tm Γ (Tree1 A)}{w : Tm Γ A} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]    : ∀{Γ Δ A}{σ : Sub Γ Δ} → leaf1 {Δ}{A} [ σ ] ≡ leaf1
    node1[]    : ∀{Γ Δ A}{t₁ t₂ : Tm Δ (Tree1 A)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[] : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ B ▹ A ▹ B) B}{t : Tm Δ (Tree1 A)}{σ : Sub Γ Δ} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf2      : ∀{Γ A B} → Tm Γ B → Tm Γ (Tree2 A B)
    node2      : ∀{Γ A B} → Tm Γ (Tree2 A B) → Tm Γ A → Tm Γ (Tree2 A B) → Tm Γ (Tree2 A B)
    recTree2   : ∀{Γ A B C} → Tm (Γ ▹ B) C → Tm (Γ ▹ C ▹ A ▹ C) C → Tm Γ (Tree2 A B) → Tm Γ C
    Tree2β₁    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t : Tm Γ B} → recTree2 u v (leaf2 t) ≡ u [ id , t ]
    Tree2β₂    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t₁ t₂ : Tm Γ (Tree2 A B)}{w : Tm Γ A} →
                 recTree2 u v (node2 t₁ w t₂) ≡ v [ id , recTree2 u v t₁ , w , recTree2 u v t₂ ]
    leaf2[]    : ∀{Γ Δ B A}{t : Tm Δ B}{σ : Sub Γ Δ} → leaf2 {A = A} t [ σ ] ≡ leaf2 (t [ σ ])
    node2[]    : ∀{Γ Δ B A}{t₁ t₂ : Tm Δ (Tree2 A B)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node2 t₁ w t₂) [ σ ] ≡ node2 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree2[] : ∀{Γ Δ A B C}{u : Tm (Δ ▹ B) C}{v : Tm (Δ ▹ C ▹ A ▹ C) C}{t : Tm Δ (Tree2 A B)}{σ : Sub Γ Δ} →
                 recTree2 u v t [ σ ] ≡ recTree2 (u [ σ ∘ p , q ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  tt[] : ∀{Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt
  tt[] = Unitη

  proj₁[] : ∀{Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    proj₁ t [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {Γ}{Δ}{A}{B}{t}{σ} = ×β₁ ⁻¹ ◾ ap proj₁ ⟨,⟩[] ⁻¹ ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀{Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    proj₂ t [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {Γ}{Δ}{A}{B}{t}{σ} = ×β₂ ⁻¹ ◾ ap proj₂ ⟨,⟩[] ⁻¹ ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE iteβ₁ iteβ₂ true[] false[] ite[] #-}
  {-# REWRITE Natβ₁ Natβ₂  zero[] suc[] recNat[] #-}
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}
  {-# REWRITE tt[] #-}
  {-# REWRITE ×β₁ ×β₂ ×η ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE Prodβ pair[] recProd[] #-}
  {-# REWRITE absurd[] #-}
  {-# REWRITE +β₁ +β₂ inl[] inr[] case[] #-}
  {-# REWRITE Listβ₁ Listβ₂ nil[] cons[] recList[] #-}
  {-# REWRITE Treeβ₁ Treeβ₂ leaf[] node[] recTree[] #-}
  {-# REWRITE Tree1β₁ Tree1β₂ leaf1[] node1[] recTree1[] #-}
  {-# REWRITE Tree2β₁ Tree2β₂ leaf2[] node2[] recTree2[] #-}
