{-# OPTIONS --prop --rewriting #-}

import Full.I
import Full.Algebra
import Full.DepAlgebra
import Full.Standard
import Full.Canonicity
import Full.Completeness
import Full.Stability

module Full.Main where
