{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I
open import Full.Algebra

open I

module Full.Standard where

St : Algebra
St = record
       { Con        = Set
       ; Sub        = λ Γ Δ → Γ → Δ
       ; Ty         = Set
       ; Tm         = λ Γ A → Γ → A
       ; _∘_        = _∘f_
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = _∘f_
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = ↑p 𝟙
       ; ε          = const *↑
       ; ∙η         = refl
       ; _▹_        = _⊗_
       ; _,_        = λ σ t γ → σ γ ,Σ t γ
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
       ; Bool       = 𝟚
       ; true       = const I
       ; false      = const O
       ; ite        = λ b t f γ → if b γ then t γ else f γ
       ; iteβ₁      = refl
       ; iteβ₂      = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
       ; Nat        = ℕ
       ; zero       = const O
       ; suc        = S ∘f_
       ; recNat     = λ z s n γ → rec-ℕ (z γ) (λ n → s (γ ,Σ n)) (n γ)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
       ; _⇒_        = λ A B → A → B
       ; lam        = λ t γ a → t (γ ,Σ a)
       ; app        = λ { t (γ ,Σ a) → t γ a }
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       ; Unit       = ↑p 𝟙
       ; tt         = const *↑
       ; Unitη      = refl
       ; _×_        = _⊗_
       ; ⟨_,_⟩      = λ a b γ → (a γ ,Σ b γ)
       ; proj₁      = λ t γ → π₁ (t γ)
       ; proj₂      = λ t γ → π₂ (t γ)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
       ; Prod       = _⊗_
       ; pair       = λ a b γ → (a γ ,Σ b γ)
       ; recProd    = λ f ab γ → rec-× (λ { (a ,Σ b) → f (γ ,Σ a ,Σ b) }) (ab γ)
       ; Prodβ      = refl
       ; pair[]     = refl
       ; recProd[]  = refl
       ; Empty      = ↑p 𝟘
       ; absurd     = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; absurd[]   = refl
       ; _+_        = λ A B → A ⊎ B
       ; inl        = λ t γ → ι₁ (t γ)
       ; inr        = λ t γ → ι₂ (t γ)
       ; case       = λ ab af bf γ → case⊎ (ab γ) (λ a → af (γ ,Σ a)) (λ b → bf (γ ,Σ b))
       ; +β₁        = refl
       ; +β₂        = refl
       ; inl[]      = refl
       ; inr[]      = refl
       ; case[]     = refl
       ; List       = λ A → ⁅ A ⁆
       ; nil        = λ Γ → ⁅⁆
       ; cons       = λ ts t γ → ts γ ∷ t γ
       ; recList    = λ b f l γ → rec-⁅⁆ (b γ) (λ a b → f (γ ,Σ b ,Σ a)) (l γ)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; recList[]  = refl
       ; Tree       = 𝕋
       ; leaf       = const Leaf
       ; node       = λ ll rr γ → Node (ll γ) (rr γ)
       ; recTree    = λ l n t γ → rec-𝕋 (l γ) (λ ll rr → n (γ ,Σ ll ,Σ rr)) (t γ)
       ; Treeβ₁     = refl
       ; Treeβ₂     = refl
       ; leaf[]     = refl
       ; node[]     = refl
       ; recTree[]  = refl
       ; Tree1      = 𝕋1
       ; leaf1      = const Leaf
       ; node1      = λ ll a rr γ → Node (ll γ) (a γ) (rr γ)
       ; recTree1   = λ l n t γ → rec-𝕋1 (l γ) (λ ll a rr → n (γ ,Σ ll ,Σ a ,Σ rr)) (t γ)
       ; Tree1β₁    = refl
       ; Tree1β₂    = refl
       ; leaf1[]    = refl
       ; node1[]    = refl
       ; recTree1[] = refl
       ; Tree2      = 𝕋2
       ; leaf2      = λ b γ → Leaf (b γ)
       ; node2      = λ ll a rr γ → Node (ll γ) (a γ) (rr γ)
       ; recTree2   = λ l n t γ → rec-𝕋2 (λ b → l (γ ,Σ b)) (λ ll a rr → n (γ ,Σ ll ,Σ a ,Σ rr)) (t γ)
       ; Tree2β₁    = refl
       ; Tree2β₂    = refl
       ; leaf2[]    = refl
       ; node2[]    = refl
       ; recTree2[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

norm : {A : Ty} → Tm ∙ A → ⟦ A ⟧T
norm t = ⟦ t ⟧t *↑

⌜_⌝B : 𝟚 → Tm ∙ Bool
⌜ O ⌝B = false
⌜ I ⌝B = true

⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝T : 𝕋 → Tm ∙ Tree
⌜ Leaf ⌝T = leaf
⌜ Node l r ⌝T = node ⌜ l ⌝T ⌜ r ⌝T

P-ite : {A : Ty} → {P : Tm ∙ A → Set} →
           {b : Tm ∙ Bool} → {t f : Tm ∙ A} →
           ⌜ norm b ⌝B ≡ b → P t → P f →
           P (ite b t f)
P-ite {A} {P} {b} {t} {f} eq Pt Pf with (norm b)
... | O = transport (λ a → P (ite a t f)) eq Pf
... | I = transport (λ a → P (ite a t f)) eq Pt
