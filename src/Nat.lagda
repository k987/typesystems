\subsection{Another example of recursion and induction: natural numbers}
\label{sec:natural}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Nat where
open import Lib
\end{code}
A model of naturals is a set \verb$Nat$ with an element \verb$Zero$ and an endofunction \verb$Suc$.
\begin{code}
record Model {i} : Set (lsuc i) where
  field
    Nat   : Set i
    Zero  : Nat
    Suc   : Nat → Nat
\end{code}
\begin{code}[hide]
  ⟦_⟧ : ℕ → Nat
  ⟦ zero ⟧ = Zero
  ⟦ suc n ⟧ = Suc ⟦ n ⟧
\end{code}
There is a model \verb$I$ given by actual natural numbers.
\begin{code}
I : Model
I = record { Nat = ℕ ; Zero = 0 ; Suc = 1 +_ }
\end{code}
\begin{code}[hide]
module I = Model I
\end{code}
For any other model \verb$M$, we have a function from \verb$I.Nat$ to \verb$M.Nat$ which respects the two operations.
\begin{verbatim}
M.⟦_⟧ : I.Nat → M.Nat
M.⟦ I.Zero ⟧ = M.Zero
M.⟦ I.Suc n ⟧ = M.Suc M.⟦ n ⟧
\end{verbatim}
We define the following model where \verb$Nat$ is syntactic natural numbers.
\begin{code}
M : Model
M = record { Nat = I.Nat ; Zero = I.Suc I.Zero ; Suc = λ n → I.Suc (I.Suc n) }
\end{code}
\begin{code}[hide]
module M = Model M
\end{code}
Interpretation into \verb$M$ is the function $n \mapsto 2*n+1$:
\begin{code}
testM0 : M.⟦ 0 ⟧  ≡ 1
testM1 : M.⟦ 1 ⟧  ≡ 3
testM2 : M.⟦ 2 ⟧  ≡ 5
\end{code}
\begin{code}[hide]
testM0 = refl {x = 1}
testM1 = refl {x = 3}
testM2 = refl {x = 5}
\end{code}
Now we define a model where \verb$Nat$ is endofunctions on \verb$I.Nat$, \verb$Zero$ is the identity function, and \verb$Suc$ is postcomposition with \verb$I.Suc$.
\begin{code}
A : Model
A = record { Nat = I.Nat → I.Nat ; Zero = λ n → n ; Suc = λ f → I.Suc ∘ f }
\end{code}
\begin{code}[hide]
module A = Model A
\end{code}
Interpretation into \verb$A$ is the function that maps $n$ into the function which adds $n$ to a number:
\begin{code}
testA0 : A.⟦ 0 ⟧  ≡ λ n → n
testA1 : A.⟦ 1 ⟧  ≡ I.Suc
testA2 : A.⟦ 2 ⟧  ≡ I.Suc ∘ I.Suc
testA3 : A.⟦ 3 ⟧  ≡ I.Suc ∘ I.Suc ∘ I.Suc
\end{code}
\begin{code}[hide]
testA0 = refl {x = λ (n : ℕ) → n}
testA1 = refl {x = I.Suc}
testA2 = refl {x = I.Suc ∘ I.Suc}
testA3 = refl {x = I.Suc ∘ I.Suc ∘ I.Suc}
\end{code}
Thus we can define addition of natural numbers as follows.
\begin{code}
_+'_ : I.Nat → I.Nat → I.Nat
_+'_ = A.⟦_⟧
\end{code}
\begin{code}
test1+3 : 1 +' 3 ≡ 4
test3+2 : 3 +' 2 ≡ 5
\end{code}
\begin{code}[hide]
test1+3 = refl {x = 4}
test3+2 = refl {x = 5}
\end{code}
A dependent model is the data for induction on natural numbers.
\begin{code}
record DepModel {i} : Set (lsuc i) where
  field
    Nat   : I.Nat → Set i
    Zero  : Nat I.Zero
    Suc   : {n : I.Nat} → Nat n → Nat (I.Suc n)
\end{code}
\begin{code}[hide]
  ⟦_⟧ : (n : I.Nat) → Nat n
  ⟦ zero ⟧ = Zero
  ⟦ suc n ⟧ = Suc ⟦ n ⟧
\end{code}
For example, we prove associativity of the above addition by the following dependent model.
The \verb$Nat$ component says what we want to prove for each number, the \verb$Zero$ component is the base case, the \verb$Suc$ components is the inductive case.
\begin{code}
Ass : (n o : I.Nat) → DepModel
Ass n o = record
  {  Nat   = λ m → Lift ((m +' n) +' o ≡ m +' (n +' o))
  ;  Zero  = mk (refl {x = n +' o})
  ;  Suc   = λ ih → ih
  }
\end{code}
In the base holds by reflexivity, in the inductive case we simply use the induction hypothesis. Now we obtain
the proof of associativity by interpreting into the dependent model \verb$Ass$.
\begin{code}
ass : (m n o : I.Nat) → (m +' n) +' o ≡ m +' (n +' o)
ass m n o = un (Assno.⟦_⟧ m)
  where
    module Assno = DepModel (Ass n o)
\end{code}
\begin{exe}
Show that \verb$0$ is right unit for addition using another dependent model.
\end{exe}
\begin{exe}
Show that \verb$+$ is commutative. You will need two separate dependent models.
\end{exe}
