\subsection{Normalisation in the empty context}

For normalisation in arbitrary contexts, see e.g.\ \cite{lmcs:4005}.

\begin{code}
normt : I.Tm I.∙ Aᴵ → St.⟦ Aᴵ ⟧T
normt tᴵ = St.⟦ tᴵ ⟧t _

norms : I.Sub I.∙ Γᴵ → St.⟦ Γᴵ ⟧C
norms γᴵ = St.⟦ γᴵ ⟧S _
\end{code}

Quoting normal forms to terms:
\begin{code}
{-
⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → Tm ∙ Bool
⌜ O ⌝B = false
⌜ I ⌝B = true

⌜_⌝ₜ : ∀ {A} → ⟦ A ⟧T → Tm ∙ A
⌜_⌝ₜ {Nat} = ⌜_⌝N
⌜_⌝ₜ {Bool} = ⌜_⌝B
-}
\end{code}

Quoting commutes with the operations \verb$isZero$, \verb$_+_$ and \verb$ite$:
\begin{code}
{-
isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝ₜ ≡ isZero ⌜ n ⌝ₜ
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

+-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝ₜ ≡ ⌜ m ⌝ₜ + ⌜ n ⌝ₜ
+-⌜⌝ {O} = refl
+-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})

ite-⌜⌝ : ∀ {A b} {u v : ⟦ A ⟧T} →
  ⌜_⌝ₜ {A} (if b then u else v) ≡ ite ⌜ b ⌝ₜ ⌜ u ⌝ₜ ⌜ v ⌝ₜ
ite-⌜⌝ {b = O} = refl
ite-⌜⌝ {b = I} = refl
-}
\end{code}
An element of the interpretation of a context can be quoted into
substitution from the empty context:
\begin{code}
{-
⌜_⌝ₛ : ∀ {Γ} → ⟦ Γ ⟧C → Sub ∙ Γ
⌜_⌝ₛ {∙} _ = id
⌜_⌝ₛ {Γ ▹ A} (σ ,Σ t) = ⌜ σ ⌝ₛ , ⌜ t ⌝ₜ
-}
\end{code}

Completeness of normalisation in the empty context is proved by
induction on the syntax.
\begin{code}
{-
Comp : DepModel
Comp = record
         { Con = λ _ → ↑p 𝟙
         ; Ty = λ _ → ↑p 𝟙
         ; Sub = λ _ _ σ' →
                   ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
                     ↑p (⌜ normₛ (σ' ∘ ν') ⌝ₛ ≡ σ' ∘ ν')
         ; Tm = λ _ _ t' →
                  ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
                     ↑p (⌜ normₜ (t' [ ν' ]) ⌝ₜ ≡ t' [ ν' ])
         ; ∙ = _
         ; _▹_ = _
         ; Nat = _
         ; Bool = _
         ; _∘_ = λ σ δ → σ ∘f δ
         ; id = idf
         ; ε = λ _ → ↑[ ∙η ◾ ∙η ⁻¹ ]↑
         ; _,_ = λ σ t h → ↑[ ap2 _,_ ↓[ σ h ]↓ ↓[ t h ]↓ ]↑
         ; p = λ h → ↑[ ap (p ∘_) ↓[ h ]↓ ]↑
         ; q = λ h → ↑[ ap (q [_]) ↓[ h ]↓ ]↑
         ; _[_] = λ t σ → t ∘f σ
         ; zero = λ _ → refl↑
         ; suc = λ n h → ↑[ ap suc ↓[ n h ]↓ ]↑
         ; isZero = λ n h → ↑[ isZero-⌜⌝
                             ◾ ap isZero ↓[ n h ]↓ ]↑
         ; _+_ = λ {_ m'} m n {ν'} h →
                   ↑[ +-⌜⌝ {normₜ (m' [ ν' ])}
                    ◾ ap2 _+_ ↓[ m h ]↓ ↓[ n h ]↓ ]↑
         ; true = λ _ → refl↑
         ; false = λ _ → refl↑
         ; ite = λ {_ _ b'} b u v {ν'} h →
                   ↑[ ite-⌜⌝ {b = normₜ (b' [ ν' ])}
                    ◾ ap3 ite ↓[ b h ]↓ ↓[ u h ]↓ ↓[ v h ]↓ ]↑
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; isZero[] = refl
         ; +[] = refl
         ; true[] = refl
         ; false[] = refl
         ; ite[] = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁ = refl
         ; +β₂ = refl
         ; iteβ₁ = refl
         ; iteβ₂ = refl
         }
module Comp = DepModel Comp

completenessₜ : ∀ {A} {t : Tm ∙ A} → ⌜ normₜ t ⌝ₜ ≡ t
completenessₜ {t = t} = ↓[ Comp.⟦ t ⟧t {id} refl↑ ]↓

completenessₛ : ∀ {Γ} {σ : Sub ∙ Γ} → ⌜ normₛ σ ⌝ₛ ≡ σ
completenessₛ {σ = σ} = ↓[ Comp.⟦ σ ⟧S {id} refl↑ ]↓
-}
\end{code}
Stability for terms:
\begin{code}
{-
stabilityN : ∀ {n} → normₜ ⌜ n ⌝N ≡ n
stabilityN {O} = refl
stabilityN {S n} = ap S stabilityN

stabilityB : ∀ {b} → normₜ ⌜ b ⌝B ≡ b
stabilityB {O} = refl
stabilityB {I} = refl

stabilityₜ : ∀ {A} {t : ⟦ A ⟧T} → normₜ (⌜_⌝ₜ {A} t) ≡ t
stabilityₜ {Nat} = stabilityN
stabilityₜ {Bool} = stabilityB
-}
\end{code}
Stability for substitutions:
\begin{code}
{-
stabilityₛ : ∀ {Γ} {σ : ⟦ Γ ⟧C} → normₛ (⌜_⌝ₛ {Γ} σ) ≡ σ
stabilityₛ {∙} = refl
stabilityₛ {Γ ▹ A} = ap2 _,Σ_ (stabilityₛ {Γ}) (stabilityₜ {A})
-}
\end{code}
