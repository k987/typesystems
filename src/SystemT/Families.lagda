\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module SystemT.Families where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import SystemT.Algebra

open I
\end{code}
\begin{code}
Fam : Set → Algebra
Fam Ι = record
        { Con = Ι → Set
        ; Ty = Ι → Set
        ; Sub = λ Γ Δ → ∀ {ι} → Γ ι → Δ ι
        ; Tm = λ Γ A → ∀ {ι} → Γ ι → A ι
        ; ∙ = λ _ → ↑p 𝟙
        ; _▹_ = λ Γ A ι → Γ ι × A ι
        ; Nat = λ _ → ℕ
        ; _⇒_ = λ A B ι → A ι → B ι
        ; _∘_ = λ σ δ γ → σ (δ γ)
        ; id = idf
        ; ε = λ _ → *↑
        ; _,_ = λ σ t γ → σ γ ,Σ t γ
        ; p = π₁
        ; q = π₂
        ; _[_] = λ t σ γ → t (σ γ)
        ; lam = λ t γ a → t (γ ,Σ a)
        ; app = λ t γa → t (π₁ γa) (π₂ γa)
        ; zero = λ _ → O
        ; suc = λ n γ → S (n γ)
        ; rec = λ u v n γ → indℕ _ (u γ) (λ a → v (γ ,Σ a)) (n γ)
        ; ass = refl
        ; idl = refl
        ; idr = refl
        ; ∙η = refl
        ; ▹β₁ = refl
        ; ▹β₂ = refl
        ; ▹η = refl
        ; [id] = refl
        ; [∘] = refl
        ; ⇒β = refl
        ; ⇒η = refl
        ; Natβ₁ = refl
        ; Natβ₂ = refl
        ; lam[] = refl
        ; zero[] = refl
        ; suc[] = refl
        ; rec[] = refl
        }
\end{code}
