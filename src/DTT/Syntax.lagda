\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module DTT.Syntax where

open import Lib hiding (_∘_; Σ; _,_)
\end{code}
\begin{code}
infixl 5 _▻_
infixl 6 _+_
infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 8 _[_]T
infixl 8 _[_]t
infixl 5 _$_

data Con : Set
data Ty  : Con → Set
data Sub : Con → Con → Set
data Tm  : (Γ : Con) → Ty Γ → Set

id       : ∀ {Γ} → Sub Γ Γ
_∘_      : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
ass      : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
  (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
idl      : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
idr      : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

_[_]T  : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
_[_]t  : ∀ {Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
[id]T  : ∀ {Γ}{A : Ty Γ} → A [ id ]T ≡ A
[∘]T   : ∀ {Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
  A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
[id]t  : ∀ {Γ A}{t : Tm Γ A} → t [ id ]t =[ Tm Γ & [id]T ]= t
[∘]t   : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
  t [ σ ]t [ δ ]t =[ Tm Γ & [∘]T ]= (t [ σ ∘ δ ]t)

------------------------------------------------------------------------

data Con where
  ∙   : Con
  _▻_ : (Γ : Con) → Ty Γ → Con

data Ty where
  Unit  : ∀ {Γ} → Ty Γ
  Empty : ∀ {Γ} → Ty Γ
  _+_   : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
  Π     : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
  Σ     : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ

ε      : ∀ {Γ} → Sub Γ ∙
∙η     : ∀ {Γ}{σ : Sub Γ ∙} → ε ≡ σ

_,_    : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
p      : ∀ {Γ A} → Sub (Γ ▻ A) Γ
q      : ∀ {Γ A} → Tm (Γ ▻ A) (A [ p ]T)
▻β₁    : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
  p ∘ (σ , t) ≡ σ
▻β₂    : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
  q [ σ , t ]t =[ Tm Γ & ([∘]T ◾ (A [_]T) & ▻β₁) ]= t
▻η     : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} →
  p ∘ σ , coe (Tm Γ & [∘]T) (q [ σ ]t) ≡ σ
,∘     : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
  (σ , t) ∘ δ ≡ σ ∘ δ , coe (Tm Γ & [∘]T) (t [ δ ]t)

_↑ : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
σ ↑ = {!!}

lam    : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (Π A B)
app    : ∀ {Γ A B} → Tm Γ (Π A B) → Tm (Γ ▻ A) B
Πβ     : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
Πη     : ∀ {Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
Π[]    : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
  (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ ]T)
lam[]  : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
  (lam t) [ σ ]t =[ Tm Γ & Π[] ]= lam (t [ σ ↑ ]t)

_⇒_ : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
A ⇒ B = {!!}

_$_ : ∀ {Γ A B} → Tm Γ (Π A B) → (u : Tm Γ A) →
  Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T)
t $ u = {!!}

app[] : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{σ : Sub Γ Δ} →
  app (coe (Tm Γ & Π[]) (t [ σ ]t)) ≡ (app t) [ σ ↑ ]t
app[] = {!!}

⟨_,_⟩  : ∀ {Γ A B} → (u : Tm Γ A) →
  Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T) → Tm Γ (Σ A B)
proj₁  : ∀ {Γ A B} → Tm Γ (Σ A B) → Tm Γ A
\end{code}
