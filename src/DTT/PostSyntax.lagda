\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module DTT.PostSyntax where

open import Lib hiding (_∘_; Σ; _,_)
\end{code}
\begin{code}
infixl 5 _▻_
infixl 6 _+_
infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 8 _[_]T
infixl 8 _[_]t
infixl 5 _$_

postulate
  Con      : Set
  Ty       : Con → Set
  Sub      : Con → Con → Set
  Tm       : (Γ : Con) → Ty Γ → Set

  id       : ∀ {Γ} → Sub Γ Γ
  _∘_      : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
  ass      : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
    (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  idl      : ∀ {Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
  idr      : ∀ {Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
  {-# REWRITE ass idl idr #-}

  _[_]T    : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
  _[_]t    : ∀ {Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  [id]T    : ∀ {Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [∘]T     : ∀ {Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
    A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
  {-# REWRITE [id]T [∘]T #-}
  [id]t    : ∀ {Γ A}{t : Tm Γ A} → t [ id ]t ≡ t
  [∘]t     : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
    t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t
  {-# REWRITE [id]t [∘]t #-}

  ∙        : Con
  ε        : ∀ {Γ} → Sub Γ ∙
  ∙η       : ∀ {Γ}{σ : Sub Γ ∙} → σ ≡ ε

  _▻_      : (Γ : Con) → Ty Γ → Con
  _,_      : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
  p        : ∀ {Γ A} → Sub (Γ ▻ A) Γ
  q        : ∀ {Γ A} → Tm (Γ ▻ A) (A [ p ]T)
  ▻β₁      : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
    p {Δ}{A} ∘ (σ , t) ≡ σ
  {-# REWRITE ▻β₁ #-}
  ▻β₂      : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
    q {Δ}{A} [ σ , t ]t ≡ t
  ▻η       : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ]t ≡ σ
  ,∘       : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
    (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]t

_↑_ : ∀ {Γ Δ}(σ : Sub Γ Δ) A → Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
σ ↑ A = σ ∘ p , q

postulate
  ↑∘       : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ}
    {t : Tm Γ (A [ σ ]T [ δ ]T)} →
    (σ ↑ A) ∘ (δ , t) ≡ σ ∘ δ , t
  {-# REWRITE ▻β₂ ▻η ,∘ ↑∘ #-}

  Π        : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
  lam      : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (Π A B)
  app      : ∀ {Γ A B} → Tm Γ (Π A B) → Tm (Γ ▻ A) B
  Πβ       : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
  Πη       : ∀ {Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
  Π[]      : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
    (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ A ]T)
  {-# REWRITE Πβ Πη Π[] #-}
  lam[]    : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
    (lam t) [ σ ]t ≡ lam (t [ σ ↑ A ]t)
  {-# REWRITE lam[] #-}

_⇒_ : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀ {Γ A B} → Tm Γ (Π A B) → (u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = (app t) [ id , u ]t

⇒[] : ∀ {Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ A [ σ ]T ⇒ B [ σ ]T
⇒[] = refl

postulate
  app[]    : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{σ : Sub Γ Δ} →
    app (t [ σ ]t) ≡ (app t) [ σ ↑ A ]t
  {-# REWRITE app[] #-}

$[] : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
  (t $ u) [ σ ]t ≡ t [ σ ]t $ u [ σ ]t
$[] = refl  

postulate
  Σ        : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
  ⟨_,_⟩    : ∀ {Γ A B}(u : Tm Γ A) → Tm Γ (B [ id , u ]T) → Tm Γ (Σ A B)
  proj₁    : ∀ {Γ A B} → Tm Γ (Σ A B) → Tm Γ A
  proj₂    : ∀ {Γ A B}(t : Tm Γ (Σ A B)) → Tm Γ (B [ id , proj₁ t ]T)
  Σβ₁      : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
    proj₁ {B = B} ⟨ u , v ⟩ ≡ u
  {-# REWRITE Σβ₁ #-}
  Σβ₂      : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
    proj₂ {B = B} ⟨ u , v ⟩ ≡ v
  Ση       : ∀ {Γ A B}{t : Tm Γ (Σ A B)} →
    ⟨_,_⟩ {B = B} (proj₁ t) (proj₂ t) ≡ t
  Σ[]      : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
    (Σ A B) [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ↑ A ]T)
  {-# REWRITE Σβ₂ Ση Σ[] #-}
  ⟨,⟩[]    : ∀ {Γ Δ A B}{u : Tm Δ A}{v : Tm Δ (B [ id , u ]T)}
    {σ : Sub Γ Δ} → (⟨_,_⟩ {B = B} u v) [ σ ]t ≡ ⟨ u [ σ ]t , v [ σ ]t ⟩
  proj₁[]  : ∀ {Γ Δ A B}{t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ]t ≡ proj₁ (t [ σ ]t)
  {-# REWRITE ⟨,⟩[] proj₁[]  #-}
  proj₂[]  : ∀ {Γ Δ A B}{t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ]t ≡ proj₂ (t [ σ ]t)
  {-# REWRITE proj₂[] #-}

  ⊤        : ∀ {Γ} → Ty Γ
  tt       : ∀ {Γ} → Tm Γ ⊤
  ⊤η       : ∀ {Γ}{t : Tm Γ ⊤} → t ≡ tt
  ⊤[]      : ∀ {Γ Δ}{σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
  {-# REWRITE ⊤[] #-}
  tt[]     : ∀ {Γ Δ}{σ : Sub Γ Δ} → tt [ σ ]t ≡ tt
  {-# REWRITE tt[] #-}

  _+_      : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
  inj₁     : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
  inj₂     : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
  +[]      : ∀ {Γ Δ A B}{σ : Sub Γ Δ} → (A + B) [ σ ]T ≡ A [ σ ]T + B [ σ ]T
  {-# REWRITE +[] #-}
  inj₁[]   : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
    (inj₁ {B = B} t) [ σ ]t ≡ inj₁ (t [ σ ]t)
  inj₂[]   : ∀ {Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} →
    (inj₂ {A = A} t) [ σ ]t ≡ inj₂ (t [ σ ]t)
  {-# REWRITE inj₁[] inj₂[] #-}
  ind+     : ∀ {Γ A B}(P : Ty (Γ ▻ A + B)) →
    Tm (Γ ▻ A) (P [ p , inj₁ q ]T) → Tm (Γ ▻ B) (P [ p , inj₂ q ]T) →
    (t : Tm Γ (A + B)) → Tm Γ (P [ id , t ]T)
  ind+[]   : ∀ {Γ Δ A B P}
    {u : Tm (Δ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Δ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Δ (A + B)}{σ : Sub Γ Δ} →
    (ind+ P u v t) [ σ ]t ≡
      {!ind+ (P [ σ ↑ (A + B) ]T) (u [ σ ↑ A ]t) (v [ σ ↑ B ]t) (t [ σ ]t)!}
  +β₁      : ∀ {Γ A B P}
    {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ A} →
    ind+ P u v (inj₁ t) ≡ u [ id , t ]t
  +β₂      : ∀ {Γ A B P}
    {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ B} →
    ind+ P u v (inj₂ t) ≡ v [ id , t ]t
  {-# REWRITE +β₁ +β₂ #-}

case : ∀ {Γ A B C} → Tm Γ (A + B) →
  Tm (Γ ▻ A) (C [ p ]T) → Tm (Γ ▻ B) (C [ p ]T) → Tm Γ C
case {C = C} t u v = ind+ (C [ p ]T) u v t

postulate
  ⊥        : ∀ {Γ} → Ty Γ
  absurd   : ∀ {Γ} A → Tm Γ ⊥ → Tm Γ A
  ⊥[]      : ∀ {Γ Δ}{σ : Sub Γ Δ} → ⊥ [ σ ]T ≡ ⊥
  {-# REWRITE ⊥[] #-}
  absurd[] : ∀ {Γ Δ A}{t : Tm Δ ⊥}{σ : Sub Γ Δ} →
    (absurd A t) [ σ ]t ≡ absurd (A [ σ ]T) (t [ σ ]t)
  {-# REWRITE absurd[] #-}
\end{code}
