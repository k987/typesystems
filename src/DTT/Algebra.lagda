\begin{code}[hide]
{-# OPTIONS --prop --rewriting --allow-unsolved-metas #-}

module DTT.Algebra where

open import Lib hiding (_∘_; Σ; _,_)
\end{code}
\begin{code}
record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▻_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]T
  infixl 6 _[_]t
  infixl 5 _$_
  
  field
    Con    : Set i
    Ty     : Con → Set j
    Sub    : Con → Con → Set k
    Tm     : (Γ : Con) → Ty Γ → Set l

    id     : ∀ {Γ} → Sub Γ Γ
    _∘_    : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    ass    : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl    : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr    : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]T  : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
    _[_]t  : ∀ {Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
    [id]T  : ∀ {Γ}{A : Ty Γ} → A [ id ]T ≡ A
    [∘]T   : ∀ {Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
    [id]t  : ∀ {Γ A}{t : Tm Γ A} → t [ id ]t =[ Tm Γ & [id]T ]= t
    [∘]t   : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]t [ δ ]t =[ Tm Γ & [∘]T ]= (t [ σ ∘ δ ]t)

    ∙      : Con
    ε      : ∀ {Γ} → Sub Γ ∙
    ∙η     : ∀ {Γ}{σ : Sub Γ ∙} → ε ≡ σ

    _▻_    : (Γ : Con) → Ty Γ → Con
    _,_    : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
    p      : ∀ {Γ A} → Sub (Γ ▻ A) Γ
    q      : ∀ {Γ A} → Tm (Γ ▻ A) (A [ p ]T)
    ▻β₁    : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
      p ∘ (σ , t) ≡ σ
    ▻β₂    : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} →
      q [ σ , t ]t =[ Tm Γ & ([∘]T ◾ (A [_]T) & ▻β₁) ]= t
    ▻η     : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} →
      p ∘ σ , coe (Tm Γ & [∘]T) (q [ σ ]t) ≡ σ
    ,∘     : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
      (σ , t) ∘ δ ≡ σ ∘ δ , coe (Tm Γ & [∘]T) (t [ δ ]t)
  
  _↑ : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
  σ ↑ = σ ∘ p , coe (Tm _ & [∘]T) q
  
  field
    Π      : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
    lam    : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (Π A B)
    app    : ∀ {Γ A B} → Tm Γ (Π A B) → Tm (Γ ▻ A) B
    Πβ     : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
    Πη     : ∀ {Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
    Π[]    : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
      (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ ]T)
    lam[]  : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ]t =[ Tm Γ & Π[] ]= lam (t [ σ ↑ ]t)

  _⇒_ : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
  A ⇒ B = Π A (B [ p ]T)

  _$_ : ∀ {Γ A B} → Tm Γ (Π A B) → (u : Tm Γ A) →
    Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T)
  t $ u = (app t) [ id , coe (Tm _ & [id]T ⁻¹) u ]t

  app[] : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{σ : Sub Γ Δ} →
    app (coe (Tm Γ & Π[]) (t [ σ ]t)) ≡ (app t) [ σ ↑ ]t
  app[] {Γ}{Δ}{A}{B}{t}{σ} =
        (λ t → app (coe (Tm Γ & Π[]) (t [ σ ]t))) & Πη ⁻¹
      ◾ app & lam[]
      ◾ Πβ
      
  field
    Σ      : ∀ {Γ} → (A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
    ⟨_,_⟩  : ∀ {Γ A B} → (u : Tm Γ A) →
      Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T) → Tm Γ (Σ A B)
    proj₁  : ∀ {Γ A B} → Tm Γ (Σ A B) → Tm Γ A
    proj₂  : ∀ {Γ A B} (t : Tm Γ (Σ A B)) →
      Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) (proj₁ t) ]T)
    Σβ₁    : ∀ {Γ A B}{u : Tm Γ A}
      {v : Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T)} →
      proj₁ ⟨ u , v ⟩ ≡ u
    Σβ₂    : ∀ {Γ A B}{u : Tm Γ A}
      {v : Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) u ]T)} →
      proj₂ ⟨ u , v ⟩
        =[ (λ t → Tm Γ (B [ id , coe (Tm Γ & [id]T ⁻¹) t ]T)) & Σβ₁ ]= v
    Ση     : ∀ {Γ A B}{t : Tm Γ (Σ A B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    Σ[]    : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
      (Σ A B) [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ↑ ]T)
    ⟨,⟩[]  : ∀ {Γ Δ A B}{u : Tm Δ A}
      {v : Tm Δ (B [ id , coe (Tm Δ & [id]T ⁻¹) u ]T)}{σ : Sub Γ Δ} →
      let p : Tm Γ (B [ id , coe (Tm Δ & [id]T ⁻¹) u ]T [ σ ]T)
            ≡ Tm Γ (B [ σ ↑ ]T [ id , coe (Tm Γ & [id]T ⁻¹) (u [ σ ]t) ]T)
          p = Tm Γ & ([∘]T ◾ (B [_]T) & (,∘ ◾ {!!}) ◾ {!!} ⁻¹)
      in ⟨ u , v ⟩ [ σ ]t =[ Tm Γ & Σ[] ]= ⟨ u [ σ ]t , coe p (v [ σ ]t) ⟩

    ⊤      : ∀ {Γ} → Ty Γ
    tt     : ∀ {Γ} → Tm Γ ⊤
    ⊤η     : ∀ {Γ}{t : Tm Γ ⊤} → tt ≡ t
    ⊤[]    : ∀ {Γ Δ}{σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
    tt[]   : ∀ {Γ Δ}{σ : Sub Γ Δ} → tt [ σ ]t =[ Tm Γ & ⊤[] ]= tt

    _+_    : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
    +[]    : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
      (A + B) [ σ ]T ≡ (A [ σ ]T) + (B [ σ ]T)
    inj₁   : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inj₂   : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    inj₁[] : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
      (inj₁ {B = B} t) [ σ ]t =[ Tm Γ & +[] ]= inj₁ (t [ σ ]t)
    inj₂[] : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
      (inj₂ {A = A} t) [ σ ]t =[ Tm Γ & +[] ]= inj₂ (t [ σ ]t)
    case   : ∀ {Γ A B}(P : Ty (Γ ▻ A + B)) →
      Tm (Γ ▻ A) (P [ p , coe (Tm (Γ ▻ A) & +[] ⁻¹) (inj₁ q) ]T) →
      Tm (Γ ▻ B) (P [ p , coe (Tm (Γ ▻ B) & +[] ⁻¹) (inj₂ q) ]T) →
      (t : Tm Γ (A + B)) → Tm Γ (P [ id , coe (Tm Γ & [id]T ⁻¹) t ]T)
    +β₁    : ∀ {Γ A B}{P : Ty (Γ ▻ A + B)}
      {u : Tm (Γ ▻ A) (P [ p , coe (Tm (Γ ▻ A) & +[] ⁻¹) (inj₁ q) ]T)}
      {v : Tm (Γ ▻ B) (P [ p , coe (Tm (Γ ▻ B) & +[] ⁻¹) (inj₂ q) ]T)}
      {t : Tm Γ A} →
      case P u v (inj₁ t) ≡ {!!}
    +β₂    : ∀ {Γ A B}{P : Ty (Γ ▻ A + B)}
      {u : Tm (Γ ▻ A) (P [ p , coe (Tm (Γ ▻ A) & +[] ⁻¹) (inj₁ q) ]T)}
      {v : Tm (Γ ▻ B) (P [ p , coe (Tm (Γ ▻ B) & +[] ⁻¹) (inj₂ q) ]T)}
      {t : Tm Γ B} →
      case P u v (inj₂ t) ≡ {!!}
\end{code}
