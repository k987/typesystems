\begin{code}[hide]
{-# OPTIONS --prop #-}

module DTT.Standard where

open import Lib renaming (_∘_ to _∘f_; Σ to Σm; _,_ to _⸴_)
\end{code}
\begin{code}
infixl 5 _▻_
infixl 6 _+_
infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 7 _↑_
infixl 8 _[_]T
infixl 8 _[_]t
infixl 5 _$_

Con : Set₁
Con = Set

Ty : Con → Set₁
Ty Γ = Γ → Set

Sub : Con → Con → Set
Sub Γ Δ = Γ → Δ

Tm : (Γ : Con) → Ty Γ → Set
Tm Γ A = (γ : Γ) → A γ

id : ∀ {Γ} → Sub Γ Γ
id γ = γ

_∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
(σ ∘ δ) γ = σ (δ γ)

ass : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
  (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

_[_]T : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
(A [ σ ]T) γ = A (σ γ)

_[_]t : ∀ {Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
(t [ σ ]t) γ = t (σ γ)

[id]T : ∀ {Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[∘]T : ∀ {Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
  A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
[∘]T = refl

[id]t : ∀ {Γ A}{t : Tm Γ A} → t [ id ]t ≡ t
[id]t = refl

[∘]t : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
  t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t
[∘]t = refl

∙ : Con
∙ = 𝟙↑

ε : ∀ {Γ} → Sub Γ ∙
ε γ = *↑

∙η : ∀ {Γ}{σ : Sub Γ ∙} → σ ≡ ε
∙η = refl

_▻_    : (Γ : Con) → Ty Γ → Con
Γ ▻ A = Σm Γ A

_,_ : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
(σ , t) γ = σ γ ⸴ t γ

p : ∀ {Γ A} → Sub (Γ ▻ A) Γ
p (γ ⸴ a) = γ

q : ∀ {Γ A} → Tm (Γ ▻ A) (A [ p ]T)
q (γ ⸴ a) = a

▻β₁ : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p {Δ}{A} ∘ (σ , t) ≡ σ
▻β₁ = refl

▻β₂ : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → q {Δ}{A} [ σ , t ]t ≡ t
▻β₂ = refl

▻η : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ]t ≡ σ
▻η = refl

,∘ : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
  (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]t
,∘ = refl

_↑_ : ∀ {Γ Δ}(σ : Sub Γ Δ) A → Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
σ ↑ A = σ ∘ p , q

↑∘ : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T [ δ ]T)} →
  (σ ↑ A) ∘ (δ , t) ≡ σ ∘ δ , t
↑∘ = refl

Π : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
(Π A B) γ = (a : A γ) → B (γ ⸴ a)

lam : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (Π A B)
(lam t) γ a = t (γ ⸴ a)

app : ∀ {Γ A B} → Tm Γ (Π A B) → Tm (Γ ▻ A) B
(app t) (γ ⸴ a) = t γ a

Πβ : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
Πβ = refl

Πη : ∀ {Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
Πη = refl

Π[] : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
  (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ A ]T)
Π[] = refl

lam[] : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
  (lam t) [ σ ]t ≡ lam (t [ σ ↑ A ]t)
lam[] = refl

_⇒_ : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀ {Γ A B} → Tm Γ (Π A B) → (u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = (app t) [ id , u ]t

⇒[] : ∀ {Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ A [ σ ]T ⇒ B [ σ ]T
⇒[] = refl

app[] : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{σ : Sub Γ Δ} →
  app (t [ σ ]t) ≡ (app t) [ σ ↑ A ]t
app[] = refl

$[] : ∀ {Γ Δ A B}{t : Tm Δ (Π A B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
  (t $ u) [ σ ]t ≡ t [ σ ]t $ u [ σ ]t
$[] = refl

Σ : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▻ A) → Ty Γ
(Σ A B) γ = Σm (A γ) (λ a → B (γ ⸴ a))

⟨_,_⟩ : ∀ {Γ A B}(u : Tm Γ A) → Tm Γ (B [ id , u ]T) → Tm Γ (Σ A B)
⟨ u , v ⟩ γ = u γ ⸴ v γ

proj₁ : ∀ {Γ A B} → Tm Γ (Σ A B) → Tm Γ A
(proj₁ t) γ = π₁ (t γ)

proj₂ : ∀ {Γ A B}(t : Tm Γ (Σ A B)) → Tm Γ (B [ id , proj₁ t ]T)
(proj₂ t) γ = π₂ (t γ)

Σβ₁ : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  proj₁ {B = B} ⟨ u , v ⟩ ≡ u
Σβ₁ = refl

Σβ₂ : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  proj₂ {B = B} ⟨ u , v ⟩ ≡ v
Σβ₂ = refl

Ση : ∀ {Γ A B}{t : Tm Γ (Σ A B)} → ⟨_,_⟩ {B = B} (proj₁ t) (proj₂ t) ≡ t
Ση = refl

Σ[] : ∀ {Γ Δ A B}{σ : Sub Γ Δ} →
  (Σ A B) [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ↑ A ]T)
Σ[] = refl

⟨,⟩[] : ∀ {Γ Δ A B}{u : Tm Δ A}{v : Tm Δ (B [ id , u ]T)}{σ : Sub Γ Δ} →
  (⟨_,_⟩ {B = B} u v) [ σ ]t ≡ ⟨ u [ σ ]t , v [ σ ]t ⟩
⟨,⟩[] = refl

proj₁[] : ∀ {Γ Δ A B}{t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} →
  (proj₁ t) [ σ ]t ≡ proj₁ (t [ σ ]t)
proj₁[] = refl

proj₂[] : ∀ {Γ Δ A B}{t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} →
  (proj₂ t) [ σ ]t ≡ proj₂ (t [ σ ]t)
proj₂[] = refl

⊤ : ∀ {Γ} → Ty Γ
⊤ γ = 𝟙↑

tt : ∀ {Γ} → Tm Γ ⊤
tt γ = *↑

⊤η : ∀ {Γ}{t : Tm Γ ⊤} → t ≡ tt
⊤η = refl

⊤[] : ∀ {Γ Δ}{σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
⊤[] = refl

tt[] : ∀ {Γ Δ}{σ : Sub Γ Δ} → tt [ σ ]t ≡ tt
tt[] = refl

_+_ : ∀ {Γ} → Ty Γ → Ty Γ → Ty Γ
(A + B) γ = A γ ⊎ B γ

inj₁ : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
(inj₁ t) γ = ι₁ (t γ)

inj₂ : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
(inj₂ t) γ = ι₂ (t γ)

ind+ : ∀ {Γ A B}(P : Ty (Γ ▻ A + B)) →
  Tm (Γ ▻ A) (P [ p , inj₁ q ]T) → Tm (Γ ▻ B) (P [ p , inj₂ q ]T) →
  (t : Tm Γ (A + B)) → Tm Γ (P [ id , t ]T)
(ind+ P u v t) γ =
  ind⊎ (λ u → P (γ ⸴ u)) (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b)) (t γ)

+β₁ : ∀ {Γ A B P}
  {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
  {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ A} →
  ind+ P u v (inj₁ t) ≡ u [ id , t ]t
+β₁ = refl

+β₂ : ∀ {Γ A B P}
  {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
  {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ B} →
  ind+ P u v (inj₂ t) ≡ v [ id , t ]t
+β₂ = refl

+[] : ∀ {Γ Δ A B}{σ : Sub Γ Δ} → (A + B) [ σ ]T ≡ A [ σ ]T + B [ σ ]T
+[] = refl

inj₁[] : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
  (inj₁ {B = B} t) [ σ ]t ≡ inj₁ (t [ σ ]t)
inj₁[] = refl

inj₂[] : ∀ {Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} →
  (inj₂ {A = A} t) [ σ ]t ≡ inj₂ (t [ σ ]t)
inj₂[] = refl

ind+[] : ∀ {Γ Δ A B P}
  {u : Tm (Δ ▻ A) (P [ p , inj₁ q ]T)}
  {v : Tm (Δ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Δ (A + B)}{σ : Sub Γ Δ} →
  (ind+ P u v t) [ σ ]t ≡
    ind+ (P [ σ ↑ (A + B) ]T) (u [ σ ↑ A ]t) (v [ σ ↑ B ]t) (t [ σ ]t)
ind+[] = refl

case : ∀ {Γ A B C} → Tm Γ (A + B) →
  Tm (Γ ▻ A) (C [ p ]T) → Tm (Γ ▻ B) (C [ p ]T) → Tm Γ C
case {C = C} t u v = ind+ (C [ p ]T) u v t

⊥ : ∀ {Γ} → Ty Γ
⊥ γ = 𝟘↑

void : ∀ {Γ} A → Tm Γ ⊥ → Tm Γ A
(void A t) γ = ⟦ ↓[ t γ ]↓ ⟧𝟘

⊥[] : ∀ {Γ Δ}{σ : Sub Γ Δ} → ⊥ [ σ ]T ≡ ⊥
⊥[] = refl

void[] : ∀ {Γ Δ A}{t : Tm Δ ⊥}{σ : Sub Γ Δ} →
  (void A t) [ σ ]t ≡ void (A [ σ ]T) (t [ σ ]t)
void[] = refl
\end{code}
