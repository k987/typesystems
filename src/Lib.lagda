 \appendix

\chapter{Metatheory}

We write \verb$:$ instead of \verb$∈$, \verb$(x : A) → B$ instead of \verb$∀x.B$ (where \verb$x$
quantifies over \verb$A$), \verb$Σ A λ x → B$ instead of \verb$∃x.B$, \verb$_,_$
constructor. \verb$⊎$ with constructors \verb$ι₁$ and \verb$ι₂$. \verb$Set$, \verb$Prop$, \verb$→$, application is space, curried
functions. \verb$⊤$ with constructor \verb$trivi$. \verb$𝟚$ with constructors
\verb$tt$, \verb$ff$. \verb$ℕ$ with constructors \verb$zero$, \verb$suc$. What
are relations and predicates, proof-relevant relations. \verb$Prop$ is a
subtype of \verb$Set$, \verb$Lift$.

Inductive sets. For small definitions, we use pattern matching.

\begin{code}
{-# OPTIONS --prop --rewriting #-}

module Lib where

open import Agda.Primitive public

infix  4 _≡_
infixr 2 _≡≡_
infix  3 _∎
infixr 2 _≡⟨_⟩_
infix  4 _≡𝟚_
infixl 7 _∨_
infix  4 _≡ℕ_
infixl 7 _+_
infixr 1 _⊎_
infixr 2 _×_
infixr 4 _,_
infixr 0 _↔_
infixl 6 _∘_
infixr 5  _::_


postulate
  exercise  : ∀{i}{A : Set  i} → A
  exercisep : ∀{i}{A : Prop i} → A

-- Bottom

data ⊥ : Prop where

exfalso : ∀{i}{A : Set i} → ⊥ → A
exfalso ()

¬_ : ∀{i}(A : Prop i) → Prop i
¬ A = A → ⊥


-- Top

record ⊤ : Prop where
  constructor trivi
open ⊤ public


-- Functions

_∘_ : ∀ {i j k} {A : Set i}{B : A → Set j}{C : ∀ {x} → B x → Set k}
  (f : ∀ {x} (y : B x) → C y)(g : (x : A) → B x)
  (x : A) → C (g x)
(f ∘ g) x = f (g x)


-- Lift

record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift public


-- Equality

postulate
  _≡_  : ∀{i}{A : Set i}(x : A) → A → Prop i
{-# BUILTIN REWRITE _≡_ #-}

postulate
  _~      : ∀{i j}{A : Set i}(B : A → Set j){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁) → B a₀ → B a₁ → Prop j
  ~const  : ∀{i j}{A : Set i}{B : Set j}{a₀ a₁ : A}{a₀₁ : a₀ ≡ a₁}{b₀ b₁ : B} → ((λ _ → B) ~) a₀₁ b₀ b₁ ≡ (b₀ ≡ b₁)
  ~refl   : ∀{i j}{A : Set i}(B : A → Set j){a : A}{a= : a ≡ a}{b₀ b₁ : B a} → (B ~) a= b₀ b₁ ≡ (b₀ ≡ b₁)
  {-# REWRITE ~const ~refl #-}

postulate
  cong : ∀{i j}{A : Set i}{B : A → Set j}(f : (a : A) → B a){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁) → (B ~) a₀₁ (f a₀) (f a₁)
  coe  : ∀{i j}{A : Set i}(B : A → Set j){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁) → B a₀ → B a₁
  coerefl : ∀{i j}{A : Set i}(B : A → Set j){a : A}{a= : a ≡ a}{b : B a} → coe B a= b ≡ b
  {-# REWRITE coerefl #-}

coep : ∀{i j}{A : Set i}(B : A → Prop j){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁) → B a₀ → B a₁
coep B a₀₁ b₀ = un (coe (λ a → Lift (B a)) a₀₁ (mk b₀))

trans : ∀{i}{A : Set i}{a a' a'' : A}(a= : a ≡ a')(a=' : a' ≡ a'') → a ≡ a''
trans a= a=' = coep (_ ≡_) a=' a=

data _≡s_ {i}{A : Set i} : A → A → Set i where
  _∎∎     : (x : A) → x ≡s x
  _≡≡_   : (x : A) → x ≡s x → x ≡s x

_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = trans x≡y y≡z


-- Raise

record Raise {i j}(A : Set i) : Set (i ⊔ j) where
  constructor mk
  field un : A
open Raise public

postulate
  coeRaise : ∀{i j k}{A : Set i}(B : A → Set j){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁) → (b₀ : B a₀) →
             coe (Raise {j = k} ∘ B) a₀₁ (mk b₀) ≡ mk (coe B a₀₁ b₀)
{-# REWRITE coeRaise #-}

record Raisep {i j}(A : Prop i) : Prop (i ⊔ j) where
  constructor mk
  field un : A
open Raisep public

-- Lift≡
postulate
  Lift~ : ∀{i j}{A : Set i}{B : A → Prop j}{a₀ a₁ : A}{a₀₁ : a₀ ≡ a₁}{b₀ : Lift (B a₀)}{b₁ : Lift (B a₁)} →
    ((λ a → Lift (B a)) ~) a₀₁ b₀ b₁ ≡ Raisep ⊤
  Lift≡ : ∀{i}{A : Prop i}{a₀ a₁ : Lift A} → _≡_ {A = Lift A} a₀ a₁ ≡ Raisep ⊤
{-# REWRITE Lift~ Lift≡ #-}


-- Booleans

data 𝟚 : Set where
  tt ff : 𝟚

variable
  b : 𝟚

_≡𝟚_ : 𝟚 → 𝟚 → Prop
tt ≡𝟚 tt = ⊤
ff ≡𝟚 ff = ⊤
tt ≡𝟚 ff = ⊥
ff ≡𝟚 tt = ⊥

postulate
  𝟚≡ : _≡_ {A = 𝟚} ≡ _≡𝟚_
  {-# REWRITE 𝟚≡ #-}

if_then_else_ : ∀{i}{A : Set i}(t : 𝟚)(u v : A) → A
if tt then u else v = u
if ff then u else v = v

ind𝟚 : ∀{i}(P : 𝟚 → Set i) → P tt → P ff → (t : 𝟚) → P t
ind𝟚 P u v tt = u
ind𝟚 P u v ff = v

_∨_ : 𝟚 → 𝟚 → 𝟚
a ∨ b = if a then tt else b

tt≠ff : ¬ (tt ≡ ff)
tt≠ff e = coep P e trivi
  where
    P : 𝟚 → Prop
    P tt = ⊤
    P ff = ⊥


-- Natural numbers

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

variable
  m n : ℕ

_≡ℕ_ : ℕ → ℕ → Prop
zero  ≡ℕ zero = ⊤
zero  ≡ℕ suc _ = ⊥
suc _ ≡ℕ zero = ⊥
suc m ≡ℕ suc n = m ≡ℕ n

postulate
  ℕ≡ : _≡_ {A = ℕ} ≡ _≡ℕ_
  {-# REWRITE ℕ≡ #-}

rec : ∀{i}{A : Set i}(u : A)(v : A → A)(t : ℕ) → A
rec u v zero = u
rec u v (suc t) = v (rec u v t)

indℕ : ∀{i}(P : ℕ → Set i) → P zero → ((n : ℕ) → P n → P (suc n)) → (t : ℕ) → P t
indℕ P u v zero = u
indℕ P u v (suc t) = v t (indℕ P u v t)

_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc (m + n)

max : ℕ → ℕ → ℕ
max zero    b       = b
max (suc a) zero    = suc a
max (suc a) (suc b) = suc (max a b)


-- Sum type

data _⊎_ {i}{j}(A : Set i)(B : Set j) : Set (i ⊔ j) where
  ι₁ : A → A ⊎ B
  ι₂ : B → A ⊎ B

case : ∀ {i j k}{A : Set i}{B : Set j}{C : Set k}
       (t : A ⊎ B)(u : A → C)(v : B → C) → C
case (ι₁ t) u v = u t
case (ι₂ t) u v = v t

ind⊎ : ∀{i j k}{A : Set i}{B : Set j}(P : A ⊎ B → Set k) →
       ((a : A) → P (ι₁ a)) → ((b : B) → P (ι₂ b)) → (t : A ⊎ B) → P t
ind⊎ P u v (ι₁ t) = u t
ind⊎ P u v (ι₂ t) = v t


-- Sigma

record Σ {i}{j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σ public

record Σp {i}{j}(A : Prop i)(B : A → Prop j) : Prop (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σp public

_×_ : ∀{i}{j}(A : Set i)(B : Set j) → Set (i ⊔ j)
A × B = Σ A (λ _ → B)

_↔_ : ∀{i j}(A : Set i)(B : Set j) → Set (i ⊔ j)
A ↔ B = (A → B) × (B → A)

postulate
  Σ≡ : ∀{i}{j}{A : Set i}{B : A → Set j}{w₀ w₁ : Σ A B} →
    _≡_ {A = Σ A B} w₀ w₁ ≡ Σp (π₁ w₀ ≡ π₁ w₁) λ a₂ → (B ~) a₂ (π₂ w₀) (π₂ w₁)
  {-# REWRITE Σ≡ #-}


-- Maybe

data Maybe {i}(A : Set i) : Set i where
  Nothing  : Maybe A
  Just     : A → Maybe A

_≡Maybe_ : ∀{i}{A : Set i} → Maybe A → Maybe A → Prop i
Nothing  ≡Maybe Nothing  = Raisep ⊤
Nothing  ≡Maybe Just _   = Raisep ⊥
Just _   ≡Maybe Nothing  = Raisep ⊥
Just a₀  ≡Maybe Just a₁  = a₀ ≡ a₁

postulate
  Maybe≡ : ∀{i}{A : Set i} → _≡_ {A = Maybe A} ≡ _≡Maybe_
  {-# REWRITE Maybe≡ #-}


-- Vector

data Vec {i}(A : Set i) : ℕ → Set i where
  [] : Vec A zero
  _::_ : A → {n : ℕ} → Vec A n → Vec A (suc n)


-- reflexivity and symmetry of equality

refl : ∀{i}{A : Set i}{x : A} → x ≡ x
refl {_}{A}{x} = cong {A = Lift ⊤}{B = λ _ → A}(λ _ → x) (mk trivi)

sym : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a') → a' ≡ a
sym {a = a} a= = coep (_≡ a) a= (refl {x = a})

J : ∀{i}{A : Set i}{a : A}{j}(B : {x : A} → a ≡ x → Set j)(b : B (refl {x = a})){a' : A}(a= : a ≡ a') → B a=
J {A = A}{a} B b {a'} a= = coe {A = Σ A λ a' → Lift (a ≡ a')}(λ w → B (un (π₂ w))) {a , mk (refl {x = a})}{a' , mk a=} (a= , mk trivi) b
Jp : ∀{i}{A : Set i}{a : A}{j}(B : {x : A} → a ≡ x → Prop j)(b : B (refl {x = a})){a' : A}(a= : a ≡ a') → B a=
Jp {A = A}{a} B b {a'} a= = un (J {A = A} (λ a → Lift (B a)) (mk b) a=)

sym~ : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a'){j}{B : A → Set j}{b : B a}{b' : B a'}(b~ : (B ~) a= b b') → (B ~) (sym {A = A} a=) b' b
sym~ {A = A}{a}{a'} a= {B = B}{b}{b'} b~ = Jp {A = A} (λ {a'} a= → (b' : B a') → (B ~) a= b b' → (B ~) (sym {A = A} a=) b' b) (λ b' → sym {A = B a}) a= b' b~

~coe : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a'){j}{B : A → Set j}{b' : B a'} → (B ~) a= (coe B (sym {A = A} a=) b') b'
~coe {_}{A}{a}{a'} a= {_}{B}{b'} = Jp {A = A}{a}(λ {a'} a= → (b' : B a') → (B ~) a= (coe B (sym {A = A} a=) b') b') (λ b' → refl {x = b'}) a= b'

_∎ : ∀{ℓ}{A : Set ℓ}(x : A) → x ≡ x
x ∎ = refl {x = x}

coh : ∀{i j}{A : Set i}(B : A → Set j){a₀ a₁ : A}(a₀₁ : a₀ ≡ a₁)(b₀ : B a₀) → (B ~) a₀₁ b₀ (coe B a₀₁ b₀)
coh {A = A} B {a₀}{a₁} a₀₁ b₀ = Jp {A = A} (λ {a₁} a₀₁ → (B ~) a₀₁ b₀ (coe B a₀₁ b₀)) (refl {x = b₀}) a₀₁
