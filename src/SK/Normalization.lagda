\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module SK.Normalization where

open import SK.Algebra
open import Lib

open I
data Nf : (A : Ty) → Tm A → Set where
  K₀ : Nf (A ⇒ B ⇒ A) K
  K₁ : Nf A t → Nf (B ⇒ A) (K $ t)
  S₀ : Nf ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) S
  S₁ : Nf (A ⇒ B ⇒ C) t → Nf ((A ⇒ B) ⇒ A ⇒ C) (S $ t)
  S₂ : Nf (A ⇒ B ⇒ C) t → Nf (A ⇒ B) u → Nf (A ⇒ C) (S $ t $ u)

Norm : DepModel
Norm = record
  { Ty  = λ A' → Σ (Tm A' → Set) (λ P → ({t' : Tm A'} → P t' → Nf A' t'))
  ; Tm  = λ where (PA , QA) t' → PA t'
  ; ι   = (λ _ → Lift ⊥) , λ where ()
  ; _⇒_ = λ where {A'} (PA , QA) (PB , QB) → (λ f' → ({t' : Tm A'} → PA t' → PB (f' $ t')) × Nf _ f') , π₂
  ; _$_ = λ A t → π₁ A t
  ; K   = λ where {A = (PA , QA)} → ((λ where {t' = t'} Pt → (λ Pu → coe PA (sym {a' = t'} Kβ) Pt) , (K₁ (QA Pt))) , K₀)
  ; S   = λ where {C = (PC , QC)} →
                   ((λ where {t'} (Pt , Qt) →
                              (λ where {u'} (Pu , Qu) →
                                        (λ where {v'} v →
                                                  coe PC (sym {a = S $ t' $ u' $ v'} Sβ) (π₁ (Pt v) (Pu v))
                                        ) , S₂ Qt Qu
                              ) , S₁ Qt
                   ) , S₀)
  ; Kβ  = λ where {A = A} → refl {A = π₁ A _}
  ; Sβ  = λ where {C = C} → refl {A = π₁ C _}
  -- ; Kβ  = λ {A'} {B'} {t'} {u'} {A} {B} {t} {u} → refl {i = lsuc lzero} {A = Raise (π₁ A (I.K I.$ t' I.$ u'))} {x = mk (coe (π₁ A) (sym {a' = t'} I.Kβ) (un t)) }
  -- ; Sβ  = λ {A'} {B'} {C'} {t'} {u'} {v'} {A} {B} {C} {t} {u} {v} → refl {i = lsuc lzero} {A = Raise (π₁ C (I.S I.$ t' I.$ u' I.$ v'))} {x = mk (coe (π₁ C) (sym {a = I.S I.$ t' I.$ u' I.$ v'} I.Sβ) (π₁ (π₁ (un t) (un v)) (π₁ (un u) (un v))))}
  }

norm : (t : Tm I.A) → Nf A t
norm {A} t = π₂ ⟦ A ⟧T ⟦ t ⟧t
  where open DepModel Norm
\end{code}