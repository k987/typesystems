\chapter{SK Combinator Calculus}

\textit{Note: More commonly known as SKI Combinator Calculus, but since \texttt{I = SKK} we only use S and K.}\\
\small{(https://en.wikipedia.org/wiki/SKI\_combinator\_calculus)}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module SK.Algebra where

open import Lib
open import Agda.Primitive using (_⊔_)

module I where
  infixr 5 _⇒_
  infixl 5 _$_

  postulate
    Ty : Set
    Tm : Ty → Set

  variable
    A B C : Ty

  variable
    t u v : Tm A

  postulate
    ι   : Ty
    _⇒_ : Ty → Ty → Ty
    _$_ : Tm (A ⇒ B) → Tm A → Tm B
    K   : Tm (A ⇒ B ⇒ A)
    S   : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)

    Kβ  : K $ t $ u ≡ t
    Sβ  : S $ t $ u $ v ≡ (t $ v) $ (u $ v)

  -- {-# REWRITE Kβ Sβ #-}

record Model {i j : Level} : Set (lsuc (i ⊔ j)) where
  open I using (A ; B ; C ; t ; u ; v)

  infixr 5 _⇒_
  infixl 5 _$_

  field
    Ty  : Set i
    Tm  : Ty → Set j

    ι   : Ty
    _⇒_ : Ty → Ty → Ty
    _$_ : {A B : Ty} → Tm (A ⇒ B) → Tm A → Tm B
    K   : {A B : Ty} → Tm (A ⇒ B ⇒ A)
    S   : {A B C : Ty} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)

    Kβ  : {A B : Ty} → {t : Tm A} → {u : Tm B} →
          K $ t $ u ≡ t
    Sβ  : {A B C : Ty} → {t : Tm (A ⇒ B ⇒ C)} → {u : Tm (A ⇒ B)} → {v : Tm A} →
          S $ t $ u $ v ≡ (t $ v) $ (u $ v)

  postulate
    ⟦_⟧T : I.Ty → Ty
    ⟦ι⟧T : ⟦ I.ι ⟧T ≡ ι
    ⟦⇒⟧T : ⟦ A I.⇒ B ⟧T ≡ ⟦ A ⟧T ⇒ ⟦ B ⟧T
    {-# REWRITE ⟦ι⟧T ⟦⇒⟧T #-}

    ⟦_⟧t : I.Tm A → Tm ⟦ A ⟧T
    ⟦$⟧t : ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
    ⟦K⟧t : ⟦ I.K {A} {B} ⟧t ≡ K {⟦ A ⟧T} {⟦ B ⟧T}
    ⟦S⟧t : ⟦ I.S {A} {B} {C} ⟧t ≡ S {⟦ A ⟧T} {⟦ B ⟧T}{⟦ C ⟧T}
    {-# REWRITE ⟦$⟧t ⟦K⟧t ⟦S⟧t #-}

open I using () renaming (A to A' ; B to B' ; C to C' ; t to t' ; u to u' ; v to v')

record DepModel {i j : Level} : Set (lsuc (i ⊔ j)) where

  infixr 5 _⇒_
  infixl 5 _$_

  field
    Ty  : I.Ty → Set i
    Tm  : Ty A' → I.Tm A' → Set j

    ι   : Ty I.ι
    _⇒_ : Ty A' → Ty B' → Ty (A' I.⇒ B')

    _$_ : {A : Ty A'} → {B : Ty B'} →
          Tm (A ⇒ B) t' → Tm A u' → Tm B (t' I.$ u')

    K   : {A : Ty A'} → {B : Ty B'} → Tm (A ⇒ B ⇒ A) I.K
    S   : {A : Ty A'} → {B : Ty B'} → {C : Ty C'} →
          Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) I.S

    Kβ  : {A : Ty A'} → {B : Ty B'} → {t : Tm A t'} → {u : Tm B u'} → K $ t $ u ≡ coe (Tm A) (sym {a' = t'} I.Kβ) t
    Sβ  : {A : Ty A'} → {B : Ty B'} → {C : Ty C'} →
          {t : Tm (A ⇒ B ⇒ C) t'} → {u : Tm (A ⇒ B) u'} → {v : Tm A v'} →
          S $ t $ u $ v ≡ coe (Tm C) (sym {a = I.S I.$ t' I.$ u' I.$ v'} I.Sβ) ((t $ v) $ (u $ v))

  postulate
    ⟦_⟧T : (A' : I.Ty) → Ty A'
    ⟦ι⟧T : ⟦ I.ι ⟧T ≡ ι
    ⟦⇒⟧T : ⟦ A' I.⇒ B' ⟧T ≡ ⟦ A' ⟧T ⇒ ⟦ B' ⟧T
    {-# REWRITE ⟦ι⟧T ⟦⇒⟧T #-}

    ⟦_⟧t : (t' : I.Tm A') → Tm ⟦ A' ⟧T t'
    ⟦$⟧t : ⟦ t' I.$ u' ⟧t ≡ ⟦ t' ⟧t $ ⟦ u' ⟧t
    ⟦K⟧t : ⟦ I.K {A'} {B'} ⟧t ≡ K {A'} {B'} {⟦ A' ⟧T} {⟦ B' ⟧T}
    ⟦S⟧t : ⟦ I.S {A'} {B'} {C'} ⟧t ≡ S {A'} {B'} {C'} {⟦ A' ⟧T} {⟦ B' ⟧T}{⟦ C' ⟧T}
    {-# REWRITE ⟦$⟧t ⟦K⟧t ⟦S⟧t #-}
\end{code}
