\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module SK.Standard where

open import SK.Algebra
open import Lib

Std : Model
Std = record
  { Ty  = Set
  ; Tm  = λ A → A
  ; ι   = Lift ⊤
  ; _⇒_ = λ A B → A → B
  ; _$_ = λ f x → f x
  ; K   = λ x y → x
  ; S   = λ x y z → x z (y z)
  ; Kβ  = λ where {t = t} → refl {x = t}
  ; Sβ  = λ where {t = t} {u = u} {v = v} → refl {x = t v (u v)}
  -- Alternative (shorter) version:
  -- ; Sβ  = λ {A} {B} {C} → refl {A = C}
  }
open Model Std using (⟦_⟧T ; ⟦_⟧t)

eval : {A : I.Ty} → I.Tm A → ⟦ A ⟧T
eval t = ⟦ t ⟧t
\end{code}
