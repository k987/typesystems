\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Tree.LogicalPredicate where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Tree.Algebra
open import Tree.Standard

open I
open Algebra St using (⟦_⟧C ; ⟦_⟧T ; ⟦_⟧S ; ⟦_⟧t)
\end{code}
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀ {n'} → PNat n' → PNat (suc n')

indPNat : ∀ {i} (P : ∀ {n'} → PNat n' → Set i) →
  P Pzero → (∀ {n'} {n : PNat n'} → P n → P (Psuc n)) →
  ∀ {n'} (n : PNat n') → P n
indPNat P u v Pzero = u
indPNat P u v (Psuc n) = v (indPNat P u v n)

recPNat : ∀ {i} (A : Tm ∙ Nat → Set i) →
  A zero → (∀ {n'} → A n' → A (suc n')) →
  ∀ {n'} → PNat n' → A n'
recPNat A u v n = indPNat (λ {n'} _ → A n') u v n

Nat-Prec : ∀ {i A'} (A : Tm ∙ A' → Set i)
  {u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'} →
  A u' → (∀ {t'} → A t' → A (v' [ id , t' ])) →
  ∀ {n'} → PNat n' → A (rec u' v' n')
Nat-Prec A {u'}{v'} u v n = recPNat (λ n' → A (rec u' v' n')) u v n

data PTree : Tm ∙ Tree → Set where
  Pleaf : ∀ {n'} → PNat n' → PTree (leaf n')
  Pnode : ∀ {a' b'} → PTree a' → PTree b' → PTree (node a' b')

indPTree : ∀ {i} (P : ∀ {t'} → PTree t' → Set i) →
  (∀ {n'} (n : PNat n') → P (Pleaf n)) →
  (∀ {a' b'} {a : PTree a'}{b : PTree b'} → P a → P b → P (Pnode a b)) →
  ∀ {t'} (t : PTree t') → P t
indPTree P u v (Pleaf n) = u n
indPTree P u v (Pnode a b) = v (indPTree P u v a) (indPTree P u v b)

recPTree : ∀ {i} (A : Tm ∙ Tree → Set i) →
  (∀ {n'} → PNat n' → A (leaf n')) →
  (∀ {a' b'} → A a' → A b' → A (node a' b')) →
  ∀ {t'} → PTree t' → A t'
recPTree A u v t = indPTree (λ {t'} _ → A t') u v t

Tree-Prec : ∀ {i A'} (A : Tm ∙ A' → Set i)
  {u' : Tm (∙ ▹ Nat) A'}{v' : Tm (∙ ▹ A' ▹ A') A'} →
  (∀ {n'} → PNat n' → A (u' [ id , n' ])) →
  (∀ {a' b'} → A a' → A b' → A (v' [ id , a' , b' ])) →
  ∀ {t'} → PTree t' → A (fold u' v' t')
Tree-Prec A {u'}{v'} u v t = recPTree (λ t' → A (fold u' v' t')) u v t

LP : DepAlgebra
LP = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; Nat = PNat
         ; Tree = PTree
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; zero = λ _ → Pzero
         ; suc = λ n x → Psuc (n x)
         ; rec = λ { {A = A} u v n x →
                   Nat-Prec A (u x) (λ y → v (x ,Σ y)) (n x) }
         ; leaf = λ n x → Pleaf (n x)
         ; node = λ a b x → Pnode (a x) (b x)
         ; fold = λ { {A = A} u v t x →
                    Tree-Prec A (λ n → u (x ,Σ n))
                                (λ a b → v (x ,Σ a ,Σ b))
                                (t x) }
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; Natβ₁ = refl
         ; Natβ₂ = refl
         ; Treeβ₁ = refl
         ; Treeβ₂ = refl
         ; lam[] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; rec[] = refl
         ; leaf[] = refl
         ; node[] = refl
         ; fold[] = refl
         }
module LP = DepAlgebra LP

pNat : ∀ n → PNat n
pNat n = LP.⟦ n ⟧t {id} *↑

pTree : ∀ t → PTree t
pTree t = LP.⟦ t ⟧t {id} *↑

completenessN : ∀ {n} → ⌜ eval n ⌝N ≡ n
completenessN {n} = 
  let A n = ↑p (⌜ eval n ⌝N ≡ n)
  in  ↓[ recPNat A refl↑ (λ n → ↑[ ap suc ↓[ n ]↓ ]↑) (pNat n) ]↓

completenessT : ∀ {t} → ⌜ eval t ⌝T ≡ t
completenessT {t = t} =
  let A t = ↑p (⌜ eval t ⌝T ≡ t)
  in  ↓[ recPTree A (λ _ → ↑[ ap leaf completenessN ]↑)
                    (λ a b → ↑[ ap2 node ↓[ a ]↓ ↓[ b ]↓ ]↑)
                    (pTree t) ]↓
\end{code}
