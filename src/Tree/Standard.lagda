\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Tree.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_) hiding (𝕋)
open import Tree.Algebra

open I
\end{code}
\begin{code}
data 𝕋 : Set where
  Lf : ℕ → 𝕋
  Nd : 𝕋 → 𝕋 → 𝕋

rec𝕋 : ∀ {i} {A : Set i} → (ℕ → A) → (A → A → A) → 𝕋 → A
rec𝕋 u v (Lf n) = u n
rec𝕋 u v (Nd a b) = v (rec𝕋 u v a) (rec𝕋 u v b)

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; Tree = 𝕋
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ,Σ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; zero = λ _ → O
       ; suc = λ n γ → S (n γ)
       ; rec = λ u v n γ → indℕ _ (u γ) (λ a → v (γ ,Σ a)) (n γ)
       ; leaf = λ n γ → Lf (n γ)
       ; node = λ a b γ → Nd (a γ) (b γ)
       ; fold = λ u v t γ →
                  rec𝕋 (λ n → u (γ ,Σ n)) (λ a b → v (γ ,Σ a ,Σ b)) (t γ)
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; Natβ₁ = refl
       ; Natβ₂ = refl
       ; Treeβ₁ = refl
       ; Treeβ₂ = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; rec[] = refl
       ; leaf[] = refl
       ; node[] = refl
       ; fold[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝T : 𝕋 → Tm ∙ Tree
⌜ Lf n ⌝T = leaf ⌜ n ⌝N
⌜ Nd a b ⌝T = node ⌜ a ⌝T ⌜ b ⌝T
\end{code}
