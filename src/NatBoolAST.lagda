\chapter{A language of closed expressions}\label{ch:natbool}

\begin{tcolorbox}[title=Learning goals of this chapter]
  \begin{itemize}
  \item Levels of abstraction when defining a programming language: strings, sequences of lexical elements, abstract syntax trees, well typed syntax, well-typed syntax with equations
  \item Programs as trees: models, syntax, defining functions by recursion, dependent models, proving properties by induction
  \item Well-typed syntax, type inference, standard model and the standard interpreter
  \item Well-typed syntax with equations, normalisation, completeness and stability of normalisation
  \end{itemize}
\end{tcolorbox}

In this chapter we study a very simple programming language called
NatBool which contains e.g.\ the following program.
\begin{verbatim}
if isZero (num 0 + num 1) then false else isZero (num 0)
\end{verbatim}
Every program is either a numeric or a boolean expression. Numbers can
be formed using \verb$num i$ where \verb$i$ is a natural number. Booleans are
\verb$true$ and \verb$false$. We have the usual if-then-else operator, addition
and an isZero operator which says whether a number is 0. The above
program evaluates in the following steps (each line is a step).
\begin{verbatim}
if isZero (num 0 + num 1) then false else isZero (num 0)
if isZero (num 1) then false else isZero (num 0)
if false then false else isZero (num 0)
isZero (num 0)
true
\end{verbatim}

We will describe this language in several iterations. Each
iteration will be a more precise description of the language:
strings, sequences of lexical elements, abstract syntax trees,
well-typed syntax and well-typed syntax with equations. See Figures \ref{fig:levels}, \ref{fig:levels1}.

\begin{figure}
\begin{center}
\def\excludePos{-2.6} % -4.9
\def\quotientByPos{-2.6}
\begin{tikzpicture}
%\node[inner sep=0pt] (img) at (-9.5,6){\includegraphics[width=5.5cm]{levels.png}};
\node            (lab1) at (0,14) {\textbf{level}};
\node[left,red]  (lab2) at (\excludePos,13.5) {exclude\vphantom{g}};
\node[gray]      (lab3) at (-1.2,13) {going abstract};
\node[gray]      (lab4) at (1.4,13) {going concrete};
\node[left]      (lab5) at (\quotientByPos,13) {quotient by};
\node (str) at (0,0) {\textbf{(1) string}};
\node (lex) at (0,2) {\textbf{(2) sequence of lexical elements}};
\node (ast) at (0,4) {\textbf{(3) abstract syntax tree}};
\node (abt) at (0,6)  {\textbf{abstract binding tree}};
\node (wt)  at (0,8) {\textbf{(4) well typed syntax}};
\node (alg) at (0,10) {\textbf{(5) well typed syntax with equations}};
\node (hoas) at (0,12) {\textbf{higher order abstract syntax}};
\path[->] (str.north) edge [bend left] node[gray, left] {lexical analysis} (lex.south);
\path[->] (lex.north) edge [bend left] node[gray, left] {parsing} (ast.south);
\path[->] (ast.north) edge [bend left] node[gray, left] {scope checking} (abt.south);
\path[->] (abt.north) edge [bend left] node[gray, left] {type checking} (wt.south);
\path[->] (wt.north)  edge [bend left] node[gray, left] {} (alg.south);
\path[->] (alg.north)  edge [bend left] node[gray, left] {} (hoas.south);
\node (lex1) at (\excludePos,0.5) {};
\node (ast1) at (\excludePos,2.5) {};
\node (abt1) at (\excludePos,4.5) {};
\node (wt1)  at (\excludePos,6.5) {};
\node (alg1) at (\excludePos,8.5) {};
\node (hoas1) at (\excludePos,10.5) {};
\path[->] (str.north) edge [bend right=15] node[red, left, pos=1] {invalid lexical elements} (lex1.west);
\path[->] (lex.north) edge [bend right=15] node[red, left, pos=1] {bad number of parameters} (ast1.west);
\path[->] (ast.north) edge [bend right=15] node[red, left, pos=1] {variable not in scope} (abt1.west);
\path[->] (abt.north) edge [bend right=15] node[red, left, pos=1] {non-matching types} (wt1.west);
%\path[->] (alg.north) edge [bend right=15] node[red, left, pos=1] {operations not respecting substitution} (hoas1.west);
\draw[->] (lex) edge [bend left] node[gray, right] {add spaces} (str);
\draw[->] (ast) edge [bend left] node[gray, right] {add brackets} (lex);
\draw[->] (abt) edge [bend left] node[gray, right] {pick variable names} (ast);
\draw[->] (wt)  edge [bend left] node[gray, right] {} (abt);
\draw[->] (alg) edge [bend left] node[gray, right] {normalise} (wt);
\draw[->] (hoas) edge [bend left] node[gray, right] {} (alg);
\node[left] (qstr) at (\quotientByPos,1) {removal of spaces};
\node[left] (qlex) at (\quotientByPos,3) {removal of extra brackets};
\node[left] (qast) at (\quotientByPos,5) {renaming of bound variables};
\node[left] (qabt) at (\quotientByPos,7) {};
\node[left] (qwt)  at (\quotientByPos,9) {operational equivalence};
\end{tikzpicture}
\end{center}
\caption{Different levels of abstraction when defining a programming language and transformations between levels.
At higher levels certain programs are excluded and others identified. Abstract binding trees are sometimes called well-scoped syntax trees, see Chapter \ref{ch:def}. We don't cover higher order abstract syntax in these notes.}
\label{fig:levels} \end{figure}

\begin{figure}
\begin{minipage}[t]{0.4\textwidth}
\includegraphics[width=6cm]{levels.png}
\end{minipage}
\begin{minipage}[b]{0.4\textwidth}
\begin{alignat*}{10}
& \rlap{(5) well-typed syntax with equations} \\
& \hspace{1.6em} && \texttt{Ty}   && \texttt{: Set} \\
& && \texttt{Tm}   && \texttt{: Ty → Set} \\
& && \texttt{Bool}   && \texttt{: Ty} \\
& && \texttt{true}   && \texttt{: Tm Bool} \\
& && \texttt{false}   && \texttt{: Tm Bool} \\
& && \texttt{ite}   && \texttt{: Tm Bool → Tm A → Tm A → Tm A} \\
& && \texttt{num}   &&  \texttt{: ℕ → Tm Nat} \\
& && \texttt{isZero }  && \texttt{: Tm Nat → Tm Bool} \\
& && \texttt{\_+\_}    && \texttt{: Tm Nat → Tm Nat → Tm Nat} \\
& && \texttt{iteβ₁}    && \texttt{: ite true u v = u} \\
& && \texttt{iteβ₂}    && \texttt{: ite false u v = v} \\
& && \texttt{isZeroβ₁} && \texttt{: isZero (num 0) = true} \\
& && \texttt{isZeroβ₂ } && \texttt{: isZero (num (1+n)) = false} \\
& && \texttt{+β} && \texttt{: num m + num n = num (m + n)} \\
& \rlap{(4) well-typed syntax} \\
& && \texttt{Ty}   && \texttt{: Set} \\
& && \texttt{Tm}   && \texttt{: Ty → Set} \\
& && \texttt{Bool}   && \texttt{: Ty} \\
& && \texttt{true}   && \texttt{: Tm Bool} \\
& && \texttt{false}   && \texttt{: Tm Bool} \\
& && \texttt{ite}   && \texttt{: Tm Bool → Tm A → Tm A → Tm A} \\
& && \texttt{num}   &&  \texttt{: ℕ → Tm Nat} \\
& && \texttt{isZero }  && \texttt{: Tm Nat → Tm Bool} \\
& && \texttt{\_+\_}    && \texttt{: Tm Nat → Tm Nat → Tm Nat} \\
& \rlap{(3) abstract syntax tree} \\
& && \texttt{Tm}   && \texttt{: Set} \\
& && \texttt{true}   && \texttt{: Tm} \\
& && \texttt{false}   && \texttt{: Tm} \\
& && \texttt{ite}   && \texttt{: Tm → Tm → Tm → Tm} \\
& && \texttt{num}   &&  \texttt{: ℕ → Tm} \\
& && \texttt{isZero}  && \texttt{: Tm → Tm} \\
& && \texttt{\_+\_}    && \texttt{: Tm → Tm → Tm} \\
& \rlap{(2) list of the following lexical elements:} \\
& && \rlap{$\texttt{(, ), true, false, if, then, else, num, isZero, +, 0, 1, 2, 3, ...}$} \\
& \rlap{(1) any string}
\end{alignat*}
\end{minipage}
\caption{Left: example programs at different levels of abstractions in the NatBool expression language. Each bubble represents a separate program. Right: description of the NatBool expression language at levels (1)--(5).}
\label{fig:levels1} \end{figure}


\section{String}

As a first approximation, we say that a program is a string, that is,
a sequence of (ASCII) characters. This is how we write programs on a
computer. Any string is a program.

Many strings do not correspond to meaningful programs in our language
such as \verb$num 3 - num 2$ as we don't have subtraction. Also, there
are different strings which represent the same program. For example,
\verb$isZero (num 1)$ and \verb$isZero     (num 1)$ are different as
strings but should be the same programs as the extra spaces after
\verb$isZero$ shouldn't matter.

Instead of describing which strings are meaningful programs and
defining an equivelence relation for identifying strings that
represent the same program, we will describe programs using a more
abstract structure.

\section{Sequence of lexical elements}

We describe NatBool by the following lexical elements.
\[
\verb$($, \verb$)$, \verb$true$, \verb$false$, \verb$if$, \verb$then$,
\verb$else$, \verb$num$, \verb$isZero$, \verb$+$, \verb$0$, \verb$1$, \verb$2$, \verb$...$
\]
The \verb$...$ part means that any natural number is a lexical element.
A program is an arbitrary sequence made of these.

Our example program
\[
\verb$if isZero (num 0 + num 1) then false else isZero (num 0)$
\]
is the following sequence.
\[
[\verb$if$, \verb$isZero$, \verb$($, \verb$num$, \verb$0$, \verb$+$,
\verb$num$, \verb$0$, \verb$)$, \verb$then$, \verb$false$,
\verb$else$, \verb$isZero$, \verb$($, \verb$num$, \verb$0$, \verb$)$]
\]

Now we have much fewer programs and \verb$num 3 - num 2$ is
not a program anymore because there is no lexical element for \verb$-$. Any two programs given as strings which differ
only in the number of spaces will end up as the same program at this
level: \verb$isZero (num 1)$ and \verb$isZero     (num 1)$ are both
given by the sequence [\verb$isZero$, \verb$($, \verb$num$, \verb$1$, \verb$)$].

However, we still have meaningless programs, e.g. [\verb$($,
\verb$true$] (there is no closing parenthesis) or [\verb$num$,
\verb$1$, \verb$+$] (\verb$+$ needs two arguments), and so
on. Also, there are programs which could be identified, e.g.\
[\verb$($, \verb$true$, \verb$)$] and [\verb$true$] (the parentheses are redundant in the former).

Again, to solve these issues, we move to a higher level representation
of programs.

Note that we can always obtain a string from a sequence of lexical
elements by simply printing the sequence. In the other direction, we
can implement a lexical analyser (lexer) which turns a string into a
sequence of lexical elements or returns an error.

\begin{exe}
  Write a lexical analyser for NatBool: the input is a string, the output is either a sequence of lexical elements or an error.
\end{exe}

\section{Abstract syntax tree}

We define our language by the following BNF grammar (where \verb$N$ means a natural number):
\begin{verbatim}
  T ::= true | false | if T then T else T | num N | isZero T | T + T
\end{verbatim}
In Haskell we would write this as follows (using \verb$Int$ for natural numbers).
\begin{verbatim}
  data Tm = True | False | Ite Tm Tm Tm | Num Int | IsZero Tm | Tm :+: Tm
\end{verbatim}
We will use the following Agda notation for the same definition. Here every operator is followed by its \emph{arity}: the number of arguments it takes.
We call elements of \verb$Tm$ terms. We write \verb$ite$ instead of if-then-else and \verb$+o$ instead of $+$ for convenience.
\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module NatBoolAST where

open import Lib
module I where

  infixl 7 _+o_
\end{code}
\begin{code}
  data Tm   : Set where
    true    : Tm
    false   : Tm
    ite     : Tm → Tm → Tm → Tm
    num     : ℕ → Tm
    isZero  : Tm → Tm
    _+o_    : Tm → Tm → Tm
\end{code}
In this description we added the information that if-then-else has three,
isZero one and addition two \verb$Tm$
arguments while \verb$num$ has a natural number argument. Programs are now trees which have \verb$true$, \verb$false$
or \verb$num i$ at their leaves and they can have ternary branching with
\verb$ite$ at the branching node, unary branching with \verb$isZero$ at the node or binary branching with \verb$+$ at the
node.

Our example program
\[
\verb$if isZero (num 0 + num 1) then false else isZero (num 0)$
\]
is depicted as follows.

\begin{tikzpicture}
  \node (x10) at (0,0) {\verb$ite$};
  \node (x20) at (-1,-1) {\verb$isZero$};
  \node (x21) at (0,-1) {\verb$false$};
  \node (x22) at (1,-1) {\verb$isZero$};
  \node (x30) at (-1,-2) {\verb$+$};
  \node (x31) at (1,-2) {\verb$num 0$};
  \node (x40) at (-1.7,-3) {\verb$num 1$};
  \node (x41) at (-0.3,-3) {\verb$num 0$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x10) edge node {} (x22);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x22) edge node {} (x31);
  \draw[-] (x30) edge node {} (x40);
  \draw[-] (x30) edge node {} (x41);
\end{tikzpicture}

Instead of drawing trees we usually use linear notation to save space:
\begin{code}[hide]
ex =
\end{code}
\begin{code}
  ite (isZero (num 0 +o num 1)) false (isZero (num 0))
\end{code}
\begin{code}[hide]
  where open I
\end{code}
Programs such as \verb$(true$ and \verb$num 1 +$ which were valid as sequences of
lexical elements do not correspond to any tree, and the programs \verb$(true)$ and
\verb$true$ correspond to the same tree.

Note that \verb$(num 0 + num 0) + num 0$ and
\verb$num 0 + (num 0 + num 0)$ are different trees.

\begin{tikzpicture}
  \node (x10) at (0,0) {\verb$+$};
  \node (x20) at (-0.7,-1) {\verb$+$};
  \node (x21) at (0.7,-1) {\verb$num 0$};
  \node (x30) at (-1.4,-2) {\verb$num 0$};
  \node (x31) at (0,-2) {\verb$num 0$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \node (y10) at (4,0) {\verb$+$};
  \node (y20) at (3.3,-1) {\verb$num 0$};
  \node (y21) at (4.7,-1) {$+$};
  \node (y30) at (4,-2) {\verb$num 0$};
  \node (y31) at (5.4,-2) {\verb$num 0$};
  \draw[-] (y10) edge node {} (y20);
  \draw[-] (y10) edge node {} (y21);
  \draw[-] (y21) edge node {} (y30);
  \draw[-] (y21) edge node {} (y31);
\end{tikzpicture}

Even if the intuitive meaning of both is the number zero, they are
different as trees. An example of parentheses which even changes the
intuitive meaning of an expression is $1+(2*3) = 7 ≠ 9 = (1+2)*3$ (this example is not in the NatBool language as it has multiplication).

The \emph{meta theory} (meta language) is the language that we use to
speak about the \emph{object theory} (object language). Our meta
language is Agda (and sometimes English but we can translate all of
our English arguments to Agda), our object language is NatBool.

\begin{exe}
  Draw the trees depicting the following programs:
\begin{verbatim}
(true + true) + true
((true + true) + true) + true
(true + (true + true)) + true
true + ((true + true) + true)
ite (true + true) ((true + true) + true) (true + (true + true))
\end{verbatim}
\end{exe}

\begin{exe}
  Write down the following tree with linear notation: \\
  \begin{tikzpicture}
  \node (x10) at (0,0) {\verb$ite$};
  \node (x20) at (-2,-1) {\verb$+$};
  \node (x21) at (0,-1) {\verb$num 0$};
  \node (x22) at (2,-1) {\verb$ite$};
  \node (x30) at (-2.7,-2) {\verb$num 0$};
  \node (x31) at (-1.3,-2) {\verb$isZero$};
  \node (x32) at (1,-2) {\verb$+$};
  \node (x33) at (2,-2) {\verb$num 3$};
  \node (x34) at (3,-2) {\verb$true$};
  \node (x40) at (-1.3,-3) {\verb$num 2$};
  \node (x41) at (0.5,-3) {\verb$true$};
  \node (x42) at (1.5,-3) {\verb$false$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x10) edge node {} (x22);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \draw[-] (x22) edge node {} (x32);
  \draw[-] (x22) edge node {} (x33);
  \draw[-] (x22) edge node {} (x34);
  \draw[-] (x31) edge node {} (x40);
  \draw[-] (x32) edge node {} (x41);
  \draw[-] (x32) edge node {} (x42);
\end{tikzpicture}
\end{exe}

\begin{exe}
  Write down the following BNF notations in Agda using \verb$data$ (as we did for \verb$Tm$). \verb$N$ means natural numbers, as before.
\begin{verbatim}
  T ::= op0 | op1 T | op2 T T | op3 T T T | op4 T T T T

  A ::= a | fb B
  B ::= fa A

  V ::= vzero | vsuc V
  E ::= num N | E < E | E = E | var V
  C ::= V := E | while E S | if E then S else S
  S ::= empty | C colon S
\end{verbatim}
\end{exe}

\begin{exe}
  Write a parser for NatBool: a program which given a sequence of
  lexical elements, outputs an element of $\mathsf{Tm}$ or an error.
\end{exe}

\begin{exe}
  Which of the following strings correspond to elements of
  \verb$Tm$?
\begin{verbatim}
if true then true else num 0
if true then num 0 else num 0
if num 0 then num 0 else num 0
if if then num 0 else num 0
true + false
true + num 1
true + isZero
isZero + num 0
\end{verbatim}
\end{exe}

\begin{exe}
  After parsing them into trees, which of the following strings becomes
  equal to \verb$num 0 + (num 1 + num 1)$?
\begin{verbatim}
num 0 + ((num 1) + (num 1))
(num 0 + ((num 1) + (num 1)))
(num 0 + num 1) + num 1
((num 0 + num 1) + num 1)
num 1 + num 1
num 2
(num 0 + num 0) + (num 1 + num 1)
ite true (num 0 + (num 1 + num 1)) (num 0)
ite true (num 0 + (num 1 + num 1)) (num 0 + (num 1 + num 1))
ite true (num 0 + (num 1 + num 1)) false
\end{verbatim}
\end{exe}

\subsection{Recursion}

We call a set \verb$A$ with two elements, a ternary operation on
\verb$A$ (an \verb$A → A → A → A$ function), an infinite sequence of
\verb$A$s (a \verb$ℕ → A$ function), an endofunction (an \verb$A → A$
function) and a binary operation on \verb$A$ (an \verb$A → A → A$ function) a \emph{model} of
\emph{NatBoolAST} (sometimes called a NatBoolAST algebra). We fix a notation for
this. A NatBoolAST model (or simply model) is a record with the following fields.
\begin{code}
record Model {i} : Set (lsuc i) where
  field
    Tm      : Set i
    true    : Tm
    false   : Tm
    ite     : Tm → Tm → Tm → Tm
    num     : ℕ → Tm
    isZero  : Tm → Tm
    _+o_    : Tm → Tm → Tm
\end{code}
Note that we used the same names as for the previously defined trees. The trees are
called the \emph{syntax} of NatBool. The syntax is
also a model, we denote it by \verb$I$, its components are \verb$I.Tm$,
\verb$I.true$, \verb$I.false$, and so on. The syntax has the property that it can
be interpreted into any other model (it is an initial model, hence the name \verb$I$):
given a model \verb$M$, there is a homomorphism (a function that commutes with all operators)
called the recursor from \verb$I$ to \verb$M$ which we denote \verb$M.⟦_⟧$. Other names for the recursor: fold, catamorphism, nondependent eliminator, interpreter, evaluator.
\begin{code}
  ⟦_⟧ : I.Tm → Tm
  ⟦ I.true          ⟧ = true
  ⟦ I.false         ⟧ = false
  ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
  ⟦ I.num n         ⟧ = num n
  ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
  ⟦ t I.+o t'       ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧
\end{code}
The syntax is also called the free model: we start with \verb$I.true$, \verb$I.false$,
\verb$I.num i$ (for all \verb$i$s) and then we can freely apply operators on these
and then on those terms produced by the operators, and so on, and all syntactic terms are obtained this way.

For example, the NatBoolAST model \verb$H$ (height) is the following.
\begin{code}
H : Model
H = record
  {  Tm      = ℕ
  ;  true    = 0
  ;  false   = 0
  ;  ite     = λ n n' n'' → 1 + max n (max n' n'')
  ;  num     = λ n → 0
  ;  isZero  = λ n → 1 + n
  ;  _+o_    = λ n n' → 1 + max n n'
  }
\end{code}
Note that we distinguish two addition operations:
\verb$+o$ is the addition operator in a model (\verb$H$ in this case), while \verb$+$ is
just addition of (metatheoretic) natural numbers.

If our example term \verb$ite (isZero (zero + suc zero)) false (isZero zero)$
is interpreted in the model \verb$H$, we obtain the height of the tree.
\begin{code}[hide]
module H = Model H
evalH : H.ite (H.isZero (H.num 0 H.+o H.num 1)) H.false (H.isZero (H.num 0)) ≡s 3
evalH =
\end{code}
\begin{code}
  H.ite   (H.isZero (H.num 0 H.+o H.num 1))          H.false (H.isZero (H.num 0))   ≡≡
  1 + max (H.isZero (H.num 0 H.+o H.num 1))    (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max (1 + (H.num 0 H.+o H.num 1))         (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max (1 + (1 + max (H.num 0) (H.num 1)))  (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max (1 + (1 + max 0 0))                  (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max (1 + (1 + 0))                        (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max 2                                    (max  H.false (H.isZero (H.num 0)))  ≡≡
  1 + max 2                                    (max  0       (H.isZero (H.num 0)))  ≡≡
  1 + max 2                                    (max  0       (1 + (H.num 0)))       ≡≡
  1 + max 2                                    (max  0       (1 + 1))               ≡≡
  1 + max 2                                    (max  0       2)                     ≡≡
  1 + max 2                                    2                                    ≡≡
  1 + 2                                                                             ≡≡
  3                                                                                 ∎∎
\end{code}

The metatheoretic set of booleans \verb$𝟚$ has two elements \verb$tt$ and \verb$ff$. We denote logical disjunction by \verb$∨$.

We define another model \verb$T$ where terms are elements of \verb$𝟚$ and a term is \verb$tt$ if and only if it contains \verb$true$.
\begin{code}
T : Model
T = record
  {  Tm      = 𝟚
  ;  true    = tt
  ;  false   = ff
  ;  ite     = λ t t' t'' → t ∨ t' ∨ t''
  ;  num     = λ n → ff
  ;  isZero  = λ t → t
  ;  _+o_    = λ t t' → t ∨ t'
  }
\end{code}
Now
\begin{code}[hide]
module T = Model T
evalT : T.ite (T.isZero (T.num 0 T.+o T.num 1)) T.false (T.isZero (T.num 0)) ≡s ff
evalT =
\end{code}
\begin{code}
  T.ite  (T.isZero (T.num 0 T.+o T.num 1))     T.false     (T.isZero (T.num 0))  ≡≡
         (T.isZero (T.num 0 T.+o T.num 1))  ∨  T.false  ∨  (T.isZero (T.num 0))  ≡≡
         (T.num 0  T.+o  T.num 1)           ∨  ff       ∨  (T.num 0)             ≡≡
         (T.num 0  ∨     T.num 1)           ∨  ff       ∨  ff                    ≡≡
         (ff       ∨     ff)                ∨  ff       ∨  ff                    ≡≡
         ff                                                                      ∎∎
\end{code}
and
\begin{code}[hide]
evalT' : T.ite T.true (T.num 0) T.false ≡s tt
evalT' =
\end{code}
\begin{code}
  T.ite T.true (T.num 0) T.false ≡≡ T.true ∨ (T.num 0) ∨ T.false ≡≡ tt ∨ ff ∨ ff ≡≡ tt
\end{code}
\begin{code}[hide]
  ∎∎
\end{code}

\begin{exe}\label{exe:natbool-trues}
  Define a model where a term is a natural number and if we interpret a syntactic term in the model,
  we get the number of \verb$true$s in the tree. For example, \verb$false +o num 3 = 0$,
  \verb$true +o num 3 = 1$, \verb$true +o true = 2$.
\end{exe}

\begin{exe}\label{exe:natbool-size}
  Define a model which calculates the number of nodes in a tree, e.g.\
  \verb$num 1 = 1$, \verb$isZero (num 1) = 2$,
  \verb$isZero (num 1) + true = 4$.
\end{exe}

We have that for any model \verb$M$
\begin{code}[hide]
evalM : ∀{i}{M : Model {i}} → let module M = Model M in
\end{code}
\begin{code}
  M.⟦  I.ite  (I.isZero  (I.num 0  I.+o I.num  1)) I.false  (I.isZero  (I.num 0)) ⟧ ≡
       M.ite  (M.isZero  (M.num 0  M.+o M.num  1)) M.false  (M.isZero  (M.num 0))
\end{code}
\begin{code}[hide]
evalM {M = M} = refl {x = M.ite  (M.isZero  (M.num 0 M.+o M.num 1)) M.false  (M.isZero  (M.num 0))}
  where module M = Model M
\end{code}
and similarly for any other syntactic term.

In functional languages like Haskell or Agda, using the recursor on a
model is a special case of a recursive definition using pattern
matching. For example, \verb$H.⟦_⟧$ corresponds to a \verb$height$
function defined by pattern matching:
\begin{AgdaAlign}
\begin{code}
height : I.Tm → ℕ
height I.true            = 0
height I.false           = 0
height (I.ite t t' t'')  = 1 + max (height t) (max (height t') (height t''))
height (I.num n)         = 0
height (I.isZero t)      = 1 + height t
height (t I.+o t')       = 1 + max (height t) (height t')
\end{code}

\AgdaNoSpaceAroundCode{}
\begin{verbatim}
H.⟦_⟧ : I.Tm → ℕ
\end{verbatim}
\begin{code}[hide]
evalHtrue :
\end{code}
\begin{code}
  H.⟦ I.true ⟧           ≡ 0
\end{code}
\begin{code}[hide]
evalHfalse :
\end{code}
\begin{code}
  H.⟦ I.false ⟧          ≡ 0
\end{code}
\begin{code}[hide]
evalHite : ∀{t t' t''} →
\end{code}
\begin{code}
  H.⟦ I.ite t t' t'' ⟧   ≡ 1 + max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)
\end{code}
\begin{code}[hide]
evalHnum : ∀{n} →
\end{code}
\begin{code}
  H.⟦ I.num n ⟧          ≡ 0
\end{code}
\begin{code}[hide]
evalHisZero : ∀{t} →
\end{code}
\begin{code}
  H.⟦ I.isZero t ⟧       ≡ 1 + H.⟦ t ⟧
\end{code}
\begin{code}[hide]
evalHplus : ∀{t t'} →
\end{code}
\begin{code}
  H.⟦ t I.+o t' ⟧        ≡ 1 + max H.⟦ t ⟧ H.⟦ t' ⟧
\end{code}
\begin{code}[hide]
evalHtrue = trivi
evalHfalse = trivi
evalHite {t}{t'}{t''} = refl {x = max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
evalHnum = trivi
evalHisZero {t} = refl {x = H.⟦ t ⟧}
evalHplus {t}{t'} = refl {x = 1 + max H.⟦ t ⟧ H.⟦ t' ⟧}
\end{code}
\end{AgdaAlign}
\AgdaSpaceAroundCode{}

Some Haskell functions defined by pattern matching are not expressible using a
model and the recursor. Haskell allows nonterminating recursion or
recursion where some cases are not handled, as in the following
examples.
\begin{verbatim}
f : I.Tm → ℕ
f t = f t

g : I.Tm → ℕ
g I.true  = 3
g I.false = 1
\end{verbatim}
In Agda, all functions defined by pattern matching are also expressible
using the recursor (or using induction, see below). For details see
\cite{conorthesis,andreasfoetus}.

The syntactic operators are \emph{disjoint}: for example,
\verb$I.true$ is not equal to \verb$I.false$. We can show this using
a model where terms are metatheoretic booleans, \verb$true$ is metatheoretic true, \verb$false$ is
metatheoretic false. The other components don't matter, here we just
set them to constant false.
\begin{code}
TF : Model
TF = record
  {  Tm      = 𝟚
  ;  true    = tt
  ;  false   = ff
  ;  ite     = λ _ _ _ → ff
  ;  num     = λ _ → ff
  ;  isZero  = λ _ → ff
  ;  _+o_    = λ _ _ → ff
  }
\end{code}
\begin{code}[hide]
module TF = Model TF
\end{code}
Now we can use the recursor and \verb$congruence$ of equality to
obtain a contradiction from \verb$I.true ≡ I.false$:
\begin{code}
true≠false : ¬ (I.true ≡ I.false)
true≠false e = tt≠ff (cong TF.⟦_⟧ e)
\end{code}

\begin{exe}
  Prove that \verb$I.num 0 I.+o I.num 0$ is not equal \verb$I.num 0$.
\end{exe}
\begin{exe}
  Prove that \verb$I.isZero (I.num 100)$ is not equal \verb$I.isZero (I.num 101)$.
\end{exe}

However there are models where \verb$true = false$, e.g.\ the
trivial model where terms are given by the set with one element
\verb$tt$ and all operators return this element.
\begin{code}
Triv : Model
Triv = record
  {  Tm      = Lift ⊤
  ;  true    = _
  ;  false   = _
  ;  ite     = _
  ;  num     = _
  ;  isZero  = _
  ;  _+o_    = _
  }
\end{code}

\begin{exe}
  Create a model where \verb$true = false$, but \verb$true ≠ num 0$.
\end{exe}

Note that the height of every syntactic term (element of \verb$I.Tm$)
is finite, e.g.\ there is no tree \verb$I.suc (I.suc (I.suc ⋯))$ (an
infinitely deep tree with \verb$I.suc$ at each node) because it would
be mapped to an infinite natural number by \verb$H.⟦_⟧$ which does not
exist. This is not true for arbitrary models: there can be models
where \verb$Tm$ has infinite elements, for example \verb$Tm = ℕ → 𝟚$
(a term is an infinite sequence of booleans).

% \begin{exe}
%   Define a NatBoolAST model where terms are \verb$ℕ → 𝟚$ sequences,
%   \verb$true$ is constant \verb$I$, \verb$false$ is constant \verb$O$ and
%   (following C) every sequence which is not constant \verb$O$ is also interpreted
%   as \verb$true$. A number is the number of \verb$I$s in a sequence. Addition
%   creates a sequence where the number of \verb$I$s is the sum of
%   the number of \verb$I$s in the two numbers.
% \end{exe}

\begin{exe}\label{ex:cstyle}
Define a NatBoolAST model where terms are natural numbers, booleans
are interpreted in the C style: false is $0$, true is $1$ and for
if-then-else, anything that is not $0$ is interpreted as true. We can call
this model the degenerate standard model.
\end{exe}

\begin{exe}
  Is there a model where \verb$Tm = Lift ⊥$?
\end{exe}

\begin{exe}
Show that for any two models \verb$M$ and \verb$N$ there is a model
where \verb$Tm = M.Tm × N.Tm$.
\end{exe}

\begin{exe}
  Create a model where \verb$Tm = 𝟚$, \verb$num 0 = tt$ and all other operators are
  $ff$. With the help of this, create a function \verb$isZero : I.Tm → 𝟚$. Then
  create an optimisation \verb$I.Tm → I.Tm$ which maps \verb$I.num 0 + t$ to
  \verb$t$ and \verb$t + I.num 0$ to \verb$t$.
\end{exe}

\begin{exe}
  Consider a subset of NatBool, the programming language Nat:
  \begin{verbatim}
    Tm : Set
    zero : Tm
    suc : Tm → Tm
    _+_ : Tm → Tm → Tm
  \end{verbatim} Define a model \verb$St$ such that \verb$St.⟦_⟧ : I.Tm → ℕ$ is an
  interpreter for this language (where \verb$I$ is the syntax for this language).
  E.g.\ \verb$St.⟦num 1 + num 3⟧ = 4$.
\end{exe}

\subsection{Induction}

In addition to recursion, the syntax supports induction which is a
generalisation (dependent variant) of recursion. Induction can be
stated by saying that for any \emph{dependent model} \verb$D$ there
is a \emph{dependent homomorphism} from \verb$I$ to \verb$D$. A
\emph{dependent model} (dependent algebra, displayed algebra/model) is
given by the following components.
\begin{code}
record DepModel {i} : Set (lsuc i) where
  field
    Tm      : I.Tm → Set i
    true    : Tm I.true
    false   : Tm I.false
    ite     : ∀{t u v} → Tm t → Tm u → Tm v → Tm (I.ite t u v)
    num     : (n : ℕ) → Tm (I.num n)
    isZero  : ∀{t} → Tm t → Tm (I.isZero t)
    _+o_    : ∀{u v} → Tm u → Tm v → Tm (u I.+o v)
\end{code}
In a dependent model, instead of a set of terms, there is a family
of sets indexed over \verb$I.Tm$, that is, there is a set \verb$Tm t$
for each element \verb$t : I.Tm$ (See Figure \ref{fig:depmodel}). Then we have elements of these
families at each syntactic operator as index. So, we have a \verb$Tm I.true$, a
\verb$Tm I.false$, $\dots$, a \verb$Tm (t I.+o t')$. For those
operators with parameters, we also have induction hypotheses, for
example we have \verb$Tm t$ and \verb$Tm t'$ as inputs for \verb$_+o_$.

\begin{figure}
\begin{center}
\begin{tikzpicture}
\draw (0,1) circle (0.9cm);
\draw (2,1) circle (0.9cm);
\draw (4,1) circle (0.9cm);
\draw (6,1) circle (0.9cm);
\node (d1) at (0,2.25) {\verb$D.Tm t$};
\node (d2) at (2,2.25) {\verb$D.Tm t'$};
\node (d3) at (4,2.25) {\verb$D.Tm (t I.+o t')$};
\node (d4) at (6,2.25) {\verb$D.Tm I.true$};
\draw[rounded corners=12pt] (-1,-1) rectangle ++(10,0.9);
\node (i) at (-1.5,-0.5) {\verb$I.Tm$};
\node (i1) at (0,-0.5) {\verb$t$};
\node (i2) at (2,-0.5) {\verb$t'$};
\node (i3) at (4,-0.5) {\verb$(t I.+o t')$};
\node (i4) at (6,-0.5) {\verb$I.true$};
\node (i4) at (8,-0.5) {$\dots$};
\node (d5) at (0,1) {\verb$u$};
\node (d6) at (2,1) {\verb$u'$};
\node (d7) at (4,1) {\verb$(u D.+o u')$};
\node (d8) at (6,1) {\verb$D.true$};
\node (d8) at (8,1) {$\dots$};
\end{tikzpicture}
\end{center}
\cprotect\caption{Depiction of a dependent model \verb$D$.
There is a separate set \verb$D.Tm t$ for every
syntactic term \verb$t$.}
\label{fig:depmodel}
\end{figure}

The dependent homomorphism from the syntax is called \emph{induction}
(induction principle, eliminator). This is a dependent function
which for an input \verb$t : I.Tm$ outputs a \verb$Tm t$. Moreover, as in
the non-dependent case, it maps each syntactic operator to its
counterpart in the dependent model.
\begin{code}
  ⟦_⟧ : (t : I.Tm) → Tm t
  ⟦ I.true          ⟧ = true
  ⟦ I.false         ⟧ = false
  ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
  ⟦ I.num n         ⟧ = num n
  ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
  ⟦ t I.+o t'       ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧
\end{code}

A special case of the dependent model is when \verb$Tm t = Lift (P t)$
for some \verb$P : I.Tm → Prop$, that is, \verb$Tm$ is a predicate (unary
relation) on syntactic terms. In this case the other components say
that each syntactic operator respects the predicate. For example, if
the predicate holds for \verb$t$ and \verb$t'$, then it also holds for
\verb$t I.+o t'$. Cf.\ natural number models (a set with an element and an endofunction) and induction on natural
numbers, see Section \ref{sec:natural}. All inductively defined sets come with a notion
of model, recursion, dependent model and induction. Other well-known
examples are lists, binary trees.

An example is the dependent model expressing that the number of
\verb$true$s in a syntactic term is less than or equal to \verb$3$ to
the power the height of the tree: \verb$D.Tm t$ has an inhabitant
exactly when \verb$N.⟦t⟧$ is less than or equal to \verb$3 ^ (H.⟦t⟧)$.
Here \verb$N$ is the model defined in Exercise \ref{exe:natbool-trues}
and \verb$H$ is the model defined for calculating the height of a
term.

% \begin{code}[hide]
% import Lemmas
%
% D-1 : {t : I.Tm} →
%   ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
%   ↑p (T.⟦ I.suc t ⟧ ≤ 3 ^ℕ H.⟦ I.suc t ⟧)
% D-1 le = ↑[ Lemmas.≤-+ℕ-r ↓[ le ]↓ ]↑
%
% D-2 : {t t' : I.Tm} →
%   ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
%   ↑p (T.⟦ t' ⟧ ≤ 3 ^ℕ H.⟦ t' ⟧) →
%   ↑p (T.⟦ t I.+ t' ⟧ ≤ 3 ^ℕ H.⟦ t I.+ t' ⟧)
% D-2 {t} {t'} le1 le2 = ↑[
%       (Lemmas.+ℕ-≤-mono ↓[ le1 ]↓ ↓[ le2 ]↓)
%     Lemmas.≤-◾
%       Lemmas.≤-+ℕ-l {c = 3 ^ℕ max H.⟦ t ⟧ H.⟦ t' ⟧}
%         (Lemmas.+ℕ-≤-mono
%           (Lemmas.^ℕ-≤-mono-r {b = H.⟦ t ⟧} (s≤s z≤n) Lemmas.≤-max-l)
%           (Lemmas.≤-+ℕ-r
%             (Lemmas.^ℕ-≤-mono-r
%               {b = H.⟦ t' ⟧} {b' = max H.⟦ t ⟧ H.⟦ t' ⟧}
%               (s≤s z≤n) Lemmas.≤-max-r
%             )
%           )
%         )
%   ]↑
%
% D-3 : {t t' t'' : I.Tm} →
%   ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
%   ↑p (T.⟦ t' ⟧ ≤ 3 ^ℕ H.⟦ t' ⟧) →
%   ↑p (T.⟦ t'' ⟧ ≤ 3 ^ℕ H.⟦ t'' ⟧) →
%   ↑p (T.⟦ I.ite t t' t'' ⟧ ≤ 3 ^ℕ H.⟦ I.ite t t' t'' ⟧)
% D-3 {t} {t'} {t''} le1 le2 le3 = ↑[
%       Lemmas.+ℕ-≤-mono (Lemmas.+ℕ-≤-mono ↓[ le1 ]↓ ↓[ le2 ]↓) ↓[ le3 ]↓
%     Lemmas.≤-◾
%       Lemmas.+ℕ-≤-mono
%         {a = 3 ^ℕ H.⟦ t ⟧ +ℕ 3 ^ℕ H.⟦ t' ⟧}
%         {b = 3 ^ℕ H.⟦ t'' ⟧}
%         {b' = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
%         (Lemmas.+ℕ-≤-mono
%           {a = 3 ^ℕ H.⟦ t ⟧}
%           {a' = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
%           (Lemmas.^ℕ-≤-mono-r
%             {b = H.⟦ t ⟧}
%             {b' = max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
%             (s≤s z≤n) (Lemmas.≤-max-3a {b = H.⟦ t' ⟧})
%           )
%           (Lemmas.≤-+ℕ-r
%             {b = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
%             (Lemmas.^ℕ-≤-mono-r {b = H.⟦ t' ⟧} (s≤s z≤n) (Lemmas.≤-max-3b {a = H.⟦ t ⟧}))
%           )
%         )
%         (Lemmas.^ℕ-≤-mono-r
%           {b = H.⟦ t'' ⟧}
%           -- {b' = max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
%           (s≤s z≤n) (Lemmas.≤-max-3c {a = H.⟦ t ⟧})
%         )
%     Lemmas.≤-◾
%       Lemmas.≤-≡ (Lemmas.+ℕ-comm {b = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)})
%   ]↑
% \end{code}
% \begin{code}
% D : DepModel
% D = record
%   { Tm      = λ t → ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧)
%   ; true    = ↑[ s≤s z≤n ]↑
%   ; false   = ↑[ z≤n ]↑
%   ; ite     = λ {t}{t'}{t''} → D-3 {t}{t'}{t''}
%   ; zero    = ↑[ z≤n ]↑
%   ; suc     = λ {t} → D-1 {t}
%   ; isZero  = λ {t} → D-1 {t}
%   ; _+_     = λ {t}{t'} → D-2 {t}{t'}
%   }
% \end{code}

\begin{verbatim}
D.Tm t := N.⟦ t ⟧ ≤ 3 ^ (H.⟦ t ⟧)
D.true : N.⟦ I.true ⟧ = 1 ≤ 1 = 3 ^ 0 = 3 ^ (H.⟦ I.true ⟧)
D.false : N.⟦ I.false ⟧ = 0 ≤ 1 = 3 ^ 0 = 3 ^ (H.⟦ I.false ⟧)
D.ite tᴰ tᴰ' tᴰ'' :
  N.⟦ I.ite t t' t'' ⟧ =
  N.⟦t⟧ + N.⟦t'⟧ + N.⟦t''⟧ ≤(tᴰ,tᴰ',tᴰ'')
  3 ^ (H.⟦t⟧) + 3 ^ (H.⟦t'⟧) + 3 ^ (H.⟦t''⟧) ≤
  3 ^ (H.⟦t⟧) + 3 ^ (H.⟦t'⟧) + 3 ^ (H.⟦t''⟧) ≤
  3 * 3 ^ (max H.⟦t⟧ (max H.⟦t'⟧ H.⟦t''⟧)) =
  3 ^ (1 + max H.⟦t⟧ (max H.⟦t'⟧ H.⟦t''⟧)) =
  3 ^ (H.⟦ I.ite t t' t'' ⟧)
D.num n : N.⟦ I.num n ⟧ = 0 ≤ 1 = 3 ^ 0 = 3 ^ (H.⟦ I.num n ⟧)
D.isZero tᴰ : N.⟦ I.isZero t ⟧ = N.⟦ t ⟧ ≤(tᴰ) 3 ^ (H.⟦ t ⟧) ≤ 3 ^ (1 + H.⟦ t ⟧) = 3 ^ (H.⟦ I.isZero t ⟧)
tᴰ D.+o tᴰ' :
  N.⟦ t I.+o t' ⟧ =
  N.⟦ t ⟧ + N.⟦ t' ⟧ ≤(tᴰ,tᴰ')
  3 ^ (H.⟦ t ⟧) + 3 ^ (H.⟦ t' ⟧) ≤
  2 * 3 ^ (max H.⟦ t ⟧ H.⟦ t' ⟧) ≤
  3 * 3 ^ (max H.⟦ t ⟧ H.⟦ t' ⟧) ≤
  3 ^ (1 + max H.⟦ t ⟧ H.⟦ t' ⟧) =
  3 ^ (H.⟦ t I.+o t' ⟧)
\end{verbatim}

Applying the induction principle on the dependent model $D$, we
obtain a function
\begin{verbatim}
  D.⟦_⟧ : (t : I.Tm) → T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧
\end{verbatim}

\begin{exe}
  Show that every model can be turned into a dependent model, and
  derive the recursor using the induction principle.
\end{exe}

\begin{exe}
  Prove that \verb$H.⟦ t ⟧ ≤ S.⟦ t ⟧$ where \verb$S$ is the model defined in
  exercise \ref{exe:natbool-size}.
\end{exe}

\begin{exe}
  Prove that \verb$I.isZero$ is injective: if \verb$I.isZero t ≡ I.isZero t'$, then \verb$t ≡ t'$.
  Define a model \verb$M$ where \verb$M.isZero$ is not injective.
\end{exe}

\begin{exe}
Define a model which counts the number of \verb$true$s and \verb$false$s in a term.
Define a model which counts the number of \verb$true$s in a term. Show
that interpreting a syntactic term in the first one always gives a greater
or equal number than interpreting a term in the second one.
\end{exe}
