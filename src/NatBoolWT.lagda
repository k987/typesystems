\section{Well-typed syntax}

The abstract syntax tree description of NatBool still contains
meaningless programs, e.g.\ \verb$isZero true$ or \verb$true + num 2$.
The problem is that \verb$isZero$ expects a natural number and
not a boolean and \verb$+$ expects two natural numbers. The solution
is to add the type information to the terms, that is, whether a term
is a boolean or a natural number.

NatBoolWT (well-typed) contains two sorts: \verb$Ty$ and \verb$Tm$,
and the latter sort is indexed by the first one. 
\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module NatBoolWT where

open import Lib

module I where
  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty
  data Tm   : Ty → Set where
    true    : Tm Bool
    false   : Tm Bool
    ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
    num     : ℕ → Tm Nat
    isZero  : Tm Nat → Tm Bool
    _+o_    : Tm Nat → Tm Nat → Tm Nat
\end{code}
A NatBoolWT model is a record with the following components.
\begin{code}
record Model {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty      : Set i
    Tm      : Ty → Set j
    Nat     : Ty
    Bool    : Ty
    true    : Tm Bool
    false   : Tm Bool
    ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
    num     : ℕ → Tm Nat
    isZero  : Tm Nat → Tm Bool
    _+o_    : Tm Nat → Tm Nat → Tm Nat
\end{code}

There is no set of terms anymore, but there is a set of
\verb$Nat$-terms and a set of \verb$Bool$-terms, separately. Each
operator expects a term of the correct type as input and returns a
term of the appropriate type. For example, the first argument of
\verb$ite$ has to be a boolean and the second and third argument have
to be of the same type. The type of \verb$ite t u v $ is the same as
the type of the second and third arguments. This type is an
\emph{implicit argument} of \verb$ite$ which we omit for readability
(the full type would be \verb$(A : Ty) → Tm Bool → Tm A → Tm A → Tm A$,
now it is \verb${A : Ty} → Tm Bool → Tm A → Tm A → Tm A$).

We could have used two sorts, \verb$TmBool$ and \verb$TmNat$ instead
however that would not generalise to function types (Chapter
\ref{TODO}). 

The syntax of NatBoolWT is still called \verb$I$ and its recursor
consists of two functions (one for each sort, denoted by
\verb$⟦_⟧T$ and \verb$⟦_⟧t$) which preserve all operations.
\begin{code}
  ⟦_⟧T  : I.Ty → Ty
  ⟦_⟧t  : ∀{A} → I.Tm A → Tm ⟦ A ⟧T
  ⟦ I.Nat           ⟧T = Nat
  ⟦ I.Bool          ⟧T = Bool
  ⟦ I.true          ⟧t = true
  ⟦ I.false         ⟧t = false
  ⟦ I.ite t u v     ⟧t = ite ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
  ⟦ I.num n         ⟧t = num n
  ⟦ I.isZero t      ⟧t = isZero ⟦ t ⟧t
  ⟦ u I.+o v        ⟧t = ⟦ u ⟧t +o ⟦ v ⟧t
\end{code}

We also have a notion of dependent model and induction just as for \verb$NatBoolAST$.
\begin{code}
record DepModel {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty      :  I.Ty → Set i
    Tm      :  ∀{Aᴵ} → Ty Aᴵ → I.Tm Aᴵ → Set j
    Nat     :  Ty I.Nat
    Bool    :  Ty I.Bool
    true    :  Tm Bool I.true
    false   :  Tm Bool I.false
    ite     :  ∀{Aᴵ tᴵ uᴵ vᴵ}{A : Ty Aᴵ} → Tm Bool tᴵ → Tm A uᴵ → Tm A vᴵ →
               Tm A (I.ite tᴵ uᴵ vᴵ)
    num     :  (n : ℕ) → Tm Nat (I.num n)
    isZero  :  ∀{tᴵ} → Tm Nat tᴵ → Tm Bool (I.isZero tᴵ)
    _+o_    :  ∀{uᴵ vᴵ} → Tm Nat uᴵ → Tm Nat vᴵ → Tm Nat (uᴵ I.+o vᴵ)
  ⟦_⟧T  : (Aᴵ : I.Ty) → Ty Aᴵ
  ⟦_⟧t  : ∀{Aᴵ}(tᴵ : I.Tm Aᴵ) → Tm ⟦ Aᴵ ⟧T tᴵ
  ⟦ I.Nat           ⟧T = Nat
  ⟦ I.Bool          ⟧T = Bool
  ⟦ I.true          ⟧t = true
  ⟦ I.false         ⟧t = false
  ⟦ I.ite tᴵ uᴵ vᴵ  ⟧t = ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
  ⟦ I.num n         ⟧t = num n
  ⟦ I.isZero tᴵ     ⟧t = isZero ⟦ tᴵ ⟧t
  ⟦ uᴵ I.+o vᴵ      ⟧t = ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
\end{code}

\begin{exe}
  Implement the size, height and the number of \verb$true$s functions for NatBoolWT, show that \verb$trues t ≤ 3 ^ℕ height t$. See the previous section.
\end{exe}

\subsection{Type inference}

The goal of type inference is to turn a syntactic NatBoolAST term into
a well typed (NatBoolWT) term. To achieve this, we try to build a
NatBoolAST model using NatBoolWT terms. This is not really possible
because e.g.\ NatBoolAST requires us to add any two terms using
\verb$_+_$, but the addition that we have only works for terms of type
\verb$Nat$. Terms in our NatBoolAST algebra are either a pair of a
NatBoolWT type and a term of that type or a failure. We use
\verb$Maybe$ to express possible failure: \verb$Just (A, t)$ and
\verb$Nothing$ are both in the set \verb$Maybe (Σ I.Ty I.Tm)$, the
latter expresses failure. Failure propagates through the operations \verb$ite$, \verb$isZero$ and \verb$_+_$.
\begin{code}[hide]
import NatBoolAST
\end{code}
\begin{code}
Inf : NatBoolAST.Model
Inf = record
  { Tm      = Maybe (Σ I.Ty I.Tm)
  ; true    = Just (I.Bool , I.true)
  ; false   = Just (I.Bool , I.false)
  ; ite     = ite
  ; num     = λ n → Just (I.Nat , I.num n)
  ; isZero  = isZero
  ; _+o_    = _+o_
  }
  where
    ite : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm)
    ite (Just (I.Bool , t))  (Just (I.Nat   , u))  (Just (I.Nat   , v))  = Just (I.Nat  , I.ite t u v)
    ite (Just (I.Bool , t))  (Just (I.Bool  , u))  (Just (I.Bool  , v))  = Just (I.Bool , I.ite t u v)
    ite _                    _                     _                     = Nothing
          
    isZero : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm)
    isZero (Just (I.Nat , t))  = Just (I.Bool , I.isZero t)
    isZero _                   = Nothing

    _+o_ : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm)
    Just (I.Nat , t)  +o Just (I.Nat , t')  = Just (I.Nat , (t I.+o t'))
    _                 +o _                  = Nothing
module Inf = NatBoolAST.Model Inf
\end{code}
Interpretation into this model gives us type inference.
\begin{code}
infer : NatBoolAST.I.Tm → Maybe (Σ I.Ty I.Tm)
infer = Inf.⟦_⟧
\end{code}

\subsection{Standard interpretation}

We define the standard model (set model, denotational
semantics). The idea is that all operators in our object language are
given by their metatheoretic counterparts.
\begin{code}

St : Model
St = record
  { Ty      = Set
  ; Tm      = λ T → T
  ; Nat     = ℕ
  ; Bool    = 𝟚
  ; true    = tt
  ; false   = ff
  ; ite     = if_then_else_
  ; num     = λ n → n
  ; isZero  = λ { zero → tt ; _ → ff }
  ; _+o_    = _+_
  }
module St = Model St
\end{code}
Using the recursor we get an intepreter (the metacircular interpreter, evaluator, normaliser) for our language:
\begin{code}
norm : ∀{A} → I.Tm A → St.⟦ A ⟧T
norm = St.⟦_⟧t
\end{code}

We were not able to define the standard interpreter for NatBoolAST
because there we had meaningless programs. For example, we would have
had to interpret both \verb$Bool$ and \verb$Nat$ by the same set
(c.f.\ Exercise \ref{ex:cstyle}).

\subsection{Typing relation}

The traditional way of defining the well-typed syntax is by defining a
binary relation between syntactic types and syntactic NatBoolAST
terms (see e.g.\ \cite{harper}). This relation is defined inductively as follows.
\begin{code}
module I' = NatBoolAST.I
data _⦂_  :  I'.Tm → I.Ty → Prop where
  true    :
                               -----------------------
                               I'.true ⦂ I.Bool
  false   :
                               -----------------------
                               I'.false ⦂ I.Bool
  ite     :  ∀{t u v A} →
                               t ⦂ I.Bool                →
                               u ⦂ A                     →
                               v ⦂ A                     →
                               -----------------------
                               I'.ite t u v ⦂ A
  num     :  ∀{n} →
                               -----------------------
                               I'.num n ⦂ I.Nat
  isZero  :  ∀{t} →
                               t ⦂ I.Nat                 →
                               -----------------------
                               I'.isZero t ⦂ I.Bool
  _+o_    :  ∀{u v} →
                               u ⦂ I.Nat                 →
                               v ⦂ I.Nat                 →
                               -----------------------
                               (u I'.+o v) ⦂ I.Nat
\end{code}
\begin{exe}
  Show that there is an isomorphism between \verb$I.Tm A$ and \verb$Σ I'.Tm (_⦂ A)$.
\end{exe}

We defined well-typed terms directly because we do not want to talk
about non well-typed terms in the same way as we are not interested in
programs with non-matching brackets.
