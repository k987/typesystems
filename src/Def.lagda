\section{Well-typed syntax with equations}
\label{sec:def}

If we have local definitions , then in addition to the equations such as e.g.\ \verb$isZero (num 0) = true$,
we need equations like the following.
\begin{verbatim}
def t (v0 + v0) = t + t
def t (def t' (v0 + v1)) = t' + t
def t (def t' (v1 + (v0 + (v1 + num 3)))) = t + (t' + (t + num 3))\end{verbatim}
In general, we would like to have \verb$def t t' = t'[id,t]$ where
\verb$t'[id,t]$ is the same as \verb$t'$, but all \verb$v0$s are replaced
by \verb$t$, all \verb$v1$s are replaced by \verb$v0$s, all \verb$v2$s
are replaced by \verb$v1$s, and so on. The nicest way to express this
is to add a new sort of \emph{substitutions}:
\begin{verbatim}
Sub : Con → Con → Set\end{verbatim}
If \verb$Γ = ∙ ▹ A₁ ▹ A₂ ▹ ... ▹ Aₙ$, then a \verb$γ : Sub Δ Γ$ is a list of terms \verb$γ = (t₁ , t₂ , ... tₙ)$  where
\begin{verbatim}
t₁ : Tm Δ A₁
t₂ : Tm Δ A₂
...
tₙ : Tm Δ Aₙ\end{verbatim}
We call \verb$Sub Δ Γ$ a substitution from \verb$Γ$ to \verb$Δ$.
A \verb$Sub Δ Γ$ contains enough information to turn a term
defined in context \verb$Γ$ into a term defined in context
\verb$Δ$. This is witnessed by a new operator called
\emph{instantiation  of substitution}:
\begin{verbatim}
_[_] : Tm Γ A → Sub Δ Γ → Tm Δ A
\end{verbatim}
There is an identity substitution \verb$id : Sub Γ Γ$ which doesn't do anything, i.e.\
\verb$t [ id ] = t$ and with this we can express the equation for let expressions:
\begin{verbatim}
def t u = u [ id , t ]\end{verbatim}
Substitutions can be composed: \verb$γ ⊚ δ : Sub Θ Γ$ if \verb$γ : Sub Δ Γ$ and \verb$δ : Sub Θ Δ$.
The \verb$v0$ De Bruijn index is now called \verb$q : Tm (Γ ▹ A) A$ and
we have a substitution \verb$p : Sub (Γ ▹ A) Γ$ which forgets about the
last element in the context: \verb$p ⊚ (σ , t) = p$.
The rest of the variables can be defined: \verb$v1 = q [ p ]$,
\verb$v2 = q [ p ] [ p ]$, \verb$v3 = q [ p ] [ p ] [ p ]$, and so on.
\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Def where

open import Lib

module I where
  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty

  data Con  : Set where
    ∙       : Con
    _▹_     : Con → Ty → Con

  infixl 5 _▹_
  infixl 6 _⊚_
  infixl 5 _,o_
  infixl 6 _[_]

  postulate
    Sub       : Con → Con → Set
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm        : Con → Ty → Set
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
    iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    ite[]     : ∀{Γ Δ A t u v}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

    num       : ∀{Γ} → ℕ → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
    isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
    +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
    num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
    isZero[]  : ∀{Γ Δ t}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
    +[]       : ∀{Γ Δ u v}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])

  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]

variable
  Aᴵ Bᴵ : I.Ty
  Γᴵ Δᴵ Θᴵ : I.Con
  γᴵ δᴵ θᴵ σᴵ γaᴵ : I.Sub Δᴵ Γᴵ
  tᴵ uᴵ vᴵ : I.Tm Γᴵ Aᴵ
\end{code}
\begin{code}
record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
\end{code}
\begin{code}[hide]
  infixl 5 _▹_
  infixl 6 _⊚_
  infixl 5 _,o_
  infixl 6 _[_]
  
  field
\end{code}
A Def-model consists of the following three groups of components:
\begin{itemize}
\item The substitution calculus (this is also called simply typed category with families (sCwF \cite{DBLP:journals/corr/abs-1904-00827})):
\begin{code}
    Con       : Set i
    Sub       : Con → Con → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} →
                (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Ty        : Set k
    
    Tm        : Con → Ty → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →
                t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_       : Con → Ty → Con
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa
\end{code}
\item Booleans:
\begin{code}
    Bool      : Ty
    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
    iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
    true[]    : ∀{Γ Δ}{σ : Sub Δ Γ} → true [ σ ] ≡ true
    false[]   : ∀{Γ Δ}{σ : Sub Δ Γ} → false [ σ ] ≡ false
    ite[]     : ∀{Γ Δ A t u v}{γ : Sub Δ Γ} →
                (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])
\end{code}
\item Natural numbers:
\begin{code}
    Nat       : Ty
    num       : ∀{Γ} → ℕ → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
    isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
    +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
    num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
    isZero[]  : ∀{Γ Δ t}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
    +[]       : ∀{Γ Δ u v}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])
\end{code}
\end{itemize}
We define let (\verb$def$) as the following abbreviation.
\begin{code}
  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]
\end{code}
Variables are typed De Bruijn indices:
\begin{code}
  v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
  v0 = q
  v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]
\end{code}
Some equalities that hold in any model:
\begin{code}
  ▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
  ▹η' {Γ}{A} =
    p ,o q
      ≡⟨ sym {A = Sub (Γ ▹ A) (Γ ▹ A)}
           (cong {A = Sub (Γ ▹ A) Γ × Tm (Γ ▹ A) A} (λ w → π₁ w ,o π₂ w) (idr , [id])) ⟩
    p ⊚ id ,o q [ id ]
      ≡⟨ ▹η ⟩
    id
      ∎

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong (_⊚ δ) ▹β₁ , cong (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎
\end{code}
\begin{code}[hide]
  -- the recursor

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
    ⟦_⟧t      : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S       ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S       ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S        ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t     ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S      ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S    ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t    ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦true⟧    : ⟦ I.true {Γᴵ} ⟧t     ≡ true
    ⟦false⟧   : ⟦ I.false {Γᴵ} ⟧t    ≡ false
    ⟦ite⟧     : ⟦ I.ite tᴵ uᴵ vᴵ ⟧t  ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦num⟧     : ⟦ I.num {Γᴵ} n ⟧t    ≡ num n
    ⟦isZero⟧  : ⟦ I.isZero tᴵ ⟧t     ≡ isZero ⟦ tᴵ ⟧t
    ⟦+⟧       : ⟦ uᴵ I.+o vᴵ ⟧t      ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
    {-# REWRITE ⟦num⟧ ⟦isZero⟧ ⟦+⟧ #-}
\end{code}
The term \verb$let x:= false in if x then true else isZero (if x then (num 0) else (num 1))$ is equal to \verb$false$ in any model.
\begin{code}[hide]
module Example {i}{j}{k}{l}(M : Model {i}{j}{k}{l}) where
  open Model M
  pt : def false (ite q true (isZero (ite q (num 0) (num 1)))) ≡ false {∙}
  pt =
\end{code}
\begin{code}
    def false (ite q true (isZero (ite q (num 0) (num 1))))
      ≡⟨ refl {x = ite q true (isZero (ite q (num 0) (num 1))) [ id ,o false ]} ⟩
    ite q true (isZero (ite q (num 0) (num 1))) [ id ,o false ]
      ≡⟨ ite[] ⟩
    ite (q [ id ,o false ]) (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ])
      ≡⟨ cong {A = Tm ∙ Bool}
           (λ x → ite x (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ]))
           ▹β₂ ⟩
    ite false (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ])
      ≡⟨ iteβ₂ ⟩
    isZero (ite q (num 0) (num 1)) [ id ,o false ]
      ≡⟨ isZero[] ⟩
    isZero (ite q (num 0) (num 1) [ id ,o false ])
      ≡⟨ cong isZero ite[] ⟩
    isZero (ite (q [ id ,o false ]) (num 0 [ id ,o false ]) (num 1 [ id ,o false ]))
      ≡⟨ cong {A = Tm ∙ Bool}
           (λ x → isZero (ite x (num 0 [ id ,o false ]) (num 1 [ id ,o false ])))
           ▹β₂ ⟩
    isZero (ite false (num 0 [ id ,o false ]) (num 1 [ id ,o false ]))
      ≡⟨ cong isZero iteβ₂ ⟩
    isZero (num 1 [ id ,o false ])
      ≡⟨ cong isZero num[] ⟩
    isZero (num 1)
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎
\end{code}

\begin{exe}
Prove \verb$(def t u) [ γ ] ≡ def (t [ γ ]) (u [ γ ⊚ p ,o q ])$ for any \verb$t$, \verb$u$, \verb$γ$ in any model.
\end{exe}

\begin{code}[hide]
record DepModel {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixl 6 _⊚_
  infixl 5 _,o_
  infixl 6 _[_]

  field
    Con       : I.Con → Set i
    Sub       : Con Δᴵ → Con Γᴵ → I.Sub Δᴵ Γᴵ → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ γᴵ → Sub Θ Δ δᴵ → Sub Θ Γ (γᴵ I.⊚ δᴵ)
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ γᴵ}{δ : Sub Θ Δ δᴵ}{θ : Sub Ξ Θ θᴵ} →
                (Sub Ξ Γ ~) I.ass ((γ ⊚ δ) ⊚ θ) (γ ⊚ (δ ⊚ θ))
    id        : ∀{Γ} → Sub Γ Γ (I.id {Γᴵ})
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Sub Δ Γ ~) I.idl (id ⊚ γ) γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Sub Δ Γ ~) I.idr (γ ⊚ id) γ
    
    ∙         : Con I.∙
    ε         : ∀{Γ} → Sub Γ ∙ (I.ε {Γᴵ})
    ∙η        : ∀{Γ}{σ : Sub Γ ∙ σᴵ} → (Sub Γ ∙ ~) I.∙η σ ε

    Ty        : I.Ty → Set k

    Tm        : Con Γᴵ → Ty Aᴵ → I.Tm Γᴵ Aᴵ → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A tᴵ → Sub Δ Γ γᴵ → Tm Δ A (tᴵ I.[ γᴵ ])
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A tᴵ}{γ : Sub Δ Γ γᴵ}{δ : Sub Θ Δ δᴵ} →
                (Tm Θ A ~) I.[∘] (t [ γ ⊚ δ ]) (t [ γ ] [ δ ])
    [id]      : ∀{Γ A}{t : Tm Γ A tᴵ} → (Tm Γ A ~) I.[id] (t [ id ]) t
    _▹_       : Con Γᴵ → Ty Aᴵ → Con (Γᴵ I.▹ Aᴵ)
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ γᴵ → Tm Δ A tᴵ → Sub Δ (Γ ▹ A) (γᴵ I.,o tᴵ)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ (I.p {Γᴵ}{Aᴵ})
    q         : ∀{Γ A} → Tm (Γ ▹ A) A (I.q {Γᴵ}{Aᴵ})
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ γᴵ}{t : Tm Δ A tᴵ} → (Sub Δ Γ ~) I.▹β₁ (p ⊚ (γ ,o t)) γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ γᴵ}{t : Tm Δ A tᴵ} → (Tm Δ A ~) I.▹β₂ (q [ γ ,o t ]) t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A) γaᴵ} → (Sub Δ (Γ ▹ A) ~) I.▹η (p ⊚ γa ,o q [ γa ]) γa

    Bool      : Ty I.Bool
    true      : ∀{Γ} → Tm Γ Bool (I.true {Γᴵ})
    false     : ∀{Γ} → Tm Γ Bool (I.false {Γᴵ})
    ite       : ∀{Γ A} → Tm Γ Bool tᴵ → Tm Γ A uᴵ → Tm Γ A vᴵ → Tm Γ A (I.ite tᴵ uᴵ vᴵ)
    iteβ₁     : ∀{Γ}{A : Ty Aᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ} → (Tm Γ A ~) I.iteβ₁ (ite true u v) u
    iteβ₂     : ∀{Γ}{A : Ty Aᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ} → (Tm Γ A ~) I.iteβ₂ (ite false u v) v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Tm Δ Bool ~) I.true[] (true [ γ ]) true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Tm Δ Bool ~) I.false[] (false [ γ ]) false
    ite[]     : ∀{Γ Δ A}{t : Tm Γ Bool tᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ A ~) I.ite[] ((ite t u v) [ γ ]) (ite (t [ γ ]) (u [ γ ]) (v [ γ ]))
                
    Nat       : Ty I.Nat
    num       : ∀{Γ}(n : ℕ) → Tm Γ Nat (I.num {Γᴵ} n)
    isZero    : ∀{Γ} → Tm Γ Nat tᴵ → Tm Γ Bool (I.isZero tᴵ)
    _+o_      : ∀{Γ} → Tm Γ Nat uᴵ → Tm Γ Nat vᴵ → Tm Γ Nat (uᴵ I.+o vᴵ)
    isZeroβ₁  : ∀{Γ} → (Tm Γ Bool ~) I.isZeroβ₁ (isZero (num {Γᴵ}{Γ} 0)) true
    isZeroβ₂  : ∀{Γ n} → (Tm Γ Bool ~) I.isZeroβ₂ (isZero (num {Γᴵ}{Γ} (1 + n))) false
    +β        : ∀{Γ m n} → (Tm Γ Nat ~) I.+β (num {Γᴵ}{Γ} m +o num n) (num (m + n))
    num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ γᴵ} → (Tm Δ Nat ~) I.num[] (num n [ γ ]) (num n)
    isZero[]  : ∀{Γ Δ}{t : Tm Γ Nat tᴵ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ Bool ~) I.isZero[] (isZero t [ γ ]) (isZero (t [ γ ]))
    +[]       : ∀{Γ Δ}{u : Tm Γ Nat uᴵ}{v : Tm Γ Nat vᴵ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ Nat ~) I.+[] ((u +o v) [ γ ]) ((u [ γ ]) +o (v [ γ ]))

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : (γᴵ : I.Sub  Δᴵ  Γᴵ)  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C  γᴵ
    ⟦_⟧t      : (tᴵ : I.Tm   Γᴵ  Aᴵ)  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T  tᴵ
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S       ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S       ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S        ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t     ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S      ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S    ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t    ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦true⟧    : ⟦ I.true {Γᴵ} ⟧t     ≡ true
    ⟦false⟧   : ⟦ I.false {Γᴵ} ⟧t    ≡ false
    ⟦ite⟧     : ⟦ I.ite tᴵ uᴵ vᴵ ⟧t  ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦num⟧     : ⟦ I.num {Γᴵ} n ⟧t    ≡ num n
    ⟦isZero⟧  : ⟦ I.isZero tᴵ ⟧t     ≡ isZero ⟦ tᴵ ⟧t
    ⟦+⟧       : ⟦ uᴵ I.+o vᴵ ⟧t      ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
    {-# REWRITE ⟦num⟧ ⟦isZero⟧ ⟦+⟧ #-}
\end{code}

\subsection{Standard model}

In the standard model, types are sets as before, contexts are iterated
products of their constituent types, so they are also sets. Terms are
not simply elements of their types because they depend on a
context. This dependency is modelled using functions: a term of
type \verb$A$ in context \verb$Γ$ is a function \verb$Γ → A$. Substitutions are also functions.
\begin{code}
St : Model
St = record
  { Con       = Set
  ; Sub       = λ Δ Γ → Δ → Γ
  ; _⊚_       = λ γ δ θ* → γ (δ θ*)
  ; ass       = λ {_}{_}{_}{_}{γ}{δ}{θ} → refl {x = λ ξ* → γ (δ (θ ξ*))}
  ; id        = λ γ* → γ*
  ; idl       = λ {_}{_}{γ} → refl {x = γ}
  ; idr       = λ {_}{_}{γ} → refl {x = γ}
  
  ; ∙         = Lift ⊤
  ; ε         = _
  ; ∙η        = λ {_}{σ} → refl {x = σ}
  
  ; Ty        = Set
  
  ; Tm        = λ Γ A → Γ → A
  ; _[_]      = λ a γ δ* → a (γ δ*) 
  ; [∘]       = λ {_}{_}{_}{_}{a}{γ}{δ} → refl {x = λ θ* → a (γ (δ θ*))}
  ; [id]      = λ {_}{_}{a} → refl {x = a}
  ; _▹_       = _×_
  ; _,o_      = λ γ t δ* → γ δ* , t δ*
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = λ {_}{_}{_}{γ} → refl {x = γ}
  ; ▹β₂       = λ {_}{_}{_}{_}{a} → refl {x = a}
  ; ▹η        = λ {_}{_}{_}{γa} → refl {x = γa}
  
  ; Bool      = 𝟚
  ; true      = λ _ → tt
  ; false     = λ _ → ff
  ; ite       = λ t u v γ* → if t γ* then u γ* else v γ*
  ; iteβ₁     = λ {_}{_}{u}{v} → refl {x = u}
  ; iteβ₂     = λ {_}{_}{u}{v} → refl {x = v}
  ; true[]    = λ {_}{Δ} → refl {x = λ (_ : Δ) → tt}
  ; false[]   = λ {_}{Δ} → refl {x = λ (_ : Δ) → ff}
  ; ite[]     = λ {_}{_}{_}{t}{u}{v}{γ} →
                refl {x = λ δ* → if t (γ δ*) then u (γ δ*) else v (γ δ*)}
  
  ; Nat       = ℕ
  ; num       = λ n γ* → n
  ; isZero    = λ t γ* → rec tt (λ _ → ff) (t γ*)
  ; _+o_      = λ m n γ* → m γ* + n γ*
  ; isZeroβ₁  = λ {Γ} → refl {x = λ (_ : Γ) → tt}
  ; isZeroβ₂  = λ {Γ} → refl {x = λ (_ : Γ) → ff}
  ; +β        = λ {Γ}{m}{n} → refl {x = λ (_ : Γ) → m + n}
  ; num[]     = λ {_}{Δ}{n}{_} → refl {x = λ (_ : Δ) → n}
  ; isZero[]  = λ {_}{_}{t}{γ} → refl {x = λ δ* → rec tt (λ _ → ff) (t (γ δ*))}
  ; +[]       = λ {_}{_}{u}{v}{γ} → refl {x = λ δ* → u (γ δ*) + v (γ δ*)}
  }
module St = Model St
\end{code}
A term in the empty context reproduces the interpretation of terms in the standard model of NatBool, e.g.\ \verb$Tm ∙ Nat = ⊤ → ℕ$ which is essentially the same as \verb$ℕ$.
