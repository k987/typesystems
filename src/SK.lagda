\chapter{SK combinator calculus}

Moses Schönfinkel's combinator calculus \cite{Schönfinkel1924} was the
earliest description of universal computation, before Church's lambda
calculus \cite{Church1936AnUP} and Turing machines
\cite{https://doi.org/10.1112/plms/s2-42.1.230}. It avoids the usage of variables which makes its definition much simpler, compare it with the substitution calculus of Section \ref{sec:def} corresponding to lambda calculus. However writing (and reading!) programs in SK is much harder than using lambda calculus. Here we present the simply typed variant of combinator calculus with one base type \verb$ι$.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module SK where

open import Lib
open import Agda.Primitive using (_⊔_)

module I where
  infixr 5 _⇒_
  infixl 5 _$_

  data Ty : Set where
    ι   : Ty
    _⇒_ : Ty → Ty → Ty

  postulate
    Tm  : Ty → Set
    
  variable
    A B C D : Ty
    t u v : Tm A

  postulate
    _$_ : Tm (A ⇒ B) → Tm A → Tm B
    K   : Tm (A ⇒ B ⇒ A)
    S   : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    Kβ  : K $ t $ u ≡ t
    Sβ  : S $ t $ u $ v ≡ t $ v $ (u $ v)
\end{code}
An SK model consists of two sorts (types and term), two type operations, three term operations and two term equations.
\begin{code}
record Model {i j} : Set (lsuc (i ⊔ j)) where
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Ty  : Set i
    ι   : Ty
    _⇒_ : Ty → Ty → Ty
    Tm  : Ty → Set j
    _$_ : ∀{A B}    → Tm (A ⇒ B) → Tm A → Tm B
    K   : ∀{A B}    → Tm (A ⇒ B ⇒ A)
    S   : ∀{A B C}  → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    Kβ  : ∀{A B}{t : Tm A}{u : Tm B} → K $ t $ u ≡ t
    Sβ  : ∀{A B C}{t : Tm (A ⇒ B ⇒ C)}{u : Tm (A ⇒ B)}{v : Tm A} →
          S $ t $ u $ v ≡ t $ v $ (u $ v)
\end{code}
\begin{code}[hide]
  ⟦_⟧T    : I.Ty → Ty
  ⟦ I.ι        ⟧T = ι
  ⟦ Aᴵ I.⇒ Bᴵ  ⟧T = ⟦ Aᴵ ⟧T ⇒ ⟦ Bᴵ ⟧T
  
  postulate
    ⟦_⟧t  : I.Tm I.A → Tm ⟦ I.A ⟧T
    ⟦$⟧t  : ⟦ I.t I.$ I.u ⟧t ≡ ⟦ I.t ⟧t $ ⟦ I.u ⟧t
    ⟦K⟧t  : ⟦ I.K {I.A} {I.B} ⟧t ≡ K {⟦ I.A ⟧T} {⟦ I.B ⟧T}
    ⟦S⟧t  : ⟦ I.S {I.A} {I.B} {I.C} ⟧t ≡ S {⟦ I.A ⟧T} {⟦ I.B ⟧T}{⟦ I.C ⟧T}
    {-# REWRITE ⟦$⟧t ⟦K⟧t ⟦S⟧t #-}

record DepModel {i j : Level} : Set (lsuc (i ⊔ j)) where
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Ty  : I.Ty → Set i
    ι   : Ty I.ι
    _⇒_ : Ty I.A → Ty I.B → Ty (I.A I.⇒ I.B)
    Tm  : Ty I.A → I.Tm I.A → Set j
    _$_ : {A : Ty I.A}{B : Ty I.B} →
          Tm (A ⇒ B) I.t → Tm A I.u → Tm B (I.t I.$ I.u)
    K   : {A : Ty I.A}{B : Ty I.B} → Tm (A ⇒ B ⇒ A) I.K
    S   : {A : Ty I.A}{B : Ty I.B}{C : Ty I.C} →
          Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) I.S
    Kβ  : {A : Ty I.A}{B : Ty I.B}{t : Tm A I.t}{u : Tm B I.u} → ((Tm A) ~) I.Kβ (K $ t $ u) t
    Sβ  : {A : Ty I.A}{B : Ty I.B}{C : Ty I.C}{t : Tm (A ⇒ B ⇒ C) I.t}{u : Tm (A ⇒ B) I.u}{v : Tm A I.v} →
          ((Tm C) ~) I.Sβ (S $ t $ u $ v) ((t $ v) $ (u $ v))

  ⟦_⟧T : (Aᴵ : I.Ty) → Ty Aᴵ
  ⟦ I.ι ⟧T = ι
  ⟦ Aᴵ I.⇒ Bᴵ ⟧T = ⟦ Aᴵ ⟧T ⇒ ⟦ Bᴵ ⟧T
  
  postulate
    ⟦_⟧t : (tᴵ : I.Tm I.A) → Tm ⟦ I.A ⟧T tᴵ
    ⟦$⟧t : ⟦ I.t I.$ I.u ⟧t ≡ ⟦ I.t ⟧t $ ⟦ I.u ⟧t
    ⟦K⟧t : ⟦ I.K {I.A} {I.B} ⟧t ≡ K {I.A} {I.B} {⟦ I.A ⟧T} {⟦ I.B ⟧T}
    ⟦S⟧t : ⟦ I.S {I.A} {I.B} {I.C} ⟧t ≡ S {I.A} {I.B} {I.C} {⟦ I.A ⟧T} {⟦ I.B ⟧T}{⟦ I.C ⟧T}
    {-# REWRITE ⟦$⟧t ⟦K⟧t ⟦S⟧t #-}
\end{code}
The \verb$I$ combinator can be derived (here we call it \verb$I'$ to distinguish from the initial model).
\begin{code}[hide]
module _ where
  open I
\end{code}
\begin{code}
  I' : Tm (A ⇒ A)
  I' {A} = S $ K $ K {A}{ι}

  Iβ : I' $ u ≡ u
  Iβ {A}{u} =
    I' $ u
                                  ≡⟨ refl {x = I' $ u} ⟩
    S $ K $ K $ u
                                  ≡⟨ Sβ ⟩
    K $ u $ (K $ u)
                                  ≡⟨ Kβ ⟩
    u
                                  ∎
\end{code}
Another famous combinator is \verb$B$ (which we call \verb$B'$).
\begin{code}
  B' : Tm ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  B' = S $ (K $ S) $ K

  Bβ : B' $ t $ u $ v ≡ t $ (u $ v)
  Bβ {t = t}{u = u}{v = v} =
    B' $ t $ u $ v
                                  ≡⟨ refl {x = B' $ t $ u $ v} ⟩
    S $ (K $ S) $ K $ t $ u $ v
                                  ≡⟨ cong {A = Tm _} (λ x → x $ u $ v) Sβ ⟩
    K $ S $ t $ (K $ t) $ u $ v
                                  ≡⟨ cong {A = Tm _} (λ x → x $ (K $ t) $ u $ v) Kβ ⟩
    S $ (K $ t) $ u $ v
                                  ≡⟨ Sβ ⟩
    K $ t $ v $ (u $ v)
                                  ≡⟨ cong {A = Tm _} (λ x → x $ (u $ v)) Kβ ⟩
    t $ (u $ v)
                                  ∎
\end{code}
The standard model.
\begin{code}
St : Model
St = record
  { Ty  = Set
  ; Tm  = λ A → A
  ; ι   = Lift ⊤
  ; _⇒_ = λ A B → A → B
  ; _$_ = λ f x → f x
  ; K   = λ x y → x
  ; S   = λ x y z → x z (y z)
  ; Kβ  = λ where {t = t} → refl {x = t}
  ; Sβ  = λ where {t = t} {u = u} {v = v} → refl {x = t v (u v)}
  }
module St = Model St
\end{code}
Normal forms are given as an inductive predicate over terms (as opposed to Subsection \ref{sec:natbool-norm} where they are defined as sets together with a functions into terms -- the two representations are related by the family--map correspondance, see \cite[p.~221]{gat}).

A term is in normal form if it is a partial application of either \verb$K$ or \verb$S$. A full application of \verb$K$ has two arguments, a full application of \verb$S$ has three arguments (and they are equal to some other term using the equations \verb$Kβ$ and \verb$Sβ$).
\begin{code}
module _ where
  open I
  data Nf : (A : Ty) → Tm A → Set where
    K₀ : Nf (A ⇒ B ⇒ A) K
    K₁ : Nf A t → Nf (B ⇒ A) (K $ t)
    S₀ : Nf ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) S
    S₁ : Nf (A ⇒ B ⇒ C) t → Nf ((A ⇒ B) ⇒ A ⇒ C) (S $ t)
    S₂ : Nf (A ⇒ B ⇒ C) t → Nf (A ⇒ B) u → Nf (A ⇒ C) (S $ t $ u)
\end{code}
The normalisation dependent model: for each type, we have a predicate
over terms of that type together with a function which says that if
the predicate holds for a term, then it is in normal form. For each term we have a witness of the predicate.

For \verb$ι$, the predicate is always false. For a function \verb$tᴵ$ the predicate says that if the predicate holds for an input \verb$uᴵ$ then it holds for the output \verb@tᴵ I.$ uᴵ@, and \verb$tᴵ$ has to be in normal form.
\begin{code}
Norm : DepModel
Norm = record
  { Ty   =  λ Aᴵ → Σ (I.Tm Aᴵ → Set) λ P → {tᴵ : I.Tm Aᴵ} → P tᴵ → Nf Aᴵ tᴵ
  ; Tm   =  π₁
  ; ι    =  (λ _ → Lift ⊥) , λ where ()
  ; _⇒_  =  λ {Aᴵ}{Bᴵ} A B →
            (λ tᴵ → ({uᴵ : I.Tm Aᴵ} → π₁ A uᴵ → π₁ B (tᴵ I.$ uᴵ)) × Nf (Aᴵ I.⇒ Bᴵ) tᴵ) , π₂
  ; _$_  =  λ t u → π₁ t u
  ; K    =  λ {Aᴵ}{Bᴵ}{A}{B} →
            (λ t → (λ u → coe (π₁ A) (sym {A = I.Tm Aᴵ} I.Kβ) t) , K₁ (π₂ A t)) , K₀
  ; S    =  λ {_}{_}{_}{_}{_}{C} →
            (λ t →  (λ u →  (λ v → coe (π₁ C) (sym {A = I.Tm _} I.Sβ) (π₁ (π₁ t v) (π₁ u v))
                            ) , S₂ (π₂ t) (π₂ u)
                    ) , S₁ (π₂ t)
            ) , S₀
  ; Kβ   =  λ {Aᴵ}{Bᴵ}{tᴵ}{uᴵ}{A}{B}{t}{u} → ~coe I.Kβ {B = π₁ A}{t}
  ; Sβ   =  λ {Aᴵ}{Bᴵ}{Cᴵ}{tᴵ}{uᴵ}{vᴵ}{A}{B}{C}{t}{u}{v} →
            ~coe I.Sβ {B = π₁ C}{π₁ (π₁ t v) (π₁ u v)}
  }
\end{code}
Note that \verb$Norm$ makes use of all the five ways to form normal forms and both equations in our language. To normalise a term of type \verb$Aᴵ$, we first interpret the type in the \verb$Norm$ dependent model. This results in a pair the second component of which is a function which produces a normal form from any term for which the predicate holds. Interpreting the term in \verb$Norm$ gives us a witness of the predicate. We put together these as follows.
\begin{code}[hide]
module Norm = DepModel Norm
\end{code}
\begin{code}
norm : ∀{Aᴵ}(tᴵ : I.Tm Aᴵ) → Nf Aᴵ tᴵ
norm {Aᴵ} tᴵ = π₂ Norm.⟦ Aᴵ ⟧T Norm.⟦ tᴵ ⟧t
\end{code}
Because normal forms are defined as a predicate, the function \verb$norm$
includes completeness. Stability is proven by induction on normal forms.
\begin{code}
stab : ∀{Aᴵ tᴵ}(n : Nf Aᴵ tᴵ) → norm tᴵ ≡ n
stab K₀         = refl {x = K₀}
stab (K₁ n)     = cong K₁ (stab n)
stab S₀         = refl {x = S₀}
stab (S₁ n)     = cong S₁ (stab n)
stab (S₂ n n')  = cong {A = Nf _ _ × Nf _ _} (λ w → S₂ (π₁ w) (π₂ w)) (stab n , stab n') 
\end{code}
\begin{code}[hide]
-- Decidability of equality:
-- dec : {uᴵ vᴵ : I.Tm I.A}(nu : Nf I.A uᴵ)(nv : Nf I.A vᴵ) → Lift (Σp (uᴵ ≡ vᴵ) λ e → (Nf I.A ~) e nu nv) ⊎ Lift (¬ (Σp (uᴵ ≡ vᴵ) λ e → (Nf I.A ~) e nu nv))
-- dec : {uᴵ : I.Tm I.A}(n n' : Nf I.A uᴵ) → Lift (n ≡ n') ⊎ Lift (¬ (n ≡ n'))
-- dec n n' = {!n n'!}
-- dec : (a b : Σ I.Ty λ Aᴵ → Σ (I.Tm Aᴵ) (Nf Aᴵ)) → (Lift (a ≡ b)) ⊎ Lift (¬ (a ≡ b))
{-
-- we first have to prove disjointness of Nf-constructors, this should be almost automatic
contra : {u : I.Tm I.D}(n : Nf I.D u) →
  (eTy : (I.A I.⇒ I.B I.⇒ I.A) ≡ (I.C I.⇒ I.D))
  (eTm : (I.Tm ~) eTy I.K (I.K I.$ u))
  (eNf : _~ {A = Σ I.Ty I.Tm} (λ w → Nf (π₁ w) (π₂ w)) (eTy , eTm) K₀ (K₁ n)) →
  ⊥
contra = {!!}
-- we should be able to define a function which maps K₀ to ⊤ and K₁ n to ⊥ and use that to derive this contradiction without relying on I being initial
-}
\end{code}
