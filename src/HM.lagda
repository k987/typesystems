\chapter{Hindley-Milner polymorphism}\label{ch:hm}

Type inference. Subtyping?

\begin{verbatim}
MTy : Set*
Ty  : Set
Tm  : Ty → Set*
_⇒_ : MTy → MTy → MTy
∀   : (MTy →* Ty) → Ty
i   : MTy -> Ty
lam : (Tm (i A) →* Tm (i B)) ≅ Tm (i (A ⇒ B)) : app
Lam : ((A : MTy) →* Tm (B * A)) ≅ Tm (∀ B) : App
\end{verbatim}
