\begin{code}[hide]
{-# OPTIONS --prop #-}

module MLTT.Standard where

open import Lib hiding (_∘_; J)
                renaming (Σ to Σₘ; _,_ to _⸴_; refl to reflₘ)
\end{code}
\begin{code}
infixl 5 _▻_
infixl 6 _+_
infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 7 _↑_
infixl 8 _[_]T
infixl 8 _[_]t
infixl 5 _$_

Con : (i : Level) → Set (lsuc i)
Con i = Set i

Ty : {i : Level}(j : Level) → Con i → Set (i ⊔ lsuc j)
Ty j Γ = Γ → Set j

Sub : {i j : Level} → Con i → Con j → Set (i ⊔ j)
Sub Γ Δ = Γ → Δ

Tm : {i j : Level}(Γ : Con i) → Ty j Γ → Set (i ⊔ j)
Tm Γ A = (γ : Γ) → A γ

id : ∀ {i}{Γ : Con i} → Sub Γ Γ
id γ = γ

_∘_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{Θ : Con k} →
  Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
(σ ∘ δ) γ = σ (δ γ)

ass : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{Λ : Con l}
  {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = reflₘ

idl : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = reflₘ

idr : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = reflₘ

_[_]T : ∀ {i j k}{Γ : Con i}{Δ : Con j} → Ty k Δ → Sub Γ Δ → Ty k Γ
(A [ σ ]T) γ = A (σ γ)

_[_]t : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ} →
  Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
(t [ σ ]t) γ = t (σ γ)

[id]T : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → A [ id ]T ≡ A
[id]T = reflₘ

[∘]T : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
[∘]T = reflₘ

[id]t : ∀ {i j}{Γ : Con i}{A : Ty j Γ}{t : Tm Γ A} → t [ id ]t ≡ t
[id]t = reflₘ

[∘]t : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
  {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t
[∘]t = reflₘ

∙ : Con lzero
∙ = 𝟙↑

ε : ∀ {i}{Γ : Con i} → Sub Γ ∙
ε γ = *↑

∙η : ∀ {i}{Γ : Con i}{σ : Sub Γ ∙} → σ ≡ ε
∙η = reflₘ

_▻_ : ∀ {i j}(Γ : Con i) → Ty j Γ → Con (i ⊔ j)
Γ ▻ A = Σₘ Γ A

_,_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
  (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
(σ , t) γ = σ γ ⸴ t γ

p : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → Sub (Γ ▻ A) Γ
p (γ ⸴ a) = γ

q : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → Tm (Γ ▻ A) (A [ p ]T)
q (γ ⸴ a) = a

▻β₁ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
  {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p {Γ = Δ}{A} ∘ (σ , t) ≡ σ
▻β₁ = reflₘ

▻β₂ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
  {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → q {Γ = Δ}{A} [ σ , t ]t ≡ t
▻β₂ = reflₘ

▻η : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
  {σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ]t ≡ σ
▻η = reflₘ

,∘ : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
  {σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
  (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]t
,∘ = reflₘ

_↑_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}(σ : Sub Γ Δ)(A : Ty k Δ) →
  Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
σ ↑ A = σ ∘ p , q

↑∘ : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Γ ( A [ σ ]T [ δ ]T)} →
  (σ ↑ A) ∘ (δ , t) ≡ σ ∘ δ , t
↑∘ = reflₘ

Π : ∀ {i j k}{Γ : Con i}(A : Ty j Γ) → Ty k (Γ ▻ A) → Ty (j ⊔ k) Γ
(Π A B) γ = (a : A γ) → B (γ ⸴ a)

lam : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
  Tm (Γ ▻ A) B → Tm Γ (Π A B)
(lam t) γ a = t (γ ⸴ a)

app : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
  Tm Γ (Π A B) → Tm (Γ ▻ A) B
(app t) (γ ⸴ a) = t γ a

Πβ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  {t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
Πβ = reflₘ

Πη : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  {t : Tm Γ (Π A B)} → lam (app t) ≡ t
Πη = reflₘ

Π[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {σ : Sub Γ Δ} → (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ A ]T)
Π[] = reflₘ

lam[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} → (lam t) [ σ ]t ≡ lam (t [ σ ↑ A ]t)
lam[] = reflₘ

_⇒_ : ∀ {i j k}{Γ : Con i} → Ty j Γ → Ty k Γ → Ty (j ⊔ k) Γ
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
  Tm Γ (Π A B) → (u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = (app t) [ id , u ]t

⇒[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ A [ σ ]T ⇒ B [ σ ]T
⇒[] = reflₘ

app[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm Δ (Π A B)}{σ : Sub Γ Δ} → app (t [ σ ]t) ≡ (app t) [ σ ↑ A ]t
app[] = reflₘ

$[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm Δ (Π A B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
  (t $ u) [ σ ]t ≡ t [ σ ]t $ u [ σ ]t
$[] = reflₘ

Σ : ∀ {i j k}{Γ : Con i}(A : Ty j Γ) → Ty k (Γ ▻ A) → Ty (j ⊔ k) Γ
(Σ A B) γ = Σₘ (A γ) (λ a → B (γ ⸴ a))

⟨_,_⟩ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  (u : Tm Γ A) → Tm Γ (B [ id , u ]T) → Tm Γ (Σ A B)
⟨ u , v ⟩ γ = u γ ⸴ v γ

proj₁ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
  Tm Γ (Σ A B) → Tm Γ A
(proj₁ t) γ = π₁ (t γ)

proj₂ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , proj₁ t ]T)
(proj₂ t) γ = π₂ (t γ)

Σβ₁ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  {u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} → proj₁ {B = B} ⟨ u , v ⟩ ≡ u
Σβ₁ = reflₘ

Σβ₂ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  {u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} → proj₂ {B = B} ⟨ u , v ⟩ ≡ v
Σβ₂ = reflₘ

Ση : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
  {t : Tm Γ (Σ A B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
Ση = reflₘ

Σ[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {σ : Sub Γ Δ} → (Σ A B) [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ↑ A ]T)
Σ[] = reflₘ

⟨,⟩[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {u : Tm Δ A}{v : Tm Δ (B [ id , u ]T)}{σ : Sub Γ Δ} →
  (⟨_,_⟩ {B = B} u v) [ σ ]t ≡ ⟨ u [ σ ]t , v [ σ ]t ⟩
⟨,⟩[] = reflₘ

proj₁[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₁ t) [ σ ]t ≡ proj₁ (t [ σ ]t)
proj₁[] = reflₘ

proj₂[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₂ t) [ σ ]t ≡ proj₂ (t [ σ ]t)
proj₂[] = reflₘ

⊤ : ∀ {i}{Γ : Con i} → Ty lzero Γ
⊤ γ = 𝟙↑

tt : ∀ {i}{Γ : Con i} → Tm Γ ⊤
tt γ = *↑

⊤η : ∀ {i}{Γ : Con i}{t : Tm Γ ⊤} → t ≡ tt
⊤η = reflₘ

⊤[] : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
⊤[] = reflₘ

tt[] : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → tt [ σ ]t ≡ tt
tt[] = reflₘ

_+_ : ∀ {i j k}{Γ : Con i} → Ty j Γ → Ty k Γ → Ty (j ⊔ k) Γ
(A + B) γ = A γ ⊎ B γ

inj₁ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ} → Tm Γ A → Tm Γ (A + B)
(inj₁ t) γ = ι₁ (t γ)

inj₂ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ} → Tm Γ B → Tm Γ (A + B)
(inj₂ t) γ = ι₂ (t γ)

ind+ : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}
  (P : Ty l (Γ ▻ A + B)) →
  Tm (Γ ▻ A) (P [ p , inj₁ q ]T) → Tm (Γ ▻ B) (P [ p , inj₂ q ]T) →
  (t : Tm Γ (A + B)) → Tm Γ (P [ id , t ]T)
(ind+ P u v t) γ =
  ind⊎ (λ u → P (γ ⸴ u)) (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b)) (t γ)

+β₁ : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}{P : Ty l (Γ ▻ A + B)}
  {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}{v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}
  {t : Tm Γ A} → ind+ P u v (inj₁ t) ≡ u [ id , t ]t
+β₁ = reflₘ

+β₂ : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}{P : Ty l (Γ ▻ A + B)}
  {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}{v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}
  {t : Tm Γ B} → ind+ P u v (inj₂ t) ≡ v [ id , t ]t
+β₂ = reflₘ

+[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {σ : Sub Γ Δ} → (A + B) [ σ ]T ≡ A [ σ ]T + B [ σ ]T
+[] = reflₘ

inj₁[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {t : Tm Δ A}{σ : Sub Γ Δ} → (inj₁ {B = B} t) [ σ ]t ≡ inj₁ (t [ σ ]t)
inj₁[] = reflₘ

inj₂[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {t : Tm Δ B}{σ : Sub Γ Δ} → (inj₂ {A = A} t) [ σ ]t ≡ inj₂ (t [ σ ]t)
inj₂[] = reflₘ

ind+[] : ∀ {i j k l m}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {P : Ty m (Δ ▻ A + B)}
  {u : Tm (Δ ▻ A) (P [ p , inj₁ q ]T)}
  {v : Tm (Δ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Δ (A + B)}{σ : Sub Γ Δ} →
  (ind+ P u v t) [ σ ]t ≡
    ind+ (P [ σ ↑ (A + B) ]T) (u [ σ ↑ A ]t) (v [ σ ↑ B ]t) (t [ σ ]t)
ind+[] = reflₘ

case : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}{C : Ty l Γ} →
  Tm Γ (A + B) → Tm (Γ ▻ A) (C [ p ]T) → Tm (Γ ▻ B) (C [ p ]T) → Tm Γ C
case {C = C} t u v = ind+ (C [ p ]T) u v t

⊥ : ∀ {i}{Γ : Con i} → Ty lzero Γ
⊥ γ = 𝟘↑

absurd : ∀ {i j}{Γ : Con i}(A : Ty j Γ) → Tm Γ ⊥ → Tm Γ A
(absurd A t) γ = ⟦ ↓[ t γ ]↓ ⟧𝟘

⊥[] : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → ⊥ [ σ ]T ≡ ⊥
⊥[] = reflₘ

absurd[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{t : Tm Δ ⊥}
  {σ : Sub Γ Δ} → (absurd A t) [ σ ]t ≡ absurd (A [ σ ]T) (t [ σ ]t)
absurd[] = reflₘ

U : ∀ {i}{Γ : Con i}(j : Level) → Ty (lsuc j) Γ
U j γ = Set j

c : ∀ {i j}{Γ : Con i} → Ty j Γ → Tm Γ (U j)
c A γ = A γ

El : ∀ {i j}{Γ : Con i} → Tm Γ (U j) → Ty j Γ
El a γ = a γ

Uβ : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → El (c A) ≡ A
Uβ = reflₘ

Uη : ∀ {i j}{Γ : Con i}{a : Tm Γ (U j)} → c (El a) ≡ a
Uη = reflₘ

U[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → (U k) [ σ ]T ≡ U k
U[] = reflₘ

c[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{σ : Sub Γ Δ} →
  (c A) [ σ ]t ≡ c (A [ σ ]T)
c[] = reflₘ

El[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{a : Tm Δ (U k)}{σ : Sub Γ Δ} →
  (El a) [ σ ]T ≡ El (a [ σ ]t)
El[] = reflₘ

Id : ∀ {i j}{Γ : Con i}(A : Ty j Γ) → Tm Γ A → Tm Γ A → Ty j Γ
(Id A u v) γ = u γ =̂ v γ

refl : ∀ {i j}{Γ : Con i}{A : Ty j Γ}(u : Tm Γ A) → Tm Γ (Id A u u)
(refl u) γ = reflₘ

J : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{u : Tm Γ A}
  (P : Ty k (Γ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)) →
  Tm Γ (P [ id , u , refl u ]T) →
  {v : Tm Γ A}(e : Tm Γ (Id A u v)) → Tm Γ (P [ id , v , e ]T)
(J P w e) γ = Ĵ (λ {y} p → P (γ ⸴ y ⸴ p)) (w γ) (e γ)

Jβ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{u : Tm Γ A}
  {P : Ty k (Γ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)}
  {w : Tm Γ (P [ id , u , refl u ]T)} → J P w (refl u) ≡ w
Jβ = reflₘ

Id[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{u : Tm Δ A}{v : Tm Δ A}
  {σ : Sub Γ Δ} → (Id A u v) [ σ ]T ≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
Id[] = reflₘ

refl[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{u : Tm Δ A}
  {σ : Sub Γ Δ} → (refl u) [ σ ]t ≡ refl (u [ σ ]t)
refl[] = reflₘ

J[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{u : Tm Δ A}
  {P : Ty l (Δ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)}
  {w : Tm Δ (P [ id , u , refl u ]T)}{v : Tm Δ A}{e : Tm Δ (Id A u v)}
  {σ : Sub Γ Δ} →
  (J P w e) [ σ ]t ≡ J (P [ σ ↑ A ↑ _ ]T) (w [ σ ]t) (e [ σ ]t)
J[] = reflₘ
\end{code}
