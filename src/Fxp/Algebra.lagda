\chapter{First order function space}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Bidirectional type checking, introduction and elmination operators, computation and uniqueness rules.
\end{tcolorbox}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fxp.Algebra where

open import Lib hiding (_∘_ ; _,_)

module I where
  data STy : Set where
    Nat : STy
    Bool : STy

  data Ty : Set where
    sty : STy → Ty
    _⇒_ : STy → Ty → Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_

  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ sty A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ sty A) B
    
    zero : ∀ {Γ} → Tm Γ (sty Nat)
    suc : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Nat)
    isZero : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Bool)
    add : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Nat) → Tm Γ (sty Nat)

    true : ∀ {Γ} → Tm Γ (sty Bool)
    false : ∀ {Γ} → Tm Γ (sty Bool)
    ite : ∀ {Γ A} → Tm Γ (sty Bool) → Tm Γ A → Tm Γ A → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ sty A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
      
    isZeroβ₁ : ∀ {Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ} {n : Tm Γ (sty Nat)} → isZero (suc n) ≡ false
    addβ₁ : ∀ {Γ} {n : Tm Γ (sty Nat)} → add zero n ≡ n
    addβ₂ : ∀ {Γ} {m n : Tm Γ (sty Nat)} → add (suc m) n ≡ suc (add m n)

    iteβ₁ : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ sty A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
            
    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ Δ} {n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    add[] : ∀ {Γ Δ} {m n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (add m n) [ σ ] ≡ add (m [ σ ]) (n [ σ ])

    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[] : ∀ {Γ Δ A} {b : Tm Δ (sty Bool)}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ (sty A) → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE isZeroβ₁ isZeroβ₂ addβ₁ addβ₂ #-}
  {-# REWRITE iteβ₁ iteβ₂ #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE zero[] suc[] isZero[] add[] #-}
  {-# REWRITE true[] false[] ite[] #-}
\end{code}
\begin{code}
record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_

  field
    Con : Set i
    STy : Set j
    Ty : Set j
    Sub : Con → Con → Set k
    Tm : Con → Ty → Set l

    ∙ : Con
    _▹_ : Con → Ty → Con

    Nat : STy
    Bool : STy
    sty : STy → Ty
    _⇒_ : STy → Ty → Ty

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ sty A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ sty A) B
    
    zero : ∀ {Γ} → Tm Γ (sty Nat)
    suc : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Nat)
    isZero : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Bool)
    add : ∀ {Γ} → Tm Γ (sty Nat) → Tm Γ (sty Nat) → Tm Γ (sty Nat)

    true : ∀ {Γ} → Tm Γ (sty Bool)
    false : ∀ {Γ} → Tm Γ (sty Bool)
    ite : ∀ {Γ A} → Tm Γ (sty Bool) → Tm Γ A → Tm Γ A → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ sty A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
      
    isZeroβ₁ : ∀ {Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ} {n : Tm Γ (sty Nat)} → isZero (suc n) ≡ false
    addβ₁ : ∀ {Γ} {n : Tm Γ (sty Nat)} → add zero n ≡ n
    addβ₂ : ∀ {Γ} {m n : Tm Γ (sty Nat)} → add (suc m) n ≡ suc (add m n)

    iteβ₁ : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ sty A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
            
    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ Δ} {n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    add[] : ∀ {Γ Δ} {m n : Tm Δ (sty Nat)}{σ : Sub Γ Δ} →
      (add m n) [ σ ] ≡ add (m [ σ ]) (n [ σ ])

    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[] : ∀ {Γ Δ A} {b : Tm Δ (sty Bool)}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ (sty A) → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  def⇒ : ∀ {Γ A B} {t : Tm Γ (sty A)}{u : Tm (Γ ▹ sty A) B} →
    def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ (sty A)}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧ST : I.STy → STy
  ⟦ I.Nat ⟧ST = Nat
  ⟦ I.Bool ⟧ST = Bool

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.sty A ⟧T = sty ⟦ A ⟧ST
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧ST ⇒ ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ I.sty A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦add⟧ : ∀ {Γ} {m n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.add m n ⟧t ≡ add ⟦ m ⟧t ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦add⟧ #-}
               
    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ (I.sty I.Bool)}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ (I.sty A)} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    STy : I.STy → Set j
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    Nat : STy I.Nat
    Bool : STy I.Bool
    sty : ∀ {A'} → STy A' → Ty (I.sty A')
    _⇒_ : ∀ {A' B'} → STy A' → Ty B' → Ty (A' I.⇒ B')

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
      
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : STy A'}{B : Ty B'} →
      Tm (Γ ▹ sty A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : STy A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ sty A) B (I.app t')

    zero : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ (sty Nat) I.zero
    suc : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ (sty Nat) n' → Tm Γ (sty Nat) (I.suc n')
    isZero : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ (sty Nat) n' → Tm Γ (sty Bool) (I.isZero n')
    add : ∀ {Γ' m' n'} {Γ : Con Γ'} →
      Tm Γ (sty Nat) m' → Tm Γ (sty Nat) n' → Tm Γ (sty Nat) (I.add m' n')

    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ (sty Bool) I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ (sty Bool) I.false
    ite : ∀ {Γ' A' b' u' v'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ (sty Bool) b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : STy A'}{B : Ty B'}
      {t : Tm (Γ ▹ sty A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : STy A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : STy A'}{B : Ty B'}
      {t : Tm (Δ ▹ sty A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
    
    zero[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      zero [ σ ] ≡ zero
    suc[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ (sty Nat) n'}{σ : Sub Γ Δ σ'} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ (sty Nat) n'}{σ : Sub Γ Δ σ'} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    add[] : ∀ {Γ' Δ' m' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {m : Tm Δ (sty Nat) m'}{n : Tm Δ (sty Nat) n'}{σ : Sub Γ Δ σ'} →
      (add m n) [ σ ] ≡ add (m [ σ ]) (n [ σ ])

    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ (sty Bool) b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      {σ : Sub Γ Δ σ'} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    
    isZeroβ₁ : ∀ {Γ'} {Γ : Con Γ'} → isZero (zero {Γ = Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ (sty Nat) n'} →
      isZero (suc n) ≡ false
    addβ₁ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ (sty Nat) n'} →
      add zero n ≡ n
    addβ₂ : ∀ {Γ' m' n'} {Γ : Con Γ'}
      {m : Tm Γ (sty Nat) m'}{n : Tm Γ (sty Nat) n'} →
      add (suc m) n ≡ suc (add m n)

    iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : STy A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ (sty A) u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧ST : (A : I.STy) → STy A
  ⟦ I.Nat ⟧ST = Nat
  ⟦ I.Bool ⟧ST = Bool

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.sty A ⟧T = sty ⟦ A ⟧ST
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧ST ⇒ ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ I.sty A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦add⟧ : ∀ {Γ} {m n : I.Tm Γ (I.sty I.Nat)} →
      ⟦ I.add m n ⟧t ≡ add ⟦ m ⟧t ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦add⟧ #-}
               
    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ (I.sty I.Bool)}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ (I.sty A)} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}
