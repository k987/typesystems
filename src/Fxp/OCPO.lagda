\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fxp.OCPO where

open import Lib hiding (I)

module I {i} (A : Set i) where
  infix 5 _⊑_

  postulate
    D : Set
    _⊑_ : D → D → Prop

    η : A → D
    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

\end{code}
\begin{code}
record ωCPO {i j} : Set (lsuc (i ⊔ j)) where
  infix 5 _⊑_
  
  field
    D : Set i
    _⊑_ : D → D → Prop j

    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

  -------------------------------------------
  -- recursor
  ------------------------------------------

  module _ {k}(A : Set k)(η : A → D)  where
    module J = I A
    postulate
      ⟦_⟧ : J.D → D
      ⟦_⟧⊑ : ∀ {a b} → a J.⊑ b → ⟦ a ⟧ ⊑ ⟦ b ⟧

      ⟦η⟧ : ∀ {a} → ⟦ J.η a ⟧ ≡ η a
      ⟦⊥⟧ : ⟦ J.⊥ ⟧ ≡ ⊥
      ⟦∐⟧ : ∀ {f p} → ⟦ J.∐ f p ⟧ ≡ ∐ (⟦_⟧ ∘ f) (λ n → ⟦ p n ⟧⊑)

I : Set → ωCPO
I A = record
        { D = I.D A
        ; _⊑_ = I._⊑_ A
        ; ⊥ = I.⊥ A
        ; ∐ = I.∐ A
        ; refl⊑ = I.refl⊑ A
        ; trans⊑ = I.trans⊑ A
        ; antisym⊑ = I.antisym⊑ A
        ; inf⊑ = I.inf⊑ A
        ; ιn = I.ιn A
        ; out = I.out A
        }

-- Morphism which does not preserve strictness
record ωCPOMor (Γ Δ : ωCPO {lzero}{lzero}) : Set where
  private
    module Γ = ωCPO Γ
    module Δ = ωCPO Δ
  field
    D : Γ.D → Δ.D
    _⊑_ : ∀{γ γ'} → γ Γ.⊑ γ' → D γ Δ.⊑ D γ'
    ∐ : ∀ {f p} → D (Γ.∐ f p) ≡ Δ.∐ (D ∘ f) λ n →  _⊑_ (p n) 

{-        
module PartialityMonad where
  return : ∀ {i} {A : Set i} → A → A ,⊥
  return {A = A} = I.η A

  private
    P : ∀ {i} (A : Set i) → ωCPO (A ,⊥)
    P A = let open I A
                in  record
                      { D = D
                      ; _⊑_ = _⊑_
                      ; η = idf
                      ; ⊥ = ⊥
                      ; ∐ = ∐
                      ; refl⊑ = refl⊑
                      ; trans⊑ = trans⊑
                      ; antisym⊑ = antisym⊑
                      ; inf⊑ = inf⊑
                      ; ιn = ιn
                      ; out = out
                      }
    module P {i} {A : Set i} = ωCPO (P A)

  join : ∀ {i} {A : Set i} → A ,⊥ ,⊥ → A ,⊥
  join = P.⟦_⟧

  private
    Q : ∀ {i j} {A : Set i}{B : Set j} → (A → B) → ωCPO A
    Q {A = A}{B} f = let open I B
                     in  record
                           { D = D
                           ; _⊑_ = _⊑_
                           ; η = η ∘ f
                           ; ⊥ = ⊥
                           ; ∐ = ∐
                           ; refl⊑ = refl⊑
                           ; trans⊑ = trans⊑
                           ; antisym⊑ = antisym⊑
                           ; inf⊑ = inf⊑
                           ; ιn = ιn
                           ; out = out
                           }
    module Q {i}{j} {A : Set i}{B : Set j} (f : A → B) = ωCPO (Q f)

  fmap : ∀ {i j} {A : Set i}{B : Set j} → (A → B) → A ,⊥ → B ,⊥
  fmap = Q.⟦_⟧

  open I
  fmap⊑ : ∀ {i j} {A : Set i}{B : Set j}{a a' : A ,⊥}{f} →
    _⊑_ A a a' → _⊑_ B (fmap f a) (fmap f a')
  fmap⊑ {f = f} = Q.⟦_⟧⊑ f

  fmap∐ : ∀ {i j} {A : Set i}{B : Set j}{a a' : A ,⊥}{f g p} →
    fmap g (∐ A f p) ≡ ∐ B (fmap g ∘ f) λ n → fmap⊑ (p n)
  fmap∐ {g = g} = Q.⟦∐⟧ g

  infixl 7 _<$>_
  _<$>_ = fmap

  infixl 6 _>>=_
  _>>=_ : ∀ {i j} {A : Set i}{B : Set j} → A ,⊥ → (A → B ,⊥) → B ,⊥
  ma >>= f = join (fmap f ma)

  infixl 7 _<*>_
  _<*>_ : ∀{i j} {A : Set i} {B : Set j} → (A → B) ,⊥ → A ,⊥ → B ,⊥
  f <*> x  = f >>= _<$> x
-}  
\end{code}
