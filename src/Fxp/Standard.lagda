\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fxp.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_) hiding (I)

module I {i} (A : Set i) where
  infix 5 _⊑_

  postulate
    D : Set
    _⊑_ : D → D → Prop

    η : A → D
    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

record ωCPO {i j} : Set (lsuc (i ⊔ j)) where
  infix 5 _⊑_
  
  field
    D : Set i
    _⊑_ : D → D → Prop j

    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

  module _ {k}(A : Set k)(η : A → D)  where
    module J = I A
    postulate
      ⟦_⟧ : J.D → D
      ⟦_⟧⊑ : ∀ {a b} → a J.⊑ b → ⟦ a ⟧ ⊑ ⟦ b ⟧

      ⟦η⟧ : ∀ {a} → ⟦ J.η a ⟧ ≡ η a
      ⟦⊥⟧ : ⟦ J.⊥ ⟧ ≡ ⊥
      ⟦∐⟧ : ∀ {f p} → ⟦ J.∐ f p ⟧ ≡ ∐ (⟦_⟧ ∘f f) (λ n → ⟦ p n ⟧⊑)

I : Set → ωCPO
I A = record
        { D = I.D A
        ; _⊑_ = I._⊑_ A
        ; ⊥ = I.⊥ A
        ; ∐ = I.∐ A
        ; refl⊑ = I.refl⊑ A
        ; trans⊑ = I.trans⊑ A
        ; antisym⊑ = I.antisym⊑ A
        ; inf⊑ = I.inf⊑ A
        ; ιn = I.ιn A
        ; out = I.out A
        }

record ωCPOMor (Γ Δ : ωCPO {lzero}{lzero}) : Set where
  private
    module Γ = ωCPO Γ
    module Δ = ωCPO Δ
  field
    D : Γ.D → Δ.D
    _⊑_ : ∀{γ γ'} → γ Γ.⊑ γ' → D γ Δ.⊑ D γ'
    ∐ : ∀ {f p} → D (Γ.∐ f p) ≡ Δ.∐ (D ∘f f) (λ n →  _⊑_ (p n))
open ωCPOMor

∙ : ωCPO
∙ = record { D = ↑p 𝟙 ; _⊑_ = λ _ _ → 𝟙 ; ⊥ = ↑[ * ]↑ ; ∐ = λ _ _ → ↑[ * ]↑ ; refl⊑ = * ; trans⊑ = λ _ _ → * ; antisym⊑ = λ _ _ → refl ; inf⊑ = * ; ιn = λ _ → * ; out = λ _ _ → * }

_▹_ : ωCPO {lzero}{lzero} → ωCPO {lzero}{lzero} → ωCPO {lzero}{lzero}
Γ ▹ A = let module Γ = ωCPO Γ  
            module A = ωCPO A
        in record
             { D = Γ.D × A.D
             ; _⊑_ = λ u v → (π₁ u Γ.⊑ π₁ v) ×p (π₂ u A.⊑ π₂ v)
             ; ⊥ = Γ.⊥ ,Σ A.⊥
             ; ∐ = λ f p → (Γ.∐ (π₁ ∘f f) λ n → π₁ (p n)) ,Σ (A.∐ (π₂ ∘f f) λ n → π₂ (p n))
             ; refl⊑ = Γ.refl⊑ ,p A.refl⊑
             ; trans⊑ = λ u v → (Γ.trans⊑ (π₁ u) (π₁ v)) ,p ((A.trans⊑ (π₂ u) (π₂ v)))
             ; antisym⊑ = λ u v → ap2 _,Σ_ (Γ.antisym⊑ (π₁ u) (π₁ v)) ((A.antisym⊑ (π₂ u) (π₂ v)))
             ; inf⊑ = Γ.inf⊑ ,p A.inf⊑
             ; ιn = λ u → (Γ.ιn (λ n → π₁ (u n))) ,p ((A.ιn (λ n → π₂ (u n))))
             ; out = λ u n → (Γ.out (π₁ u) n) ,p (A.out (π₂ u) n)
             }

id : ∀{Γ} → ωCPOMor Γ Γ
id = record { D = idf ; _⊑_ = λ u → u ; ∐ = refl }

_∘_ : ∀{Γ Δ Θ} → ωCPOMor Δ Θ → ωCPOMor Γ Δ → ωCPOMor Γ Θ
G ∘ F = let module F = ωCPOMor F
            module G = ωCPOMor G
         in record { D = λ γ → G.D (F.D γ) ; _⊑_ = λ u → G._⊑_ (F._⊑_ u) ; ∐ = ap G.D F.∐ ◾ G.∐ }

private
  postulate
    funext : ∀ {i j} {A : Set i} {B : Set j} {f g : A → B} →
      (∀ {a} → f a ≡ g a) → f ≡ g

_⇒_ : ωCPO {lzero}{lzero} → ωCPO {lzero}{lzero} → ωCPO {lzero}{lzero}
A ⇒ B = let module A = ωCPO A
            module B = ωCPO B
        in record
          { D = ωCPOMor A B
          ; _⊑_ = λ f g → (x : A.D) → D f x B.⊑ D g x
          ; ⊥ = record { D = λ _ → B.⊥ ; _⊑_ = λ _ → B.refl⊑ ; ∐ = λ {f}{p} → B.antisym⊑ B.inf⊑ (B.ιn λ _ → B.refl⊑) }
          ; ∐ = λ f p → record {
            D = λ x → B.∐ (λ n → D (f n) x) (λ n → p n x) ;
            _⊑_ = λ {γ}{γ'} γ⊑ → B.ιn λ n → B.trans⊑ (_⊑_ (f n) γ⊑) (B.out {d = B.∐ (λ n₁ → D (f n₁) γ') (λ n → p n γ')} B.refl⊑ n) ;
            ∐ = λ {f'}{p'} → {!!} } -- ap (λ x → B.∐ x _) (funext λ {n} → ∐ (f n) {f'}{p'} ◾ {!_⊑_ (f n)!}) }
            -- f : ℕ → ωCPOMor A B
            -- B.⊔ (λ x → D (f x) (f' n)) _
            -- B.⊔ (λ x → D (f n) (f' x)) _
          ; refl⊑ = λ _ → B.refl⊑
          ; trans⊑ = λ e e' x → B.trans⊑ (e x) (e' x)
          ; antisym⊑ = λ {f}{g} e e' → {!!}
          ; inf⊑ = λ _ → B.inf⊑
          ; ιn = {!!}
          ; out = {!!}
          }

--   ∐ : ∀ {f p} → D (Γ.∐ f p) ≡ Δ.∐ (D ∘f f) (λ n →  _⊑_ (p n))

open import Fxp.Algebra

A : Algebra
A = record
      { Con = ωCPO
      ; STy = Set
      ; Ty = ωCPO
      ; Sub = ωCPOMor
      ; Tm = ωCPOMor
      ; ∙ = ∙
      ; _▹_ = _▹_
      ; Nat = ℕ
      ; Bool = 𝟚
      ; sty = I
      ; _⇒_ = {!!}
      ; _∘_ = _∘_
      ; id = id
      ; ε = {!!}
      ; _,_ = {!!}
      ; p = {!!}
      ; q = {!!}
      ; _[_] = {!!}
      ; lam = {!!}
      ; app = {!!}
      ; zero = {!!}
      ; suc = {!!}
      ; isZero = {!!}
      ; add = {!!}
      ; true = {!!}
      ; false = {!!}
      ; ite = {!!}
      ; ass = {!!}
      ; idl = {!!}
      ; idr = {!!}
      ; ∙η = {!!}
      ; ▹β₁ = {!!}
      ; ▹β₂ = {!!}
      ; ▹η = {!!}
      ; [id] = {!!}
      ; [∘] = {!!}
      ; ⇒β = {!!}
      ; ⇒η = {!!}
      ; isZeroβ₁ = {!!}
      ; isZeroβ₂ = {!!}
      ; addβ₁ = {!!}
      ; addβ₂ = {!!}
      ; iteβ₁ = {!!}
      ; iteβ₂ = {!!}
      ; lam[] = {!!}
      ; zero[] = {!!}
      ; suc[] = {!!}
      ; isZero[] = {!!}
      ; add[] = {!!}
      ; true[] = {!!}
      ; false[] = {!!}
      ; ite[] = {!!}
      }


{-
open I

isO : ℕ → 𝟚
isO O = I
isO (S n) = O

St : Algebra
St = record
       { Con = Set
       ; STy = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; Bool = 𝟚
       ; sty = idf
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; zero = const O
       ; suc = S ∘f_
       ; isZero = isO ∘f_
       ; add = λ m n γ → m γ +ℕ n γ
       ; true = const I
       ; false = const O
       ; ite = λ b u v γ → if b γ then u γ else v γ
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; isZero[] = refl
       ; add[] = refl
       ; true[] = refl
       ; false[] = refl
       ; ite[] = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; addβ₁ = refl
       ; addβ₂ = refl
       ; iteβ₁ = refl
       ; iteβ₂ = refl
       }
open Algebra St using (⟦_⟧ST ; ⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

---------------------------------------------------------
-- embedding for Bool and Nat
---------------------------------------------------------

⌜_⌝N : ℕ → Tm ∙ (sty Nat)
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → Tm ∙ (sty Bool)
⌜ O ⌝B = false
⌜ I ⌝B = true

⌜_⌝ : ∀ {A} → ⟦ A ⟧ST → Tm ∙ (sty A)
⌜_⌝ {Nat} = ⌜_⌝N
⌜_⌝ {Bool} = ⌜_⌝B

-- lemmas

isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝B ≡ isZero ⌜ n ⌝N
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

add-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝N ≡ add ⌜ m ⌝N ⌜ n ⌝N
add-⌜⌝ {O} = refl
add-⌜⌝ {S m} = ap suc (add-⌜⌝ {m})
-}
\end{code}
