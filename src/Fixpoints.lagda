\chapter{Fixpoints and errors}

First add a term \verb$error:A$ for any \verb$A$. Then add fixpoints.

This is too naive. We need to separate values and terms (computations)
or we have to introduce natural numbers everywhere and then it won't
be strict anymore.

\begin{code}
{-# OPTIONS --prop --rewriting #-}

module Fixpoints where

open import Lib

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _⊚_
  infixl 5 _،_ --\,2
  infixl 6 _[_]
  infixl 5 _$_

  field
    Con : Set i
    Sub : Con → Con → Set k
    
    _⊚_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} → (σ ⊚ δ) ⊚ ν ≡ σ ⊚ (δ ⊚ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ⊚ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ⊚ id ≡ σ
    
    ∙ : Con
    ε : ∀ {Γ} → Sub Γ ∙
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    Ty  : Set j
    Tm  : Con → Ty → Set l

    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [⊚] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ⊚ δ ]

    _▹_ : Con → Ty → Con
    _،_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ⊚ (σ ، t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ ، t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ⊚ σ ، q [ σ ] ≡ σ


    Nat  : Ty
    Bool : Ty
    
    _⇒_  : Ty → Ty → Ty
    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ⊚ p ، q ])

M : Model
M = record
  { Con = Set
  ; Sub = λ Δ Γ → Δ → Γ
  ; _⊚_ = λ γ δ θ* → γ (δ θ*)
  ; id  = λ γ* → γ*
  ; ass = λ {_}{_}{_}{_}{γ}{δ}{θ} → refl {x = λ σ* → γ (δ (θ σ*))}
  ; idl = λ {_}{_}{γ} → refl {x = γ}
  ; idr = λ {_}{_}{γ} → refl {x = γ}
  ; ∙ = Lift ⊤
  ; ε = λ _ → mk trivi
  ; ∙η = λ {Γ} → refl {A = Γ → Lift ⊤}
  ; Ty = Set
  ; Tm = λ Γ A → ℕ → Γ → A
  ; _[_] = λ t γ n δ* → t n (γ δ*)
  ; [id] = λ {_}{_}{t} → refl {x = t}
  ; [⊚] = λ {_}{_}{_}{_}{t}{γ}{δ} → refl {x = λ n θ* → t n (γ (δ θ*))}
  ; _▹_ = _×_
  ; _،_ = λ γ t δ* → γ δ* , {!!}
  ; p = {!!}
  ; q = {!!}
  ; ▹β₁ = {!!}
  ; ▹β₂ = {!!}
  ; ▹η = {!!}
  ; Nat = {!!}
  ; Bool = {!!}
  ; _⇒_ = {!!}
  ; lam = {!!}
  ; app = {!!}
  ; ⇒β = {!!}
  ; ⇒η = {!!}
  ; lam[] = {!!}
  }

\end{code}
