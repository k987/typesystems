\chapter{Eta law for Bool}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Eta where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

module I where
  data Ty : Set where
    Bool : Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    -- Tm (Γ ▹ Bool) A ≅ Tm Γ A × Tm Γ A
    ite     : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm (Γ ▹ Bool) A
    true    : ∀{Γ A} → Tm (Γ ▹ Bool) A → Tm Γ A
    false   : ∀{Γ A} → Tm (Γ ▹ Bool) A → Tm Γ A
    Boolβ₁  : ∀{Γ A}{u v : Tm Γ A} → true  (ite u v) ≡ u
    Boolβ₂  : ∀{Γ A}{u v : Tm Γ A} → false (ite u v) ≡ v
    Boolη   : ∀{Γ A}{t : Tm (Γ ▹ Bool) A} → ite (true t) (false t) ≡ t
    ite[]   : ∀{Γ A}{u v : Tm Γ A}{Θ}{σ : Sub Θ Γ} →
              ite u v [ σ ∘ p , q ] ≡ ite (u [ σ ]) (v [ σ ])
              
  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  true[] : ∀{Γ A}{t : Tm (Γ ▹ Bool) A}{Θ}{σ : Sub Θ Γ} →
            true t [ σ ] ≡ true (t [ σ ∘ p , q ])
  true[] = {!!}

  false[] : ∀{Γ A}{t : Tm (Γ ▹ Bool) A}{Θ}{σ : Sub Θ Γ} →
            false t [ σ ] ≡ false (t [ σ ∘ p , q ])
  false[] = {!!}

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE Boolβ₁ Boolβ₂ Boolη ite[] true[] false[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con       : Set i
    Sub       : Con → Con → Set k
    Ty        : Set j
    Tm        : Con → Ty → Set l

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]      : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]      : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]       : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    _▹_       : Con → Ty → Con
    _,_       : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    Bool      : Ty
    -- Tm (Γ ▹ Bool) A ≅ Tm Γ A × Tm Γ A
    ite     : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm (Γ ▹ Bool) A
    true    : ∀{Γ A} → Tm (Γ ▹ Bool) A → Tm Γ A
    false   : ∀{Γ A} → Tm (Γ ▹ Bool) A → Tm Γ A
    Boolβ₁  : ∀{Γ A}{u v : Tm Γ A} → true  (ite u v) ≡ u
    Boolβ₂  : ∀{Γ A}{u v : Tm Γ A} → false (ite u v) ≡ v
    Boolη   : ∀{Γ A}{t : Tm (Γ ▹ Bool) A} → ite (true t) (false t) ≡ t
    ite[]   : ∀{Γ A}{u v : Tm Γ A}{Θ}{σ : Sub Θ Γ} →
              ite u v [ σ ∘ p , q ] ≡ ite (u [ σ ]) (v [ σ ])
                
  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦true⟧ : ∀{Γ A}{t : I.Tm (Γ I.▹ I.Bool) A} → ⟦ I.true t ⟧t ≡ true ⟦ t ⟧t
    ⟦false⟧ : ∀{Γ A}{t : I.Tm (Γ I.▹ I.Bool) A} → ⟦ I.false t ⟧t ≡ false ⟦ t ⟧t
    ⟦ite⟧ : ∀{Γ A}{u v : I.Tm Γ A} → 
      ⟦ I.ite u v ⟧t ≡ ite ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    Bool : Ty I.Bool
    ite     : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A u' → Tm Γ A v' → Tm (Γ ▹ Bool) A (I.ite u' v')
    true    : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ Bool) A t' → Tm Γ A (I.true t')
    false   : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ Bool) A t' → Tm Γ A (I.false t')
    Boolβ₁  : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm Γ A v'} → true (ite u v) ≡ u
    Boolβ₂  : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm Γ A v'} → false (ite u v) ≡ v
    Boolη   : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty A'}{t : Tm (Γ ▹ Bool) A t'} → ite (true t) (false t) ≡ t
    ite[]   : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm Γ A v'}{Θ' σ'}{Θ : Con Θ'}{σ : Sub Θ Γ σ'} →
              ite u v [ σ ∘ p , q ] ≡ ite (u [ σ ]) (v [ σ ])
              
  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦true⟧ : ∀{Γ A}{t : I.Tm (Γ I.▹ I.Bool) A} → ⟦ I.true t ⟧t ≡ true ⟦ t ⟧t
    ⟦false⟧ : ∀{Γ A}{t : I.Tm (Γ I.▹ I.Bool) A} → ⟦ I.false t ⟧t ≡ false ⟦ t ⟧t
    ⟦ite⟧ : ∀{Γ A}{u v : I.Tm Γ A} → 
      ⟦ I.ite u v ⟧t ≡ ite ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

open I

postulate
  funext : {A B : Set}{f g : A → B} → ((x : A) → f x ≡ g x) → f ≡ g

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; Bool = 𝟚
       ; true = λ t γ → t (γ ,Σ I)
       ; false = λ t γ → t (γ ,Σ O)
       ; ite = λ { u v (γ ,Σ b) → if b then u γ else v γ }
       ; Boolβ₁ = refl
       ; Boolβ₂ = refl
       ; Boolη = funext λ { (γ ,Σ O) → refl ; (γ ,Σ I) → refl }
       ; ite[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

norm : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
norm t = ⟦ t ⟧t *↑


Comp : DepAlgebra
Comp = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; Bool = λ t → ↑p (t ≡ true q) ⊎ ↑p (t ≡ false q)
         ; true = λ {Γ'}{A'}{t'}{Γ}{A} t {ν'} ν̂ → {!t {ν' , true q} (ν̂ ,Σ ι₁ ↑[ refl ]↑)!}
         {-
    ite u' v' [ ν' ] = 
    ite u' v' [ p ∘ ν' ∘ p , q ] [ id , q [ ν' ] ] = 
    ite (u' [ p ∘ ν' ]) (v' [ p ∘ ν' ]) [ id , true q ]

    Boolβ₁  : ∀{Γ A}{u v : Tm Γ A} → true  (ite u v) ≡ u
    Boolβ₂  : ∀{Γ A}{u v : Tm Γ A} → false (ite u v) ≡ v
    Boolη   : ∀{Γ A}{t : Tm (Γ ▹ Bool) A} → ite (true t) (false t) ≡ t
    ite[]   : ite u v [σ∘p,q] = ite (u[σ]) (v[σ])
    true[]  : true t[σ]=true(t[σ∘p,q])

    ite u v : Tm (Γ▹Bool) A
    ite u v [ id , true q ] ≟ true (ite u v) = u : Tm Γ A
    
-}
         ; false = {!!} -- λ _ → refl↑
         ; ite = λ {Γ'}{A'}{u'}{v'}{Γ}{A} u v {ν'} ν̂ → ind⊎ (λ _ → A (ite u' v' [ ν' ]))
                 (λ e → {!u {p ∘ ν'} (π₁ ν̂) !})
                 (λ e → {!!})
                 (π₂ ν̂)
         {-
         λ {_ _ b' u' v' _ A} b u v {ν'} x →
                   let P t = A (ite t (u' [ ν' ]) (v' [ ν' ]))
                   in  ind𝟚 (λ b → ⌜ b ⌝B ≡ b' [ ν' ] → P (b' [ ν' ]))
                            (λ e → transport P e (v x))
                            (λ e → transport P e (u x))
                            (norm (b' [ ν' ]))
                            ↓[ b x ]↓
         -}
         ; Boolβ₁ = {!!}
         ; Boolβ₂ = {!!}
         ; Boolη = {!!}
         ; ite[] = {!!}
         }
module Comp = DepAlgebra Comp

\end{code}
