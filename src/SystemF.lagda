\chapter{System F}\label{ch:systemf}

Church encodings.

\begin{verbatim}
Ty  : Set*                                           Ty : Con*_{PSh(C)}
Tm  : Ty → Set*                                      Tm : Ty*_{PSh(C)} Ty
_⇒_ : Ty → Ty → Ty                                   ⇒  : Tm_{PSh(C)}
∀   : (Ty →* Ty) → Ty
lam : (Tm A →* Tm B) ≅ Tm (A ⇒ B) : app
Lam : (Π* Ty λ* A . Tm (B * A)) ≅ Tm (∀ B) : App
Lam : ((A : Ty) →* Tm (B * A)) ≅ Tm (∀ B) : App      (Π* Ty (Tm[app* B])) ≅
\end{verbatim}
