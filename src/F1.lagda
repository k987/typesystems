\chapter{Polymorphisms}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting --type-in-type #-}

module F1 where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixl 5 _▹U
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 5 _,̇_
  infixl 6 _[_]T
  infixl 6 _[_]t
  infixl 5 _$_
  
  field
    Con       : Set i
    Sub       : Con → Con → Set k
    Ty        : Con → Set j
    Tm        : (Γ : Con) → Ty Γ → Set l

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]T     : ∀{Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
    [id]T     : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    [∘]T      : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T

    _[_]t     : ∀{Γ Δ A} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
    [id]t     : ∀{Γ A} {t : Tm Γ A} → transport (Tm Γ) [id]T (t [ id ]t) ≡ t
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                transport (Tm Γ) [∘]T (t [ σ ∘ δ ]t) ≡ t [ σ ]t [ δ ]t

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _▹_       : (Γ : Con) → Ty Γ → Con
    _,_       : ∀{Γ Δ A} → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (A [_]T) ▹β₁) (q [ σ , t ]t) ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , transport (Tm Γ) ([∘]T ⁻¹) (q [ σ ]t) ≡ σ

    _⇒_       : ∀{Γ} → Ty Γ → Ty Γ → Ty Γ
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) (B [ p ]T) → Tm Γ (A ⇒ B)
    app       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) (B [ p ]T)
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) (B [ p ]T)} → app (lam t) ≡ t
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    ⇒[]       : ∀{Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ (A [ σ ]T ⇒ B [ σ ]T)
    lam[]     : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) (B [ p ]T)}{σ : Sub Γ Δ} →
                transport (Tm Γ) ⇒[] ((lam t) [ σ ]t) ≡
                lam (transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹ ◾ ap (B [_]T) ▹β₁ ◾ [∘]T) (t [ σ ∘ p , transport (Tm (Γ ▹ (A [ σ ]T))) ([∘]T ⁻¹) q ]t))

    _▹U       : Con → Con
    _,̇_       : ∀{Γ Δ}(σ : Sub Γ Δ) → Ty Γ → Sub Γ (Δ ▹U)
    ṗ         : ∀{Γ} → Sub (Γ ▹U) Γ
    q̇         : ∀{Γ} → Ty (Γ ▹U)
    ▹̇β₁       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → ṗ ∘ (σ ,̇ A) ≡ σ
    ▹̇β₂       : ∀{Γ Δ}{σ : Sub Γ Δ}{A : Ty Γ} → q̇ [ σ ,̇ A ]T ≡ A
    ▹̇η        : ∀{Γ} → (ṗ ,̇ q̇ {Γ}) ≡ id

    ∀̇         : ∀{Γ} → Ty (Γ ▹U) → Ty Γ
    laṁ       : ∀{Γ A} → Tm (Γ ▹U) A → Tm Γ (∀̇ A)
    apṗ       : ∀{Γ A} → Tm Γ (∀̇ A) → Tm (Γ ▹U) A
    ∀β        : ∀{Γ A}{t : Tm (Γ ▹U) A} → apṗ (laṁ t) ≡ t
    ∀η        : ∀{Γ A}{t : Tm Γ (∀̇ A)} → laṁ (apṗ t) ≡ t
    ∀[]       : ∀{Γ Δ}{A : Ty (Δ ▹U)}{σ : Sub Γ Δ} → ((∀̇ A) [ σ ]T) ≡ ∀̇ (A [ σ ∘ ṗ ,̇ q̇ ]T)

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  _$_ {Γ}{A}{B} t u = transport (Tm Γ) ([∘]T ⁻¹ ◾ ap (B [_]T) ▹β₁ ◾ [id]T) (app t [ id , transport (Tm Γ) ([id]T ⁻¹) u ]t)

  qp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm (Γ ▹ A ▹ B) (A [ p ]T [ p ]T)
  qp = q [ p ]t
  qṗ : {Γ : Con}{A : Ty Γ} → Tm (Γ ▹ A ▹U) (A [ p ]T [ ṗ ]T)
  qṗ = q [ ṗ ]t
  qpp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)} → Tm (Γ ▹ A ▹ B ▹ C) (A [ p ]T [ p ]T [ p ]T)
  qpp = q [ p ]t [ p ]t
  qṗp : {Γ : Con}{A : Ty Γ}{C : Ty (Γ ▹ A ▹U)} → Tm (Γ ▹ A ▹U ▹ C) (A [ p ]T [ ṗ ]T [ p ]T)
  qṗp = q [ ṗ ]t [ p ]t
  qpṗ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm (Γ ▹ A ▹ B ▹U) (A [ p ]T [ p ]T [ ṗ ]T)
  qpṗ = q [ p ]t [ ṗ ]t
  qṗṗ : {Γ : Con}{A : Ty Γ} → Tm (Γ ▹ A ▹U ▹U) (A [ p ]T [ ṗ ]T [ ṗ ]T)
  qṗṗ = q [ ṗ ]t [ ṗ ]t
  qppp : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)}{D : Ty (Γ ▹ A ▹ B ▹ C)} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) (A [ p ]T [ p ]T [ p ]T [ p ]T)
  qppp = q [ p ]t [ p ]t [ p ]t

  q̇p : {Γ : Con}{A : Ty (Γ ▹U)} → Ty (Γ ▹U ▹ A)
  q̇p = q̇ [ p ]T
  q̇ṗ : {Γ : Con} → Ty (Γ ▹U ▹U)
  q̇ṗ = q̇ [ ṗ ]T
\end{code}
