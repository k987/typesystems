\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Fin.Product where

open import Lib
  hiding (_∘_)
  renaming (_×_ to _×m_ ; _,_ to _,Σ_)

module I where
  data Ty : Set where
    Unit : Ty
    Empty : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    uη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

open I hiding (⟨_,_⟩ ; proj₁ ; proj₂ ; case ;
               ×β₁ ; ×β₂ ; ×η ;
               ⟨,⟩[] ; proj₁[] ; proj₂[] ; case[])

module coind
       (gen : ∀ {Γ A B C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) B →
         Tm Γ C → Tm Γ (A × B))
       (proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A)
       (proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B)
       (×β₁ : ∀ {Γ A B C} {u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) B}{t : Tm Γ C} →
         proj₁ (gen u v t) ≡ u [ id , t ])
       (×β₂ : ∀ {Γ A B C} {u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) B}{t : Tm Γ C} →
         proj₂ (gen u v t) ≡ v [ id , t ])
       (×η : ∀ {Γ A B C} {t : Tm (Γ ▹ C) (A × B)}{u : Tm Γ C} →
         gen (proj₁ t) (proj₂ t) u ≡ t [ id , u ])
       (gen[] : ∀ {Γ Δ A B C} {u : Tm (Δ ▹ C) A}{v : Tm (Δ ▹ C) B}
         {t : Tm Δ C}{σ : Sub Γ Δ} →
         (gen u v t) [ σ ] ≡
           gen (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ]) (t [ σ ]))
       (proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
         (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ]))
       (proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
         (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ]))
       where
  ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
  ⟨ u , v ⟩ = gen (u [ p ]) (v [ p ]) tt

  ×β₁' : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
  ×β₁' {u = u} = ×β₁ ◾ [∘] ◾ ap (u [_]) ▹β₁ ◾ [id]

  ×β₂' : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
  ×β₂' {v = v} = ×β₂ ◾ [∘] ◾ ap (v [_]) ▹β₁ ◾ [id]

  ×η' : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
  ×η' {t = t} = ap2 (λ u v → gen u v tt) proj₁[] proj₂[]
              ◾ ×η ◾ [∘] ◾ ap (t [_]) ▹β₁ ◾ [id]

  ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
    ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩
  ⟨,⟩[] {u = u}{v}{σ} = gen[]
                      ◾ ap3 gen
                        ([∘] ◾ ap (u [_]) ▹β₁ ◾ [∘] ⁻¹)
                        ([∘] ◾ ap (v [_]) ▹β₁ ◾ [∘] ⁻¹)
                        tt[]

module ⇒
       (⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B))
       (proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A)
       (proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B)
       (×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u)
       (×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v)
       (×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t)
       (⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
         ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩)
       (proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
         (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ]))
       (proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
         (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ]))
       where

  ⟨_,_⟩' : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
  ⟨_,_⟩' = ⟨_,_⟩

  case : ∀ {Γ A B C} → Tm Γ (A × B) → Tm (Γ ▹ A ▹ B) C → Tm Γ C
  case t u = u [ id , proj₁ t , proj₂ t ]

  ×β' : ∀ {Γ A B C} {u : Tm Γ A}{v : Tm Γ B}{t : Tm (Γ ▹ A ▹ B) C} →
    case ⟨ u , v ⟩' t ≡ t [ id , u , v ]
  ×β' {t = t} = ap (t [_]) (ap2 (λ x y → id , x , y) ×β₁ ×β₂)

  ×η' : ∀ {Γ A B C} {t : Tm Γ (A × B)}{u : Tm (Γ ▹ A × B) C} →
    case t (u [ p ∘ p , ⟨ q [ p ] , q ⟩' ]) ≡ u [ id , t ]
  ×η' {t = t}{u} = [∘]
                 ◾ ap (u [_])
                   ( ,∘
                   ◾ ap2 _,_
                     ( ass
                     ◾ ap (p ∘_) ▹β₁
                     ◾ ▹β₁)
                     ( ⟨,⟩[]
                     ◾ ap2 ⟨_,_⟩
                       ( [∘]
                       ◾ ap (q [_]) ▹β₁
                       ◾ ▹β₂)
                       ▹β₂
                     ◾ ×η))

  ⟨,⟩'[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
    ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩
  ⟨,⟩'[] = ⟨,⟩[]

  case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A × B)}{u : Tm (Δ ▹ A ▹ B) C}
    {σ : Sub Γ Δ} →
    (case t u) [ σ ] ≡ case (t [ σ ]) (u [ (σ ∘ p , q) ∘ p , q ])
  case[] {t = t}{u}{σ} = rail
    ([∘] 
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (,∘
            ◾ ap2 _,_ idl proj₁[])
          proj₂[]))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap ((σ ∘ p , q) ∘_) ▹β₁
            ◾ ,∘
            ◾ ap2 _,_
              (ass
                ◾ ap (σ ∘_) ▹β₁
                ◾ idr)
              ▹β₂)
          ▹β₂))
    refl

module ⇐
       (⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B))
       (case : ∀ {Γ A B C} → Tm Γ (A × B) → Tm (Γ ▹ A ▹ B) C → Tm Γ C)
       (×β : ∀ {Γ A B C} {u : Tm Γ A}{v : Tm Γ B}{t : Tm (Γ ▹ A ▹ B) C} →
         case ⟨ u , v ⟩ t ≡ t [ id , u , v ])
       (×η : ∀ {Γ A B C} {t : Tm Γ (A × B)}{u : Tm (Γ ▹ A × B) C} →
         case t (u [ p ∘ p , ⟨ q [ p ] , q ⟩ ]) ≡ u [ id , t ])
       (⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
         ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩)
       (case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A × B)}{u : Tm (Δ ▹ A ▹ B) C}
         {σ : Sub Γ Δ} →
         (case t u) [ σ ] ≡ case (t [ σ ]) (u [ (σ ∘ p , q) ∘ p , q ]))
       where
  ⟨_,_⟩' : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
  ⟨_,_⟩' = ⟨_,_⟩

  proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
  proj₁ t = case t (q [ p ])

  proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B
  proj₂ t = case t q

  ×β₁' : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩' ≡ u
  ×β₁' = ×β ◾ [∘] ◾ ap (q [_]) ▹β₁ ◾ ▹β₂
       
  ×β₂' : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩' ≡ v
  ×β₂' = ×β ◾ ▹β₂

  ⟨,⟩'[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
    ⟨ u , v ⟩' [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩'
  ⟨,⟩'[] = ⟨,⟩[]

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {t = t}{σ} = case[]
                     ◾ ap (case (t [ σ ]))
                       ([∘] ◾ ap (q [_]) (▹β₁ ◾ ,∘) ◾ ▹β₂)

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {t = t}{σ} = case[] ◾ ap (case (t [ σ ])) ▹β₂

  private
    ⟨case⟩-lem : ∀ {Γ A B C} {t : Tm (Γ ▹ A ▹ B) C} →
      t [ p , proj₁ q , proj₂ q ] [ p ∘ p , ⟨ q [ p ] , q ⟩ ] ≡ t
    ⟨case⟩-lem {t = t} = [∘]
                    ◾ ap (t [_])
                      ( ,∘
                      ◾ ap2 _,_
                        ( ,∘
                        ◾ ap2 _,_
                          ▹β₁
                          (proj₁[] ◾ ap proj₁ ▹β₂ ◾ ×β₁'))
                        (proj₂[] ◾ ap proj₂ ▹β₂ ◾ ×β₂')
                      ◾ ap (_, q)
                        (,∘ ⁻¹ ◾ ap (_∘ p) ▹η' ◾ idl)
                      ◾ ▹η')
                    ◾ [id]
    
    ⟨case⟩ : ∀ {Γ A B C D} {t : Tm Γ (A × B)}
      {u : Tm (Γ ▹ A ▹ B) C}{v : Tm (Γ ▹ A ▹ B) D} →
      case t ⟨ u , v ⟩ ≡ ⟨ case t u , case t v ⟩
    ⟨case⟩ {t = t}{u}{v} = ap (case t) ⟨case⟩-lem ⁻¹
                         ◾ ×η ◾ [∘] ◾ ⟨,⟩[]
                         ◾ ap2 ⟨_,_⟩
                           ([∘] ⁻¹ ◾ ×η ⁻¹ ◾ ap (case t) ⟨case⟩-lem)
                           ([∘] ⁻¹ ◾ ×η ⁻¹ ◾ ap (case t) ⟨case⟩-lem)

    ×η2 : ∀ {Γ A B} {t : Tm Γ (A × B)} → case t ⟨ q [ p ] , q ⟩ ≡ t
    ×η2 {t = t} = ap (case t) ▹β₂ ⁻¹ ◾ ×η {t = t}{q} ◾ ▹β₂

  ×η' : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩' ≡ t
  ×η' = ⟨case⟩ ⁻¹ ◾ ×η2
