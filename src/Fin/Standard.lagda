\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module Fin.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _⊗_ ; _,_ to _,Σ_)
open import Fin.Algebra

open I

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _⊗_
       ; Unit = ↑p 𝟙
       ; Empty = ↑p 𝟘
       ; _⇒_ = λ A B → A → B
       ; _×_ = _⊗_
       ; _+_ = _⊎_
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; tt = const *↑
       ; ⟨_,_⟩ = λ u v γ → u γ ,Σ v γ
       ; proj₁ = π₁ ∘f_
       ; proj₂ = π₂ ∘f_
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; inl = ι₁ ∘f_
       ; inr = ι₂ ∘f_
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ x → u (γ ,Σ x)) (λ y → v (γ ,Σ y))
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑
\end{code}
