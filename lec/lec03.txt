M.⟦ I.isZero t ⟧ =(M.⟦_⟧ homomorfizmus)
M.isZero M.⟦ t ⟧ =(M.isZero def.)
(ha M.⟦ t ⟧ = 0 akkor 1 különben 0) =(M.true, M.false def. alapjan)
(ha M.⟦ t ⟧ = 0 akkor M.true különben M.false) =(M.ite def. alapjan)
M.ite M.⟦ t ⟧ M.false M.true =(M.⟦_⟧ megorzi ite muveletet)
M.⟦ I.ite t I.false I.true ⟧

- rekurzio

Nat-modell
Nat:Set
Zero:Nat
Suc:Nat→Nat

I = inicialis Nat-modell = ℕ,0,+1
M egy Nat-modell, akkor
M.⟦_⟧ : I.Nat → M.Nat
M.⟦ 0 ⟧ = M.Zero
M.⟦ 1 + t ⟧ = M.Suc M.⟦ t ⟧

osszeadas: A Nat-modell
A.Nat := I.Nat → I.Nat
A.Zero : A.Nat := λ n → n
A.Suc : A.Nat → A.Nat := I.Suc ∘_

A.⟦ 3 ⟧ = A.⟦ I.Suc (I.Suc (I.Suc I.Zero )) ⟧ =
A.Suc (A.Suc (A.Suc A.Zero)) =
I.Suc ∘ I.Suc ∘ I.Suc ∘ (λ n → n)

_+_ := A.⟦_⟧ : I.Nat → I.Nat → I.Nat
a + b := A.⟦ a ⟧ b

- teljes indukcio

fuggo Nat-modell:

Nat : I.Nat → Set
  Nat 0 : Set, Nat 1 : Set, Nat 2 : Set, ...
Zero : Nat I.Zero
Suc  : Nat m → Nat (I.Suc m)

Nat m := (m + 0 = m)
Zero : Nat I.Zero
       (I.Zero + 0 = I.Zero)
       ((λ n → n) 0 = I.Zero)
       (0 = 0)
Suc : Nat m       → Nat (I.Suc m)
      (m + 0 = m) → (I.Suc m + 0 = I.Suc m)
Suc ih := (I.Suc m + 0) =(+ def.) I.Suc(m+0) =(ih) I.Suc m)
  ih : m+0=m

indukcio:
minden D dependens Nat-modellre:
D.⟦_⟧ : (m : I.Nat) → D.Nat m
        ∀ m . D.Nat(m)
D.⟦ I.Zero ⟧ = D.Zero
D.⟦ I.Suc m ⟧ = D.Suc D.⟦ m ⟧




osszeadast megadtuk rekurzioval, 0 j.o-i egysegelem indukcioval

indukcio NatBoolAST-re:

dependens NatBoolAST-modell:

Tm : I.Tm → Set
true : Tm I.true
false : Tm I.false
ite : Tm t → Tm u → Tm v → Tm (I.ite t u v)
num : (n : ℕ) → Tm (I.num n)
isZero : Tm t → Tm (I.isZero t)
_+o_ : Tm u → Tm v → Tm (u I.+o v)

indukcio:
D dependens modell, akkor
D.⟦_⟧ : (t : I.Tm) → D.Tm t
D.⟦ I.true ⟧ = D.true
D.⟦ I.false ⟧ = D.false
D.⟦ I.num n ⟧ = D.num n
D.⟦ I.isZero t ⟧ = D.isZero D.⟦ t ⟧
...

height : I.Tm → ℕ
height := H.⟦_⟧
  H.Tm := ℕ
  H.true = H.false = H.num n = 0
  H.isZero t = 1 + t
  t H.+ t' := 1 + max(t,t')
  H.ite t t' t'' := 1 + max(t,t',t'')
  
trues : I.Tm → ℕ
trues := T.⟦_⟧
  T.Tm := ℕ
  T.true = 1, T.false = 0, T.num n = 0
  T.isZero t = t, t T.+ t' := t + t',
  T.ite t t' t'' := t+t'+t''

minden t-re (trues t) <= 3^(height t)
termek szerinti strukturalis indukcio
megadunk egy dependens modellt, es ebbe kiertekelunk

D.Tm t := ((trues t) <= 3^(height t))
D.true : ((trues I.true) = 1 <= 1 = 3^0 = 3^(height I.true))
D.false : ((trues I.false) = 0 <= 1 = 3^0 = 3^(height I.false))
D.num n : ((trues (I.num n)) = 0 <= 1 = 3^0 = 3^(height (I.num n)))
D.isZero : D.Tm t → D.Tm (I.isZero t)
D.isZero (ih : ((trues t) <= 3^(height t))) :
  ((trues (I.isZero t)) =
  trues t <= (x <= 3*x)
  3*(trues t) <=   (x <= y, akkor 3*x <= 3*y)
  3*3^(height t) = 
  3^(1 + height t) = 
  3^(height (I.isZero t)))
D._+o_ (ihu : (trues u) <= 3^(height u))
       (ihv : (trues v) <= 3^(height v)) :
  (trues (u I.+ v))            =
  trues u + trues v            <=(ihu, ihv)
  3^(height u)                + 3^(height v)  <= 
  3^{max(height u, height v)} + 3^{max(height u, height v)} =
  2 * 3^{max(height u, height v)} <= 
  3*3^(max (height u, height v))   =
  3^(1 + max (height u, height v)) =
  3^(height (u I.+ v))

D.ite (iht : (trues t) <= 3^(height t))
      (ihu : (trues u) <= 3^(height u))
      (ihv : (trues v) <= 3^(height v))
  : (trues (I.ite t u v)) <= ?(HF) 3^(height (I.ite t u v))

megvan D!
D.⟦_⟧ : (t : I.Tm) → ((trues t) <= 3^(height t))

-----------------------------------------------

NatBoolWT = jol tipusozott NatBool szintaxis

string, lex.el.sor, AST, WT, WT =-el

Ty : Set
Tm : Ty → Set
Nat : Ty
Bool : Ty
true : Tm Bool
false : Tm Bool
ite : Tm Bool → Tm A → Tm A → Tm A
num : ℕ → Tm Nat
isZero : Tm Nat → Tm Bool
_+_ : Tm Nat → Tm Nat → Tm Nat

I, minden M modellre van
  M.⟦_⟧ : I.Ty → M.Ty
  M.⟦_⟧ : I.Tm A → M.Tm (M.⟦ A ⟧)
  M.⟦ I.true ⟧ = M.true
  ^ : M.Tm (M.⟦ I.Bool ⟧) = M.Tm M.Bool
  M.⟦ u I.+o v ⟧ = M.⟦u⟧ M.+o M.⟦v⟧
  ...

standard modell St

St.Ty   := Set
St.Tm : Set → Set
St.Tm A := A
St.Bool := 𝟚
St.Nat  := ℕ
St.true := tt
St.false := ff
St.ite t u v := (ha t akkor u különben v)
St.num n := n
a St.+o b := a + b
  a : St.Tm St.Nat = St.Nat = ℕ
St.isZero a := (ha a=0 akkor tt különben ff)

St.⟦_⟧ : I.Tm A → St.Tm St.⟦A⟧
  evaluator, standard interpretatio, set interpretation,
  metacirkularis kiertekeles

St.⟦ I.true ⟧ = tt
St.⟦ I.ite (I.isZero (num 3)) true false) ⟧ =
  (ha (3 = 0) akkor tt kulonben ff) = ff

--------------------------------------------------

tipusellenorzes

